﻿using Microsoft.EntityFrameworkCore;
using PontosCulturais.Models;
using PontosCulturais.Models.CadastroEspaçoCultural;
using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.Serviços;
using PontosCulturais.Singletons;
using System;
using System.Diagnostics;

namespace PontosCulturais.Contexto
{
    public static class AlimentarBaseDeDados
    {
        public static void AlimentarDadosPessoaFísica(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GêneroModelo>().HasData(
                new GêneroModelo { Id = 1, Nome = "Homem Cis" },
                new GêneroModelo { Id = 2, Nome = "Mulher Cis" },
                new GêneroModelo { Id = 3, Nome = "Mulher Trans/Travesti" },
                new GêneroModelo { Id = 4, Nome = "Homem Trans" },
                new GêneroModelo { Id = 5, Nome = "Não-Binárie/Outro" },
                new GêneroModelo { Id = 6, Nome = "Não Declarar" }
            );

            modelBuilder.Entity<ComunidadeTradicionalModelo>().HasData(
                new ComunidadeTradicionalModelo { Id = 1, Nome = "Indígenas" },
                new ComunidadeTradicionalModelo { Id = 2, Nome = "Quilombolas" },
                new ComunidadeTradicionalModelo { Id = 3, Nome = "Ciganos(as)" },
                new ComunidadeTradicionalModelo { Id = 4, Nome = "Comunidades Ribeirinhas" },
                new ComunidadeTradicionalModelo { Id = 5, Nome = "Comunidades Rurais" },
                new ComunidadeTradicionalModelo { Id = 6, Nome = "Pescadores Artesanais" },
                new ComunidadeTradicionalModelo { Id = 7, Nome = "Povos de Terreiro" },
                new ComunidadeTradicionalModelo { Id = 8, Nome = "Outra" },
                new ComunidadeTradicionalModelo { Id = 9, Nome = "Nenhuma" }
            );

            modelBuilder.Entity<RaçaModelo>().HasData(
                new RaçaModelo { Id = 1, Nome = "Parda" },
                new RaçaModelo { Id = 2, Nome = "Branca" },
                new RaçaModelo { Id = 3, Nome = "Amarela" },
                new RaçaModelo { Id = 4, Nome = "Indígena" },
                new RaçaModelo { Id = 5, Nome = "Não Declarar" },
                new RaçaModelo { Id = 6, Nome = "Preta" }
            );

            modelBuilder.Entity<DeficiênciaModelo>().HasData(
                new DeficiênciaModelo { Id = 1, Nome = "Física" },
                new DeficiênciaModelo { Id = 2, Nome = "Auditiva" },
                new DeficiênciaModelo { Id = 3, Nome = "Visual" },
                new DeficiênciaModelo { Id = 4, Nome = "Intelectual" },
                new DeficiênciaModelo { Id = 5, Nome = "Múltipla" },
                new DeficiênciaModelo { Id = 6, Nome = "Nenhuma" }
            );

            modelBuilder.Entity<SegmentoCulturalModelo>().HasData(
                new SegmentoCulturalModelo { Id = 1, Nome = "Patrimônio Cultural" },
                new SegmentoCulturalModelo { Id = 2, Nome = "Artes Circenses" },
                new SegmentoCulturalModelo { Id = 3, Nome = "Artes de Dança" },
                new SegmentoCulturalModelo { Id = 4, Nome = "Artes de Teatro" },
                new SegmentoCulturalModelo { Id = 5, Nome = "Artes Visuais" },
                new SegmentoCulturalModelo { Id = 6, Nome = "Artesanato" },
                new SegmentoCulturalModelo { Id = 7, Nome = "Áudiovisual" },
                new SegmentoCulturalModelo { Id = 8, Nome = "Cultura Popular" },
                new SegmentoCulturalModelo { Id = 9, Nome = "Design" },
                new SegmentoCulturalModelo { Id = 10, Nome = "Fotografia" },
                new SegmentoCulturalModelo { Id = 11, Nome = "Gastronomia" },
                new SegmentoCulturalModelo { Id = 12, Nome = "Literatura" },
                new SegmentoCulturalModelo { Id = 13, Nome = "Moda" },
                new SegmentoCulturalModelo { Id = 14, Nome = "Música" },
                new SegmentoCulturalModelo { Id = 15, Nome = "Ópera" },
                new SegmentoCulturalModelo { Id = 16, Nome = "Outro" }
            );

            modelBuilder.Entity<FunçãoCulturalModelo>().HasData(
                new FunçãoCulturalModelo { Id = 1, Nome = "Artista, Artesão(ã), Bricante ou Cantor(a)" },
                new FunçãoCulturalModelo { Id = 2, Nome = "Consultor(a) ou Curador(a)" },
                new FunçãoCulturalModelo { Id = 3, Nome = "Produtor(a) ou Gestor(a)" },
                new FunçãoCulturalModelo { Id = 4, Nome = "Técnico(a)" },
                new FunçãoCulturalModelo { Id = 5, Nome = "Outro" }
            );

            modelBuilder.Entity<SituaçãoTrabalhoModelo>().HasData(
                new SituaçãoTrabalhoModelo { Id = 1, Nome = "Trabalho com carteira assinada" },
                new SituaçãoTrabalhoModelo { Id = 2, Nome = "Trabalho sem carteira assinada" },
                new SituaçãoTrabalhoModelo { Id = 3, Nome = "Sou autônomo" },
                new SituaçãoTrabalhoModelo { Id = 4, Nome = "Já trabalhei, mas atualmente estou desempregado" },
                new SituaçãoTrabalhoModelo { Id = 5, Nome = "Nunca trabalhei" }
            );

            modelBuilder.Entity<TipoContaBancáriaModelo>().HasData(
                new TipoContaBancáriaModelo { Id = 1, Nome = "Conta Corrente" },
                new TipoContaBancáriaModelo { Id = 2, Nome = "Conta Poupança" }
            );

            modelBuilder.Entity<BancoModelo>().HasData(
                new BancoModelo { Id = 1, Código = "001", Nome = "Banco do Brasil S.A." },
                new BancoModelo { Id = 2, Código = "237", Nome = "Banco Bradesco S.A." },
                new BancoModelo { Id = 3, Código = "104", Nome = "Caixa Econômica Federal" },
                new BancoModelo { Id = 4, Código = "745", Nome = "Banco Citibank S.A." },
                new BancoModelo { Id = 5, Código = "341", Nome = "Banco Itaú S.A." },
                new BancoModelo { Id = 6, Código = "652", Nome = "Itaú Unibanco Holding S.A." },
                new BancoModelo { Id = 7, Código = "422", Nome = "Banco Safra S.A." },
                new BancoModelo { Id = 8, Código = "033", Nome = "Banco Santander S.A." },
                new BancoModelo { Id = 9, Código = "260", Nome = "Nu Pagamentos S.A (Nubank)" },
                new BancoModelo { Id = 10, Código = "290", Nome = "Pagseguro Internet S.A" },
                new BancoModelo { Id = 11, Código = "323", Nome = "Mercado Pago" },
                new BancoModelo { Id = 12, Código = "237", Nome = "Banco Bradesco S.A / Next" },
                new BancoModelo { Id = 13, Código = "077", Nome = "Banco Inter" },
                new BancoModelo { Id = 14, Código = "336", Nome = "Banco C6 S.A – C6 Bank" }
            );
        }

        public static void AlimentarAdministrador(ModelBuilder modelBuilder)
        {
#if DEBUG
            var ambienteSingleton = new VariáveisAmbienteDesenvolvimentoSingleton();
            var ambiente = EnvironmentVariableTarget.User;
#else
            var ambienteSingleton = new VariáveisAmbienteProduçãoSingleton();
            var ambiente = EnvironmentVariableTarget.Process;
#endif
            var segurança = new SegurançaServiço(ambienteSingleton);

            modelBuilder.Entity<AdminModelo>().HasData(
                new AdminModelo { Id = 1, 
                    Email = Environment.GetEnvironmentVariable("EmailAdmin", ambiente), 
                    NomeCompleto = Environment.GetEnvironmentVariable("NomeAdmin", ambiente),
                    Senha = segurança.Criptografar(Environment.GetEnvironmentVariable("SenhaAdmin", ambiente))}
            );
        }

        public static void AlimentarDadosEspaçoCultural(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TipoDeAtividadeModelo>().HasData(
                new TipoDeAtividadeModelo { Id = 1, Nome = "Aulas e Cursos" },
                new TipoDeAtividadeModelo { Id = 2, Nome = "Apresentações Artísticas" },
                new TipoDeAtividadeModelo { Id = 3, Nome = "Exibição de Filmes" },
                new TipoDeAtividadeModelo { Id = 4, Nome = "Ensaios" },
                new TipoDeAtividadeModelo { Id = 5, Nome = "Palestras" },
                new TipoDeAtividadeModelo { Id = 6, Nome = "Exposição de Arte" },
                new TipoDeAtividadeModelo { Id = 7, Nome = "Outro" }
            );


            modelBuilder.Entity<SituaçãoBeneficiárioModelo>().HasData(
                new SituaçãoBeneficiárioModelo { Id = 1, Nome = "Espaço Alugado" },
                new SituaçãoBeneficiárioModelo { Id = 2, Nome = "Espaço Emprestado ou de Uso Compartilhado" },
                new SituaçãoBeneficiárioModelo { Id = 3, Nome = "Espaço Itinerante" },
                new SituaçãoBeneficiárioModelo { Id = 4, Nome = "Espaço Próprio" },
                new SituaçãoBeneficiárioModelo { Id = ConstantesRegistrosSingleton.EspaçoPúblicId, Nome = "Espaço Público" },
                new SituaçãoBeneficiárioModelo { Id = 7, Nome = "Espaço Público Cedido em Comodato" },
                new SituaçãoBeneficiárioModelo { Id = 8, Nome = "Espaço Virtual" }
            );


            modelBuilder.Entity<EquipamentoCulturalModelo>().HasData(
                new EquipamentoCulturalModelo { Id = 1, Nome = "Antiquários" },
                new EquipamentoCulturalModelo { Id = 2, Nome = "Arquivo" },
                new EquipamentoCulturalModelo { Id = 3, Nome = "Ateliê" },
                new EquipamentoCulturalModelo { Id = 4, Nome = "Biblioteca" },
                new EquipamentoCulturalModelo { Id = 5, Nome = "Casa de Cultura" },
                new EquipamentoCulturalModelo { Id = 6, Nome = "Casa de Eventos" },
                new EquipamentoCulturalModelo { Id = 7, Nome = "Centro Cultural" },
                new EquipamentoCulturalModelo { Id = 8, Nome = "Centro de Educação Musical" },
                new EquipamentoCulturalModelo { Id = 9, Nome = "Centro de Tradições" },
                new EquipamentoCulturalModelo { Id = 10, Nome = "Cinema ou Cineclube" },
                new EquipamentoCulturalModelo { Id = 11, Nome = "Circo" },
                new EquipamentoCulturalModelo { Id = 12, Nome = "Danceteria, Gafieira ou Casas de Dança" },
                new EquipamentoCulturalModelo { Id = 13, Nome = "Editora" },
                new EquipamentoCulturalModelo { Id = 14, Nome = "Escola de Artes" },
                new EquipamentoCulturalModelo { Id = 15, Nome = "Estúdio" },
                new EquipamentoCulturalModelo { Id = 16, Nome = "Feira ou Mercado Público" },
                new EquipamentoCulturalModelo { Id = 17, Nome = "Galeria de Arte" },
                new EquipamentoCulturalModelo { Id = 18, Nome = "Livraria ou Sebo" },
                new EquipamentoCulturalModelo { Id = 19, Nome = "Locadora de Vídeo" },
                new EquipamentoCulturalModelo { Id = 20, Nome = "Loja de Discos" },
                new EquipamentoCulturalModelo { Id = 21, Nome = "Museu ou Centro de Memória" },
                new EquipamentoCulturalModelo { Id = 22, Nome = "Parque de Diversões" },
                new EquipamentoCulturalModelo { Id = 23, Nome = "Parque de Vaquejada" },
                new EquipamentoCulturalModelo { Id = 24, Nome = "Pátio de Eventos" },
                new EquipamentoCulturalModelo { Id = 25, Nome = "Produtora" },
                new EquipamentoCulturalModelo { Id = 26, Nome = "Quadra de Escolas de Samba" },
                new EquipamentoCulturalModelo { Id = 27, Nome = "Teatro" },
                new EquipamentoCulturalModelo { Id = 28, Nome = "Terreiro" },
                new EquipamentoCulturalModelo { Id = 29, Nome = "Sede de grupo, entidade ou empresa" },
                new EquipamentoCulturalModelo { Id = 30, Nome = "Outro" }
            );

            modelBuilder.Entity<ReceitaAnualModelo>().HasData(
                new ReceitaAnualModelo { Id = 1, Nome = "R$0,00" },
                new ReceitaAnualModelo { Id = 2, Nome = "De R$0,01 a R$60.000,00" },
                new ReceitaAnualModelo { Id = 3, Nome = "De R$60.0000,01 a R$110.000,00" },
                new ReceitaAnualModelo { Id = 4, Nome = "De R$110.0000,01 a R$160.000,00" },
                new ReceitaAnualModelo { Id = 5, Nome = "De R$160.0000,01 a R$210.000,00" },
                new ReceitaAnualModelo { Id = 6, Nome = "De R$210.0000,01 a R$260.000,00" },
                new ReceitaAnualModelo { Id = 7, Nome = "De R$260.0000,01 a R$310.000,00" },
                new ReceitaAnualModelo { Id = 8, Nome = "De R$310.0000,01 a R$360.000,00" },
                new ReceitaAnualModelo { Id = 9, Nome = "Acima de 360.000,01" }
            );

            modelBuilder.Entity<GastoAnualModelo>().HasData(
                new ReceitaAnualModelo { Id = 1, Nome = "R$0,00" },
                new ReceitaAnualModelo { Id = 2, Nome = "De R$0,01 a R$60.000,00" },
                new ReceitaAnualModelo { Id = 3, Nome = "De R$60.0000,01 a R$110.000,00" },
                new ReceitaAnualModelo { Id = 4, Nome = "De R$110.0000,01 a R$160.000,00" },
                new ReceitaAnualModelo { Id = 5, Nome = "De R$160.0000,01 a R$210.000,00" },
                new ReceitaAnualModelo { Id = 6, Nome = "De R$210.0000,01 a R$260.000,00" },
                new ReceitaAnualModelo { Id = 7, Nome = "De R$260.0000,01 a R$310.000,00" },
                new ReceitaAnualModelo { Id = 8, Nome = "De R$310.0000,01 a R$360.000,00" },
                new ReceitaAnualModelo { Id = 9, Nome = "Acima de 360.000,01" }
            );

            modelBuilder.Entity<OrigemRecursosModelo>().HasData(
                new OrigemRecursosModelo {  Id = 1, Nome = "Programa Municipal de Incentivo à Cultura"},
                new OrigemRecursosModelo {  Id = 2, Nome = "Leis Municipais de Incentivo" },
                new OrigemRecursosModelo {  Id = 3, Nome = "Leis Estaduais de Incentivo" },
                new OrigemRecursosModelo {  Id = 4, Nome = "Lei Rouanet" },
                new OrigemRecursosModelo {  Id = 5, Nome = "Ancine" },
                new OrigemRecursosModelo {  Id = 6, Nome = "Outras Ações de Órgãos Públicos Nacionais" },
                new OrigemRecursosModelo {  Id = 7, Nome = "Arrecadação de Recursos Através da Internet" },
                new OrigemRecursosModelo {  Id = 8, Nome = "Venda Rifas, Almoço, Festas, etc." },
                new OrigemRecursosModelo {  Id = 9, Nome = "Patrocínio de Empresas Privadas" },
                new OrigemRecursosModelo {  Id = 10, Nome = "Mensalidades" },
                new OrigemRecursosModelo {  Id = 11, Nome = "Bilheteria" },
                new OrigemRecursosModelo {  Id = 12, Nome = "Doações e/ou Contribuições Espontâneas" },
                new OrigemRecursosModelo {  Id = 13, Nome = "Venda de Produtos/Serviços no Setor Cultural" },
                new OrigemRecursosModelo {  Id = 14, Nome = "Outro" }
            );

            modelBuilder.Entity<SituaçãoAtividadeModelo>().HasData(
                new SituaçãoAtividadeModelo {  Id = 1, Nome = "Adiada"},
                new SituaçãoAtividadeModelo {  Id = 2, Nome = "Cancelada"},
                new SituaçãoAtividadeModelo {  Id = 3, Nome = "Paralisada"},
                new SituaçãoAtividadeModelo {  Id = 4, Nome = "Indefinida"},
                new SituaçãoAtividadeModelo {  Id = 5, Nome = "Reduzida"}
            );

            modelBuilder.Entity<ModoFuncionamentoModelo>().HasData(
                new ModoFuncionamentoModelo {  Id = 1, Nome = "Aberto Gratuitamente ao Público Geral"},
                new ModoFuncionamentoModelo {  Id = 2, Nome = "Aberto Gratuitamente a um Público Específico"},
                new ModoFuncionamentoModelo {  Id = 3, Nome = "Mediante Mensalidade"},
                new ModoFuncionamentoModelo {  Id = 4, Nome = "Mediante Diária"},
                new ModoFuncionamentoModelo {  Id = 5, Nome = "Espaço Privado de Uso Particular"},
                new ModoFuncionamentoModelo {  Id = 6, Nome = "Prestação de Serviços Mediante Pagamento"},
                new ModoFuncionamentoModelo {  Id = 7, Nome = "Venda De Produtos"},
                new ModoFuncionamentoModelo {  Id = 8, Nome = "Outro"}
            );
        }
    }
}
