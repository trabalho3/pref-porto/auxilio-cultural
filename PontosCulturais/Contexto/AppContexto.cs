﻿using Microsoft.EntityFrameworkCore;
using PontosCulturais.Models;
using PontosCulturais.Models.CadastroEspaçoCultural;
using PontosCulturais.Models.CadastroPessoaFísica;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Contexto
{
    public class AppContexto : DbContext
    {
        public DbSet<BancoModelo> Bancos { get; set; }
        public DbSet<CadastroPessoaFísicaModelo> PessoasFísicas { get; set; }
        public DbSet<ComunidadeTradicionalModelo> ComunidadesTradicionais { get; set; }
        public DbSet<DeficiênciaModelo> Deficiências { get; set; }
        public DbSet<FunçãoCulturalModelo> FunçõesCulturais { get; set; }
        public DbSet<GêneroModelo> Gêneros { get; set; }
        public DbSet<RaçaModelo> Raças { get; set; }
        public DbSet<ResidenteModelo> Residentes { get; set; }
        public DbSet<SegmentoCulturalModelo> SegmentosCulturais { get; set; }
        public DbSet<SituaçãoTrabalhoModelo> SituaçõesTrabalho { get; set; }
        public DbSet<TipoContaBancáriaModelo> TiposContaBancária { get; set; }
        public DbSet<SegmentoCulturalPessoaFísicaModelo> SegmentoCulturalPessoaFísica { get; set; }
        public DbSet<DocumentosPessoaFísicaModelo> DocumentosPessoaFísica { get; set; }
        public DbSet<EspaçoCulturalSimplesModelo> EspaçosCulturais { get; set; }
        public DbSet<ReceitaAnualModelo> ReceitasAnuais { get; set; }
        public DbSet<GastoAnualModelo> GastosAnuais { get; set; }
        public DbSet<EspaçoCulturalEquipamentoCulturalModelo> EspaçoCulturalEquipamentoCultural { get; set; }
        public DbSet<EquipamentoCulturalModelo> EquipamentosCulturais { get; set; }
        public DbSet<EspaçoCulturalSegmentoCulturalModelo> EspaçoCulturalSegmentoCultural { get; set; }
        public DbSet<TipoDeAtividadeModelo> TiposAtividadesEspaçoCultural { get; set; }
        public DbSet<SituaçãoBeneficiárioModelo> SituaçãoBeneficiário { get; set; }
        public DbSet<OrigemRecursosModelo> OrigemRecursos { get; set; }
        public DbSet<OrigemRecursosEspaçoCulturalModelo> OrigemRecursosEspaçoCultural { get; set; }
        public DbSet<SituaçãoAtividadeModelo> SituaçõesAtividade { get; set; }
        public DbSet<ModoFuncionamentoModelo> ModosFuncionamento { get; set; }
        public DbSet<EspaçoCulturalModoFuncionamentoModelo> EspaçoCulturalModoFuncionamento { get; set; }
        public DbSet<EspaçoCulturalTiposAtividadeModelo> EspaçoCulturalTipoAtividade { get; set; }
        public DbSet<AnexosEspaçoCulturalModelo> EspaçoCulturalAnexos { get; set; }
        public DbSet<AdminModelo> Administradores { get; set; }


        public AppContexto()
        {
        }
        public AppContexto([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured) return;

#if DEBUG
            var ambiente = EnvironmentVariableTarget.User;
#else
            var ambiente = EnvironmentVariableTarget.Process;
#endif

            var servidor = Environment.GetEnvironmentVariable("ServidorSQLServer", ambiente);
            var usuário = Environment.GetEnvironmentVariable("UsuarioSQLServer", ambiente);
            var senha = Environment.GetEnvironmentVariable("SenhaSQLServer", ambiente);
            var baseDeDados = "AuxílioCultural";

            optionsBuilder.UseSqlServer($"Server={servidor};Database={baseDeDados};User Id={usuário};Password={senha};");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            AlimentarBaseDeDados.AlimentarDadosPessoaFísica(modelBuilder);
            AlimentarBaseDeDados.AlimentarDadosEspaçoCultural(modelBuilder);
            AlimentarBaseDeDados.AlimentarAdministrador(modelBuilder);

            var tabelaPessoasFísicas = modelBuilder.Entity<CadastroPessoaFísicaModelo>();

            tabelaPessoasFísicas.HasIndex(pf => pf.CPF).IsUnique();
            tabelaPessoasFísicas.HasIndex(pf => pf.Email).IsUnique();
            tabelaPessoasFísicas.HasIndex(pf => pf.RG).IsUnique();                
        }
    }
}
