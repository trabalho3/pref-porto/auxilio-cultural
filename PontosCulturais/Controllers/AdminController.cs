﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Repositórios.Interface;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;

namespace PontosCulturais.Controllers
{
    public class AdminController : Controller
    {
        private readonly ISegurançaServiço _segurançaServiço;
        private readonly ILoginServiço _loginServiço;
        private readonly IAdminRepositório _adminRepositório;
        private readonly IAvisoViewModelGerador _avisoViewModelGerador;

        public AdminController(ISegurançaServiço segurançaServiço, ILoginServiço loginServiço, IAdminRepositório adminRepositório, IAvisoViewModelGerador avisoViewModelGerador)
        {
            _segurançaServiço = segurançaServiço;
            _loginServiço = loginServiço;
            _adminRepositório = adminRepositório;
            _avisoViewModelGerador = avisoViewModelGerador;
        }

        public IActionResult Index()
        {
            return View(new AdminViewModel());
        }

        public async Task<IActionResult> Logar(AdminViewModel viewModel)
        {
            if(!ModelState.IsValid) return View(nameof(Index), viewModel);

            var admin = _adminRepositório.ChecarLogin(viewModel);
            if(admin == null)
            {
                var novoViewModel = new AdminViewModel { Aviso = _avisoViewModelGerador.GerarMensagemErro("Usuário não encontrado", Erros.UsuárioNãoEncontrado) };
                return View(nameof(Index), novoViewModel);
            }

            await _loginServiço.LogarAdministradorAsync(HttpContext, admin);

            return RedirectToAction(nameof(Painel));
        }

        [Authorize(Roles = ConstantesAutenticaçãoSingleton.Administrador)]
        public IActionResult Painel()
        {
            return View();
        }

        [Authorize(Roles = ConstantesAutenticaçãoSingleton.Administrador)]
        public async Task<IActionResult> Sair()
        {
            await _loginServiço.LogoutAsync(HttpContext);
            return RedirectToAction(nameof(Index));
        }
    }

}
