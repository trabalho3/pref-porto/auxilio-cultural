﻿using Microsoft.AspNetCore.Mvc;
using PontosCulturais.Contexto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Controllers.Base
{
    public abstract class EFControllerBase : Controller
    {
        private AppContexto _contexto;

        protected EFControllerBase(AppContexto contexto)
        {
            _contexto = contexto;
        }

        protected void SalvarAlterações() => _contexto.SaveChanges();
    }
}
