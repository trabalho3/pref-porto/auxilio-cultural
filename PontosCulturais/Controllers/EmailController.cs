﻿using Microsoft.AspNetCore.Mvc;
using PontosCulturais.Contexto;
using PontosCulturais.Controllers.Base;
using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Repositórios.Interface;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;

namespace PontosCulturais.Controllers
{
    public class EmailController : EFControllerBase
    {
        private readonly IEmailServiço _emailServiço;
        private readonly ISegurançaServiço _segurançaServiço;
        private readonly ICadastroPessoaFísicaRepositório _pessoaFísicaRepositório;
        private readonly IViewModelControllersGerador<EmailViewModel> _emailViewModelGerador;
        private readonly IAvisoViewModelGerador _avisoViewModelGerador;
        private readonly IValidarTokenServiço _validarTokenServiço;
        private readonly IValidarModeloServiço<RedefinirSenhaViewModel> _validarRedefinirSenhaViewModelServiço;
        private readonly IViewModelControllersGerador<RedefinirSenhaViewModel> _redefinirSenhaViewModelGerador;

        public EmailController(AppContexto contexto, IEmailServiço emailServiço, ISegurançaServiço segurançaServiço, ICadastroPessoaFísicaRepositório pessoaFísicaRepositório,
            IViewModelControllersGerador<EmailViewModel> emailViewModelGerador, IAvisoViewModelGerador avisoViewModelGerador, IValidarTokenServiço validarTokenServiço,
            IValidarModeloServiço<RedefinirSenhaViewModel> validarRedefinirSenhaViewModelServiço, IViewModelControllersGerador<RedefinirSenhaViewModel> redefinirSenhaViewModelGerador) 
        
        : base(contexto)
        {
            _emailServiço = emailServiço;
            _segurançaServiço = segurançaServiço;
            _pessoaFísicaRepositório = pessoaFísicaRepositório;
            _emailViewModelGerador = emailViewModelGerador;
            _avisoViewModelGerador = avisoViewModelGerador;
            _validarTokenServiço = validarTokenServiço;
            _validarRedefinirSenhaViewModelServiço = validarRedefinirSenhaViewModelServiço;
            _redefinirSenhaViewModelGerador = redefinirSenhaViewModelGerador;
        }

        public IActionResult ReenviarConfirmacao()
        {
            return View(_emailViewModelGerador.GerarViewModelInicial());
        }
        public IActionResult EsqueciSenha()
        {
            return View(_emailViewModelGerador.GerarViewModelInicial());
        }


        [HttpPost, AutoValidateAntiforgeryToken]
        public IActionResult ReenviarEmail(EmailViewModel viewModel) => EnviarEmail(viewModel.Email, PropósitoToken.ConfirmarEmail);

        [HttpPost, AutoValidateAntiforgeryToken]
        public IActionResult EnviarRedefinicao(EmailViewModel viewModel) => EnviarEmail(viewModel.Email, PropósitoToken.RedefinirSenha);

        [Route("{controller}/{action}/{token}")]
        public IActionResult ConfirmarEmail(string token)
        {
            var resultado = _validarTokenServiço.Validar(token, PropósitoToken.ConfirmarEmail);
            if(resultado.Item1.Erro)
            {
                var novoViewModel = _emailViewModelGerador.GerarViewModelInicial();
                novoViewModel.Aviso = resultado.Item1;

                return View(nameof(ReenviarConfirmacao), novoViewModel);
            }

            _pessoaFísicaRepositório.ConfirmarEmail(resultado.Item2);
            SalvarAlterações();

            return View("Sucesso");
        }

        [Route("{controller}/{action}/{token}")]
        public IActionResult RedefinirSenha(string token)
        {
            var resultado = _validarTokenServiço.Validar(token, PropósitoToken.RedefinirSenha);
            if(resultado.Item1.Erro)
            {
                var novoViewModel = _emailViewModelGerador.GerarViewModelInicial();
                novoViewModel.Aviso = resultado.Item1;

                return View(nameof(EsqueciSenha), novoViewModel);
            }

            var viewModel = _redefinirSenhaViewModelGerador.GerarViewModelInicial();
            viewModel.Token = token;

            return View(viewModel);
        }

        [HttpPost, AutoValidateAntiforgeryToken]
        public IActionResult AtualizarSenha(RedefinirSenhaViewModel viewModel)
        {
            var resultadoValidaçãoDados = _validarRedefinirSenhaViewModelServiço.Validar(viewModel);
            if(resultadoValidaçãoDados.Erro)
            {
                var novoViewModel = _redefinirSenhaViewModelGerador.GerarViewModelInicial();
                novoViewModel.Token = viewModel.Token;
                novoViewModel.Aviso = resultadoValidaçãoDados;

                return View(nameof(RedefinirSenha), novoViewModel);
            }

            var resultadoValidaçãoToken = _validarTokenServiço.Validar(viewModel.Token, PropósitoToken.RedefinirSenha);
            if(resultadoValidaçãoToken.Item1.Erro)
            {
                var novoViewModel = _redefinirSenhaViewModelGerador.GerarViewModelInicial();
                novoViewModel.Aviso = resultadoValidaçãoToken.Item1;
                novoViewModel.Token = viewModel.Token;

                return View(nameof(RedefinirSenha), novoViewModel);
            }

            _pessoaFísicaRepositório.AtualizarSenha(resultadoValidaçãoToken.Item2, _segurançaServiço.Criptografar(viewModel.Senha));
            SalvarAlterações();

            return View("Sucesso");
        }



        private IActionResult EnviarEmail(string email, PropósitoToken  propósito)
        {
            var id = _pessoaFísicaRepositório.AcharIdPorEmail(email);
            if (id == -1)
            {
                var novoViewModel = _emailViewModelGerador.GerarViewModelInicial();
                novoViewModel.Aviso = _avisoViewModelGerador.GerarMensagemErro("Usuário não encontrado", Erros.UsuárioNãoEncontrado);

                return View(nameof(ReenviarConfirmacao), novoViewModel);
            }

            var token = _segurançaServiço.GerarToken(id, propósito);

            if (propósito == PropósitoToken.ConfirmarEmail) _emailServiço.EnviarEmailConfirmação(token, email);
            else _emailServiço.EnviarEmailRedefinirSenha(token, email);

            var sucessoViewModel = _emailViewModelGerador.GerarViewModelInicial();
            sucessoViewModel.Aviso = _avisoViewModelGerador.GerarMensagemSucesso("E-mail enviando");

            return View(nameof(ReenviarConfirmacao), sucessoViewModel);
        }
    }
}
