﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PontosCulturais.Contexto;
using PontosCulturais.Controllers.Base;
using PontosCulturais.Exceptions.Base;
using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Models;
using PontosCulturais.Models.CadastroEspaçoCultural;
using PontosCulturais.Repositórios.Interface;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;

namespace PontosCulturais.Controllers
{
    public class EspacoCulturalController : EFControllerBase
    {
        //Geradores
        private readonly IViewModelControllersGerador<CadastroEspaçoCulturalViewModel> _geradorEspaçoCulturalViewModel;
        private readonly IAvisoViewModelGerador _geradorAvisoViewModel;
        private readonly IViewModelControllersGerador<AnexosEspaçoCulturalViewModel> _geradorAnexosEspaçoCulturalViewModel;
        private readonly IMostrarEspaçoCulturalViewModelGerador _mostrarEspaçoCulturalViewModelGerador;

        //Repositórios
        private readonly IOpçõesCadastroEspaçoCulturalRepositório _opçõesCadastroEspaçoCulturalRepositório;
        private readonly ISegmentosCulturaisRepositório _segmentosCulturaisRepositório;
        private readonly IEspaçoCulturalRepositório _espaçoCulturalRepositório;
        private readonly IAnexosEspaçoCulturalRepositório _repositórioAnexosEspaçoCultural;

        //Serviços - Validação
        private readonly IValidarModeloServiço<CadastroEspaçoCulturalViewModel> _validarEspaçoCadastroCulturalServiço;
        private readonly IValidarModeloServiço<AnexosEspaçoCulturalViewModel> _validarAnexosEspaçoViewModel;

        //Serviços - Outros
        private readonly IOpçõesCadastroParaTextoServiço _opçõesCadastroParaTextoServiço;
        private readonly IUploadServiço _serviçoUpload;

        public EspacoCulturalController(AppContexto contexto, IViewModelControllersGerador<CadastroEspaçoCulturalViewModel> geradorEspaçoCulturalViewModel, IOpçõesCadastroEspaçoCulturalRepositório opçõesCadastroEspaçoCulturalRepositório, 
            ISegmentosCulturaisRepositório segmentosCulturaisRepositório, IValidarModeloServiço<CadastroEspaçoCulturalViewModel> validarEspaçoCadastroCulturalServiço, 
            IEspaçoCulturalRepositório espaçoCulturalRepositório, IAvisoViewModelGerador geradorAvisoViewModel, IMostrarEspaçoCulturalViewModelGerador mostrarEspaçoCulturalViewModelGerador,
            IOpçõesCadastroParaTextoServiço opçõesCadastroParaTextoServiço, IViewModelControllersGerador<AnexosEspaçoCulturalViewModel> geradorAnexosEspaçoCulturalViewModel,
            IValidarModeloServiço<AnexosEspaçoCulturalViewModel> validarAnexosEspaçoViewModel, IUploadServiço serviçoUpload, IAnexosEspaçoCulturalRepositório repositórioAnexosEspaçoCultural)
            : base(contexto)
        {
            _geradorEspaçoCulturalViewModel = geradorEspaçoCulturalViewModel;
            _opçõesCadastroEspaçoCulturalRepositório = opçõesCadastroEspaçoCulturalRepositório;
            _segmentosCulturaisRepositório = segmentosCulturaisRepositório;
            _validarEspaçoCadastroCulturalServiço = validarEspaçoCadastroCulturalServiço;
            _espaçoCulturalRepositório = espaçoCulturalRepositório;
            _geradorAvisoViewModel = geradorAvisoViewModel;
            _mostrarEspaçoCulturalViewModelGerador = mostrarEspaçoCulturalViewModelGerador;
            _opçõesCadastroParaTextoServiço = opçõesCadastroParaTextoServiço;
            _geradorAnexosEspaçoCulturalViewModel = geradorAnexosEspaçoCulturalViewModel;
            _validarAnexosEspaçoViewModel = validarAnexosEspaçoViewModel;
            _serviçoUpload = serviçoUpload;
            _repositórioAnexosEspaçoCultural = repositórioAnexosEspaçoCultural;
        }

        [Authorize(Roles = ConstantesAutenticaçãoSingleton.CandidatoAoAuxílio)]
        public IActionResult Index()
        {
            if (_espaçoCulturalRepositório.ChecarPeloIdUsuário(RetornarId())) return RedirectToAction(nameof(MostrarEspaco));

            var viewModel = _geradorEspaçoCulturalViewModel.GerarViewModelInicial();
            AlimentarViewModel(viewModel);
            return View(viewModel);
        }

        [HttpPost][ValidateAntiForgeryToken][Authorize(Roles = ConstantesAutenticaçãoSingleton.CandidatoAoAuxílio)]
        public IActionResult InserirEspaco(CadastroEspaçoCulturalViewModel viewModel)
        {
            var resultado = _validarEspaçoCadastroCulturalServiço.Validar(viewModel);
            if(resultado.Erro)
            {
                var novoViewModel = _geradorEspaçoCulturalViewModel.GerarViewModelInicial();
                AlimentarViewModel(novoViewModel);
                novoViewModel.Aviso = resultado;

                return View(nameof(Index), novoViewModel);
            }

            if(!string.IsNullOrEmpty(viewModel.EspaçoCultural.Cnpj) && !_espaçoCulturalRepositório.VerificarCNPJÚnico(viewModel.EspaçoCultural.Cnpj))
            {
                var novoViewModel = _geradorEspaçoCulturalViewModel.GerarViewModelInicial();
                AlimentarViewModel(novoViewModel);
                novoViewModel.Aviso = _geradorAvisoViewModel.GerarMensagemErro("CNPJ já usado", Erros.CNPJRepetido);

                return View(nameof(Index), novoViewModel);
            }

            viewModel.EspaçoCultural.PessoaFísicaId = RetornarId();

            _espaçoCulturalRepositório.Inserir(viewModel.EspaçoCultural);
            _opçõesCadastroEspaçoCulturalRepositório.Inserir(viewModel);
            SalvarAlterações();

            return RedirectToAction(nameof(MostrarEspaco));

        }

        [Authorize(Roles = ConstantesAutenticaçãoSingleton.CandidatoAoAuxílio)]
        public IActionResult MostrarEspaco()
        {
            var id = RetornarId();

            var espaço = _espaçoCulturalRepositório.RetornarPorId(id);
            var atividades = _opçõesCadastroEspaçoCulturalRepositório.RetornarAtividadesPorIdPessoaFísica(id);
            var origens = _opçõesCadastroEspaçoCulturalRepositório.RetornarOrigensPorIdPessoaFísica(id);
            var funcionamentos = _opçõesCadastroEspaçoCulturalRepositório.RetornarFuncionamentosPorIdPessoaFísica(id);
            var segmentos = _opçõesCadastroEspaçoCulturalRepositório.RetornarSegmentosPorIdPessoaFísica(id);
            var equipamentos = _opçõesCadastroEspaçoCulturalRepositório.RetornarEquipamentosPorIdPessoaFísica(id);

            var viewModel = _mostrarEspaçoCulturalViewModelGerador.GerarViewModelInicial(_opçõesCadastroParaTextoServiço, espaço, atividades, origens, funcionamentos, segmentos, equipamentos);

            return View(viewModel);
        }

        [Authorize(Roles = ConstantesAutenticaçãoSingleton.CandidatoAoAuxílio)]
        public IActionResult Anexos()
        {
            if (_repositórioAnexosEspaçoCultural.VerificarSeJáCadastrou(RetornarId())) return View("Sucesso");

            return View(_geradorAnexosEspaçoCulturalViewModel.GerarViewModelInicial());
        }

        [Authorize(Roles = ConstantesAutenticaçãoSingleton.CandidatoAoAuxílio)]
        public async Task<IActionResult> InserirAnexos(AnexosEspaçoCulturalViewModel viewModel)
        {
            var id = RetornarId();
            var espaçoCultural = _espaçoCulturalRepositório.RetornarPorIdSemIncludesPodendoSerNulo(id);

            if(espaçoCultural == null)
            {
                var novoViewModel = _geradorAnexosEspaçoCulturalViewModel.GerarViewModelInicial();
                novoViewModel.Aviso = _geradorAvisoViewModel.GerarMensagemErro("Cadastre um espaço cultural antes", Erros.AnexoAntesEspaço);

                return View(nameof(Anexos), novoViewModel);
            }

            var resultado = _validarAnexosEspaçoViewModel.Validar(viewModel);
            if(resultado.Erro)
            {
                var novoViewModel = _geradorAnexosEspaçoCulturalViewModel.GerarViewModelInicial();
                novoViewModel.Aviso = resultado;

                return View(nameof(Anexos), novoViewModel);
            }

            if(string.IsNullOrEmpty(espaçoCultural.Cnpj) ^ viewModel.ComprovanteCNPJ == null)
            {
                var novoViewModel = _geradorAnexosEspaçoCulturalViewModel.GerarViewModelInicial();
                novoViewModel.Aviso = _geradorAvisoViewModel.GerarMensagemErro("Há uma inconsistência entre o CNPJ informado e o comprovante anexado", Erros.ComprovanteCNPJVazio);

                return View(nameof(Anexos), novoViewModel);
            }

            if(!espaçoCultural.PossuiAlvaráDeFuncionamento ^ viewModel.ComprovanteAlvará == null)
            {
                var novoViewModel = _geradorAnexosEspaçoCulturalViewModel.GerarViewModelInicial();
                novoViewModel.Aviso = _geradorAvisoViewModel.GerarMensagemErro("Há uma inconsistência com o alvará de funcionamento", Erros.AlvaráVazio);

                return View(nameof(Anexos), novoViewModel);
            }

            if(!espaçoCultural.DocumentoVínculoEmpregatícioFuncionários ^ viewModel.ComprovanteFuncionários == null)
            {
                var novoViewModel = _geradorAnexosEspaçoCulturalViewModel.GerarViewModelInicial();
                novoViewModel.Aviso = _geradorAvisoViewModel.GerarMensagemErro("Há uma inconsistência com o comprovante de vínculo empregatício", Erros.ComprovanteFuncionáriosVazio);

                return View(nameof(Anexos), novoViewModel);
            }

            var caminhoComprovanteEndereço = "";
            var caminhoComprovanteCNPJ = "";
            var caminhoComprovanteAtividades = "";
            var caminhoComprovanteFuncionários = "";
            var caminhoComprovanteAlvará = "";

            try
            {
                var idString = id.ToString();

                caminhoComprovanteEndereço = await _serviçoUpload.FazerUpload(viewModel.ComprovanteEndereço, idString, ConstantesUploadSingleton.ExtensõesDocumentos);
                caminhoComprovanteCNPJ = viewModel.ComprovanteCNPJ != null ? await _serviçoUpload.FazerUpload(viewModel.ComprovanteCNPJ, idString, ConstantesUploadSingleton.ExtensõesDocumentos) : "";
                caminhoComprovanteAtividades = await _serviçoUpload.FazerUpload(viewModel.ComprovanteAtividades, idString, ConstantesUploadSingleton.ExtensõesAtividades);
                caminhoComprovanteFuncionários = viewModel.ComprovanteFuncionários != null ?
                    await _serviçoUpload.FazerUpload(viewModel.ComprovanteFuncionários, idString, ConstantesUploadSingleton.ExtensõesDocumentos) : "";

                caminhoComprovanteAlvará = viewModel.ComprovanteAlvará != null ? await _serviçoUpload.FazerUpload(viewModel.ComprovanteAlvará, idString, ConstantesUploadSingleton.ExtensõesDocumentos) : "";

                var modelo = new AnexosEspaçoCulturalModelo
                {
                    PessoaFísicaId = id,
                    ComprovanteAlvará = caminhoComprovanteAlvará,
                    ComprovanteAtuação = caminhoComprovanteAtividades,
                    ComprovanteCNPJ = caminhoComprovanteCNPJ,
                    ComprovanteEndereço = caminhoComprovanteEndereço,
                    ComprovanteFuncionários = caminhoComprovanteFuncionários
                };

                _repositórioAnexosEspaçoCultural.Inserir(modelo);
                SalvarAlterações();

                return View("Sucesso");
            }
            catch (Exception e)
            {

                DeletarSeExistir(caminhoComprovanteEndereço);
                DeletarSeExistir(caminhoComprovanteCNPJ);
                DeletarSeExistir(caminhoComprovanteAlvará);
                DeletarSeExistir(caminhoComprovanteAtividades);
                DeletarSeExistir(caminhoComprovanteAtividades);

                if(e is ExceptionBase)
                {
                    var novoViewModel = _geradorAnexosEspaçoCulturalViewModel.GerarViewModelInicial();
                    novoViewModel.Aviso = _geradorAvisoViewModel.GerarMensagemErro(e.Message, Erros.ExceçãoLançada);

                    return View(nameof(Anexos), novoViewModel);
                }

                throw;
            }

        }

        private void DeletarSeExistir(string caminho)
        {
            if (string.IsNullOrEmpty(caminho)) return;
            if (!System.IO.File.Exists(caminho)) return;

            System.IO.File.Delete(caminho);

        }

        private void AlimentarViewModel(CadastroEspaçoCulturalViewModel viewModel)
        {
            viewModel.TiposAtividades = _opçõesCadastroEspaçoCulturalRepositório.RetornarTiposAtividades();
            viewModel.GastosAnuais = _opçõesCadastroEspaçoCulturalRepositório.RetornarDespesasAnuais();
            viewModel.EquipamentosCulturais = _opçõesCadastroEspaçoCulturalRepositório.RetornarEquipamentosCulturais();
            viewModel.SituaçõesBeneficiário = _opçõesCadastroEspaçoCulturalRepositório.RetornarSituaçõesImóvel();
            viewModel.ReceitasAnuais = _opçõesCadastroEspaçoCulturalRepositório.RetornarReceitasAnuais();
            viewModel.OrigensRecursos = _opçõesCadastroEspaçoCulturalRepositório.RetornarOrigemRecursos();
            viewModel.SituaçõesAtividade = _opçõesCadastroEspaçoCulturalRepositório.RetornarSituaçõesAtividades();
            viewModel.ModosFuncionamento = _opçõesCadastroEspaçoCulturalRepositório.RetornarModosFuncionamento();
            viewModel.SegmentosCulturais = _segmentosCulturaisRepositório.RetornarTodos();
        }

        private int RetornarId() => int.Parse(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).First().Value);
    }
}
