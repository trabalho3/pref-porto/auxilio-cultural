﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Models;
using PontosCulturais.Repositórios.Interface;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;

namespace PontosCulturais.Controllers
{
    public class PainelController : Controller
    {
        private readonly IViewModelControllersGerador<LoginViewModel> _geradorLoginViewModel;
        private readonly IAvisoViewModelGerador _geradorAvisoViewModel;

        private readonly ICadastroPessoaFísicaRepositório _repositórioPessoaFísica;

        private readonly ISegurançaServiço _criptografiaServiço;
        private readonly ILoginServiço _loginServiço;

        public PainelController(IViewModelControllersGerador<LoginViewModel> geradorLoginViewModel, IAvisoViewModelGerador geradorAvisoViewModel,
            ICadastroPessoaFísicaRepositório repositórioPessoaFísica, ISegurançaServiço criptografiaServiço, ILoginServiço loginServiço)
        {
            _geradorLoginViewModel = geradorLoginViewModel;
            _geradorAvisoViewModel = geradorAvisoViewModel;
            _repositórioPessoaFísica = repositórioPessoaFísica;
            _criptografiaServiço = criptografiaServiço;
            _loginServiço = loginServiço;
        }

        public IActionResult Entrar()
        {
            if (User.Identity.IsAuthenticated) return RedirectToAction(nameof(Mostrar));
            return View(_geradorLoginViewModel.GerarViewModelInicial());
        }

        public async Task<IActionResult> FazerLogin(LoginViewModel viewModel)
        {

            if (string.IsNullOrEmpty(viewModel.Email) || string.IsNullOrEmpty(viewModel.Senha))
            {
                viewModel.Aviso = _geradorAvisoViewModel.GerarMensagemErro("Nenhum dos dois campos pode estar vazio", Erros.CamposLoginVazios);
                return View(nameof(Entrar), viewModel);
            }

            var senhaCriptografada = _criptografiaServiço.Criptografar(viewModel.Senha);
            var campo = _repositórioPessoaFísica.Logar(viewModel.Email, senhaCriptografada);

            if (campo == null)
            {
                viewModel.Aviso = _geradorAvisoViewModel.GerarMensagemErro("Usuário não encontrado", Erros.UsuárioNãoEncontrado);
                viewModel.Senha = "";

                return View(nameof(Entrar), viewModel);
            }

            if(!campo.EmailConfirmado)
            {
                viewModel.Aviso = _geradorAvisoViewModel.GerarMensagemErro("E-mail não confirmado", Erros.EmailNãoConfirmado);
                viewModel.Senha = "";

                return View(nameof(Entrar), viewModel);
            }

            await _loginServiço.LogarCandidatoAoAuxílioAsync(HttpContext, campo);

            return RedirectToAction(nameof(Mostrar));
        }

        [Authorize(Roles = ConstantesAutenticaçãoSingleton.CandidatoAoAuxílio)]
        public IActionResult Mostrar()
        {
            var usuário = ObterUsuário();
            return View(usuário.CadastrarEspaçoCultural);
        }

        [Authorize(Roles = ConstantesAutenticaçãoSingleton.CandidatoAoAuxílio)]
        public async Task<IActionResult> Sair()
        {
            await _loginServiço.LogoutAsync(HttpContext);
            return RedirectToAction(nameof(Entrar));
        }

        public IActionResult Negado()
        {
            return View();
        }

        private CadastroPessoaFísicaModelo ObterUsuário()
        {
            var id = int.Parse(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).First().Value);
            var usuário = _repositórioPessoaFísica.RetornarPorId(id);

            return usuário;
        }
    }
}
