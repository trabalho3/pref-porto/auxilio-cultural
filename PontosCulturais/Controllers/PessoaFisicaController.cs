﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PontosCulturais.Contexto;
using PontosCulturais.Controllers.Base;
using PontosCulturais.Exceptions.Base;
using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.Repositórios.Interface;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons;
using PontosCulturais.Singletons.Interfaces;
using PontosCulturais.ViewModels;

namespace PontosCulturais.Controllers
{
    public class PessoaFisicaController : EFControllerBase
    {
        //Repositórios
        private readonly IOpçõesCadastroPessoaFísicaRepositório _repositórioOpçõesCadastro;
        private readonly ICadastroPessoaFísicaRepositório _repositórioCadastroPessoaFísica;
        private readonly ISegmentosCulturaisRepositório _repositórioSegmentosCulturais;
        private readonly IResidentesRepositório _repositórioResidentes;
        private readonly IDocumentosPessoaFísicaRepositório _repositórioDocumentos;

        //Serviços
        private readonly ISegurançaServiço _criptografiaServiço;
        private readonly IUploadServiço _uploadServiço;
        private readonly IEmailServiço _emailServiço;

        //Validar
        private readonly IValidarModeloServiço<CadastroPessoaFísicaModelo> _validarPessoaFísica;
        private readonly IValidarModeloServiço<ResidenteModelo> _validarResidente;
        private readonly IValidarModeloServiço<DocumentosPessoaFísicaViewModel> _validarDocumentosViewModel;

        //Geradores
        private readonly IViewModelControllersGerador<CadastroPessoaFísicaViewModel> _geradorCadastroViewModel;
        private readonly IAvisoViewModelGerador _geradorAvisos;
        private readonly IResidentesViewModelGerador _geradorResidentesViewModel;
        private readonly IMostrarDadosViewModelGerador _geradorMostrarDadosViewModel;
        private readonly IViewModelControllersGerador<DocumentosPessoaFísicaViewModel> _geradorDocumentosViewModel;

        public PessoaFisicaController(AppContexto contexto, IViewModelControllersGerador<CadastroPessoaFísicaViewModel> geradorCadastroViewModel, IOpçõesCadastroPessoaFísicaRepositório repositórioOpçõesCadastro, 
            IValidarModeloServiço<CadastroPessoaFísicaModelo> validarPessoaFísica, IAvisoViewModelGerador geradorAvisos, ICadastroPessoaFísicaRepositório repositórioCadastroPessoaFísica,
            ISegmentosCulturaisRepositório repositórioSegmentosCulturais, ISegurançaServiço criptografiaServiço, IResidentesRepositório repositórioRedidentes, 
            IResidentesViewModelGerador geradorResidentesViewModel, IValidarModeloServiço<ResidenteModelo> validarResidente, IMostrarDadosViewModelGerador geradorMostrarDadosViewModel,
            IViewModelControllersGerador<DocumentosPessoaFísicaViewModel> geradorDocumentosViewModel, IUploadServiço uploadServiço, IValidarModeloServiço<DocumentosPessoaFísicaViewModel> validarDocumentosViewModel,
            IDocumentosPessoaFísicaRepositório repositórioDocumentos, IEmailServiço emailServiço) 
            : base(contexto)
        {
            _geradorCadastroViewModel = geradorCadastroViewModel;
            _repositórioOpçõesCadastro = repositórioOpçõesCadastro;
            _validarPessoaFísica = validarPessoaFísica;
            _geradorAvisos = geradorAvisos;
            _repositórioCadastroPessoaFísica = repositórioCadastroPessoaFísica;
            _repositórioSegmentosCulturais = repositórioSegmentosCulturais;
            _criptografiaServiço = criptografiaServiço;
            _repositórioResidentes = repositórioRedidentes;
            _geradorResidentesViewModel = geradorResidentesViewModel;
            _validarResidente = validarResidente;
            _geradorMostrarDadosViewModel = geradorMostrarDadosViewModel;
            _geradorDocumentosViewModel = geradorDocumentosViewModel;
            _uploadServiço = uploadServiço;
            _validarDocumentosViewModel = validarDocumentosViewModel;
            _repositórioDocumentos = repositórioDocumentos;
            _emailServiço = emailServiço;

        }

        public IActionResult Cadastrar()
        {

            var viewModel = _geradorCadastroViewModel.GerarViewModelInicial();
            CarregarListasModelo(viewModel);
            return View(viewModel);
        }


        [HttpPost] [ValidateAntiForgeryToken]
        public IActionResult Finalizar(CadastroPessoaFísicaViewModel modelo)
        {
            var resultado = _validarPessoaFísica.Validar(modelo.Cadastro);
            if (resultado.Erro)
            {
                CarregarListasModelo(modelo);
                modelo.Aviso = resultado;
                return View(nameof(Cadastrar), modelo);
            }

            if(!_repositórioCadastroPessoaFísica.VerificarCPFÚnico(modelo.Cadastro.CPF))
            {
                CarregarListasModelo(modelo);
                modelo.Aviso = _geradorAvisos.GerarMensagemErro("CPF já usado", Erros.CPFRepetidoCadastroCadastro);
                return View(nameof(Cadastrar), modelo);
            }

            if(!_repositórioCadastroPessoaFísica.VerificarEmailÚnico(modelo.Cadastro.Email))
            {
                CarregarListasModelo(modelo);
                modelo.Aviso = _geradorAvisos.GerarMensagemErro("Email já usado", Erros.EmailRepetido);
                return View(nameof(Cadastrar), modelo);
            }

            if(!_repositórioCadastroPessoaFísica.VerificarRGÚnico(modelo.Cadastro.RG))
            {
                CarregarListasModelo(modelo);
                modelo.Aviso = _geradorAvisos.GerarMensagemErro("RG já usado", Erros.RGRepetido);
                return View(nameof(Cadastrar), modelo);
            }

            if(!_repositórioResidentes.VerificarCPFCertidãoNãoCadastrado(modelo.Cadastro.CPF))
            {
                CarregarListasModelo(modelo);
                modelo.Aviso = _geradorAvisos.GerarMensagemErro("CPF já usada como residente em outro cadastro", Erros.CPFRepetidoCadastroResidente);
                return View(nameof(Cadastrar), modelo);
            }

            modelo.Cadastro.Senha = _criptografiaServiço.Criptografar(modelo.Cadastro.Senha);
            _repositórioCadastroPessoaFísica.Inserir(modelo.Cadastro);
            var segmentos = _repositórioSegmentosCulturais.RetornarSegmentos(modelo.Cadastro.SegmentosCulturaisId);
            var entradasRelacionamento = segmentos.Select(sc => new SegmentoCulturalPessoaFísicaModelo { PessoaFísica = modelo.Cadastro, SegmentoCultural = sc });
            _repositórioSegmentosCulturais.InserirRelacionamentos(entradasRelacionamento);
            SalvarAlterações();

            var token = _criptografiaServiço.GerarToken(modelo.Cadastro.Id, PropósitoToken.ConfirmarEmail);
            _emailServiço.EnviarEmailConfirmação(token, modelo.Cadastro.Email);

            return View("Sucesso");
        }

        [Authorize(Roles = ConstantesAutenticaçãoSingleton.CandidatoAoAuxílio)]
        public IActionResult Residentes()
        {
            var idUsuário = RetornarID();
            var residentes = _repositórioResidentes.RetornarResidentes(idUsuário);
            var viewModel = _geradorResidentesViewModel.GerarViewModelInicial(residentes);

            return View(viewModel);
        }



        [HttpPost] [ValidateAntiForgeryToken] [Authorize(Roles = ConstantesAutenticaçãoSingleton.CandidatoAoAuxílio)]
        public IActionResult CadastrarResidente(ResidentesViewModel viewModel)
        {
            var resultado = _validarResidente.Validar(viewModel.Residente);
            var idUsuário = RetornarID();

            if (resultado.Erro)
            {
                var residentes = _repositórioResidentes.RetornarResidentes(idUsuário);
                var novoViewModel = _geradorResidentesViewModel.GerarComAviso(residentes, resultado);
                return View(nameof(Residentes), novoViewModel);
            }

            if(!_repositórioCadastroPessoaFísica.VerificarCPFÚnico(viewModel.Residente.CPFOuCertidão))
            {
                var residentes = _repositórioResidentes.RetornarResidentes(idUsuário);
                var aviso = _geradorAvisos.GerarMensagemErro("CPF já cadastrado como usuário pessoa física", Erros.CPFRepetidoResidenteCadastro);
                var novoViewModel = _geradorResidentesViewModel.GerarComAviso(residentes, aviso);

                return View(nameof(Residentes), novoViewModel);
            }

            if(!_repositórioResidentes.VerificarCPFCertidãoNãoCadastrado(viewModel.Residente.CPFOuCertidão))
            {
                var residentes = _repositórioResidentes.RetornarResidentes(idUsuário);
                var aviso = _geradorAvisos.GerarMensagemErro("CPF/Registro de Nascimento já cadastrado por você ou outra pessoa", Erros.CPFRepetidoResidenteResidente);
                var novoViewModel = _geradorResidentesViewModel.GerarComAviso(residentes, aviso);

                return View(nameof(Residentes), novoViewModel);
            }

            viewModel.Residente.CadastroPessoaFísicaModeloId = idUsuário;
            _repositórioResidentes.Inserir(viewModel.Residente);
            SalvarAlterações();

            return RedirectToAction(nameof(Residentes));
        }

        [Authorize(Roles = ConstantesAutenticaçãoSingleton.CandidatoAoAuxílio)]
        public IActionResult Visualizar()
        {
            var id = RetornarID();
            var usuário = _repositórioCadastroPessoaFísica.RetornarPorIdComJoins(id);
            var segmentos = _repositórioSegmentosCulturais.RetornarSegmentosPessoaFísica(id);
            var viewModel = _geradorMostrarDadosViewModel.GerarViewModelInicial(usuário, segmentos);

            return View(viewModel);
        }

        [Authorize(Roles = ConstantesAutenticaçãoSingleton.CandidatoAoAuxílio)]
        public IActionResult Anexos()
        {
            if (_repositórioDocumentos.ChecarUsuárioJáMandou(RetornarID())) return View("DocumentosEnviados");

            var viewModel = _geradorDocumentosViewModel.GerarViewModelInicial();
            return View(viewModel);
        }

        [HttpPost] [ValidateAntiForgeryToken] [Authorize(Roles = ConstantesAutenticaçãoSingleton.CandidatoAoAuxílio)]
        public async Task<IActionResult> InserirAnexos(DocumentosPessoaFísicaViewModel viewModel)
        {
            var resultado = _validarDocumentosViewModel.Validar(viewModel);
            if(resultado.Erro)
            {
                var novoViewModel = _geradorDocumentosViewModel.GerarViewModelInicial();
                novoViewModel.Aviso = resultado;
                return View(nameof(Anexos), novoViewModel);
            }
            var id = RetornarID();
            var idString = id.ToString();

            string caminhoFrenteRGCNH = "";
            string caminhoVersoRGCNH = "";
            string caminhoSelfie = "";
            string caminhoComprovanteCPF = "";
            string caminhoComprovanteEndereço = "";
            string caminhoComprovanteBancário = "";
            string caminhoComprovanteAtividades = "";

            try
            {
                caminhoFrenteRGCNH = await _uploadServiço.FazerUpload(viewModel.FrenteRGCNH, idString, ConstantesUploadSingleton.ExtensõesDocumentos);
                caminhoVersoRGCNH = await _uploadServiço.FazerUpload(viewModel.VersoRGCNH, idString, ConstantesUploadSingleton.ExtensõesDocumentos);
                caminhoSelfie = await _uploadServiço.FazerUpload(viewModel.Selfie, idString, ConstantesUploadSingleton.ExtensõesFoto);
                caminhoComprovanteCPF = await _uploadServiço.FazerUpload(viewModel.CPF, idString, ConstantesUploadSingleton.ExtensõesDocumentos);
                caminhoComprovanteEndereço = await _uploadServiço.FazerUpload(viewModel.ComprovanteEndereço, idString, ConstantesUploadSingleton.ExtensõesDocumentos);
                caminhoComprovanteBancário = viewModel.ComprovanteBancário != null ? await _uploadServiço.FazerUpload(viewModel.ComprovanteBancário, idString, ConstantesUploadSingleton.ExtensõesDocumentos) : "";
                caminhoComprovanteAtividades = await _uploadServiço.FazerUpload(viewModel.ComprovanteAtividades, idString, ConstantesUploadSingleton.ExtensõesAtividades);

                var modelo = new DocumentosPessoaFísicaModelo
                {
                    ComprovanteBancário = caminhoComprovanteBancário,
                    ComprovanteCPF = caminhoComprovanteCPF,
                    ComprovanteEndereço = caminhoComprovanteEndereço,
                    FrenteRGCNH = caminhoFrenteRGCNH,
                    VersoRGCNG = caminhoVersoRGCNH,
                    ComprovanteAtividades = caminhoComprovanteAtividades,
                    Selfie = caminhoSelfie,
                    PessoaFísicaId = id
                };

                _repositórioDocumentos.Inserir(modelo);
                SalvarAlterações();
                return View("DocumentosEnviados");
            }
            catch(Exception e)
            {
                DeletarSeExistir(caminhoFrenteRGCNH);
                DeletarSeExistir(caminhoVersoRGCNH);
                DeletarSeExistir(caminhoSelfie);
                DeletarSeExistir(caminhoComprovanteCPF);
                DeletarSeExistir(caminhoComprovanteEndereço);
                DeletarSeExistir(caminhoComprovanteBancário);
                DeletarSeExistir(caminhoComprovanteAtividades);

                if(e is ExceptionBase)
                {
                    var aviso = _geradorAvisos.GerarMensagemErro(e.Message, Erros.ExceçãoLançada);
                    var novoViewModel = _geradorDocumentosViewModel.GerarViewModelInicial();

                    novoViewModel.Aviso = aviso;
                    return View(nameof(Anexos), novoViewModel);
                }
                else
                {
                    throw;
                }
            }
        }

        private int RetornarID() => int.Parse(User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).First().Value);

        private void CarregarListasModelo(CadastroPessoaFísicaViewModel model)
        {
            model.Gêneros = _repositórioOpçõesCadastro.RetornarGêneros();
            model.Raças = _repositórioOpçõesCadastro.RetornarRaças();
            model.Deficiências = _repositórioOpçõesCadastro.RetornarDeficiências();
            model.FunçõesCulturais = _repositórioOpçõesCadastro.RetornarFunçõesCulturais();
            model.Comunidades = _repositórioOpçõesCadastro.RetornarComunidadesTradicionais();
            model.SituaçõesTrabalho = _repositórioOpçõesCadastro.RetornarSituaçõesTrabalho();
            model.Bancos = _repositórioOpçõesCadastro.RetornarBancos();
            model.TiposContasBancárias = _repositórioOpçõesCadastro.RetornarTiposContas();
            model.SegmentosCulturais = _repositórioSegmentosCulturais.RetornarTodos();
        }

        private void DeletarSeExistir(string caminho)
        {
            if (string.IsNullOrEmpty(caminho)) return;

            if(System.IO.File.Exists(caminho))
            {
                System.IO.File.Delete(caminho);
            }
        }
    }
}
