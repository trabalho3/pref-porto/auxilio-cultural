﻿using PontosCulturais.Exceptions.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Exceptions
{
    public class ArquivoExistenteException : ExceptionBase 
    {
        private static readonly string _mensagem = "Já existe um arquivo com esse nome";

        public ArquivoExistenteException() : base(_mensagem)
        {

        }

        public ArquivoExistenteException(Exception e) : base(_mensagem, e)
        {

        }
    }
}
