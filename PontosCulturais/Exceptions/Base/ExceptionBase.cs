﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace PontosCulturais.Exceptions.Base
{
    public class ExceptionBase : Exception
    {
        public ExceptionBase()
        {
        }

        public ExceptionBase(string message) : base(message)
        {
        }

        public ExceptionBase(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExceptionBase(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
