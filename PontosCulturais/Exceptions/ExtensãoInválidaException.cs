﻿using PontosCulturais.Exceptions.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;

namespace PontosCulturais.Exceptions
{
    public class ExtensãoInválidaException : ExceptionBase
    {
        private static readonly string _mensagem = "Extensão de arquivo inválida";
        public ExtensãoInválidaException() : base(_mensagem)
        {
        }

        public ExtensãoInválidaException(Exception innerException) : base(_mensagem, innerException)
        {
        }
    }
}
