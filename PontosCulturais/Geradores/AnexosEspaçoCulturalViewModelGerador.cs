﻿using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Geradores
{
    public class AnexosEspaçoCulturalViewModelGerador : IViewModelControllersGerador<AnexosEspaçoCulturalViewModel>
    {
        public AnexosEspaçoCulturalViewModel GerarViewModelInicial() => new AnexosEspaçoCulturalViewModel();
    }
}
