﻿using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Geradores
{
    public class AvisoViewModelGerador : IAvisoViewModelGerador
    {
        public AvisoViewModel GerarFlagSucesso() => new AvisoViewModel { Erro = false, CódigoErro = Erros.Nenhum };

        public AvisoViewModel GerarMensagemComErroBooleanoFalso(string nomeAmigável, Erros códigoErro) => GerarMensagemErro($"{nomeAmigável} deve ser marcado como verdadeiro", códigoErro);

        public AvisoViewModel GerarMensagemComErroDeCaracteres(string nomeAmigável,Erros códigoErro, int mínimo = 0, int máximo = 0)
        {
            var mensagem = $"{nomeAmigável} deve ter a seguinte quantidade de caracteres:";
            if (mínimo > 0) mensagem = $"{mensagem} {mínimo}";
            if (máximo > 0) mensagem = $"{mensagem} até {máximo}";

            return GerarMensagemErro(mensagem, códigoErro);
        }

        public AvisoViewModel GerarMensagemComErroNulo(string nomeAmigável, Erros códigoErro) => GerarMensagemErro($"{nomeAmigável} não pode estar vazio", códigoErro);
        public AvisoViewModel GerarMensagemErro(string mensagem, Erros códigoErro) => new AvisoViewModel { Classe = "alert-danger", Erro = true, Mensagem = mensagem, CódigoErro = códigoErro };
        public AvisoViewModel GerarMensagemSucesso(string mensagem) => new AvisoViewModel { Classe = "alert-success", Erro = false, Mensagem = mensagem, CódigoErro = Erros.Nenhum };
    }
}
