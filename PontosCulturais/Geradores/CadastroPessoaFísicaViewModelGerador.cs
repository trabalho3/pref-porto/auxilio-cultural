﻿using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Models;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Geradores
{
    public class CadastroPessoaFísicaViewModelGerador : IViewModelControllersGerador<CadastroPessoaFísicaViewModel>
    {
        public CadastroPessoaFísicaViewModel GerarViewModelInicial()
        {
            var viewModel = new CadastroPessoaFísicaViewModel();
            viewModel.Aviso = new AvisoViewModel();
            viewModel.Cadastro = new CadastroPessoaFísicaModelo();

            return viewModel;
        }
    }
}
