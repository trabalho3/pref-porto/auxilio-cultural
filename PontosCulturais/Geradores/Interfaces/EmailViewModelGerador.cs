﻿using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Geradores.Interfaces
{
    public class EmailViewModelGerador : IViewModelControllersGerador<EmailViewModel>
    {
        public EmailViewModel GerarViewModelInicial() => new EmailViewModel();
    }
}
