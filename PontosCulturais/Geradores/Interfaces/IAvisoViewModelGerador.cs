﻿using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Geradores.Interfaces
{
    public interface IAvisoViewModelGerador
    {
        AvisoViewModel GerarFlagSucesso();
        AvisoViewModel GerarMensagemSucesso(string mensagem);
        AvisoViewModel GerarMensagemErro(string mensasgem, Erros códigoErro);

        AvisoViewModel GerarMensagemComErroDeCaracteres(string nomeAmigável, Erros códigoErro, int mínimo = 0, int máximo = 0);
        AvisoViewModel GerarMensagemComErroBooleanoFalso(string nomeAmigável, Erros códigoErro);
        AvisoViewModel GerarMensagemComErroNulo(string nomeAmigável, Erros códigoErro);
    }
}
