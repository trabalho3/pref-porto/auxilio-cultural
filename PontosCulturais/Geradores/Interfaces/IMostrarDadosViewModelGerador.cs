﻿using PontosCulturais.Models;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Geradores.Interfaces
{
    public interface IMostrarDadosViewModelGerador
    {
        MostrarDadosViewModel GerarViewModelInicial(CadastroPessoaFísicaModelo cadastro, IEnumerable<SegmentoCulturalModelo> segmentos);
    }
}
