﻿using PontosCulturais.Models;
using PontosCulturais.Models.CadastroEspaçoCultural;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Geradores.Interfaces
{
    public interface IMostrarEspaçoCulturalViewModelGerador
    {
        MostrarEspaçoCulturalViewModel GerarViewModelInicial(IOpçõesCadastroParaTextoServiço opçõesParaCadastro, EspaçoCulturalSimplesModelo espaço, IEnumerable<TipoDeAtividadeModelo> atividades, 
            IEnumerable<OrigemRecursosModelo> origens, IEnumerable<ModoFuncionamentoModelo> funcionamentos, IEnumerable<SegmentoCulturalModelo> segmentos, IEnumerable<EquipamentoCulturalModelo> equipamentos);
    }
}
