﻿using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Geradores.Interfaces
{
    public interface IResidentesViewModelGerador
    {
        ResidentesViewModel GerarViewModelInicial(IEnumerable<ResidenteModelo> modelos);
        public ResidentesViewModel GerarComAviso(IEnumerable<ResidenteModelo> modelos, AvisoViewModel aviso);
    }
}
