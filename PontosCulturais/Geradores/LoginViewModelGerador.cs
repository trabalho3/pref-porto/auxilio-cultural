﻿using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Geradores
{
    public class LoginViewModelGerador : IViewModelControllersGerador<LoginViewModel>
    {
        public LoginViewModel GerarViewModelInicial() => new LoginViewModel();
    }
}
