﻿using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Models;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Geradores
{
    public class MostrarDadosViewModelGerador : IMostrarDadosViewModelGerador
    {
        private readonly IOpçõesCadastroParaTextoServiço _opçõesCadastroParaTexto;

        public MostrarDadosViewModelGerador(IOpçõesCadastroParaTextoServiço opçõesCadastroParaTexto)
        {
            _opçõesCadastroParaTexto = opçõesCadastroParaTexto;
        }

        public MostrarDadosViewModel GerarViewModelInicial(CadastroPessoaFísicaModelo cadastro, IEnumerable<SegmentoCulturalModelo> segmentos) =>
            new MostrarDadosViewModel(_opçõesCadastroParaTexto) { PessoaFísica = cadastro, SegmentosCulturais = segmentos };
    }
}
