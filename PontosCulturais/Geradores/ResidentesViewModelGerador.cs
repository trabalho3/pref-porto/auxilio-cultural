﻿using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Geradores
{
    public class ResidentesViewModelGerador : IResidentesViewModelGerador
    {
        public ResidentesViewModel GerarViewModelInicial(IEnumerable<ResidenteModelo> modelos) => new ResidentesViewModel { Residentes = modelos, Residente = new ResidenteModelo() };
        public ResidentesViewModel GerarComAviso(IEnumerable<ResidenteModelo> modelos, AvisoViewModel aviso) => new ResidentesViewModel { Residentes = modelos, Residente = new ResidenteModelo(), Aviso = aviso };
    }
}
