﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PontosCulturais.Migrations
{
    public partial class Inicio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bancos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Código = table.Column<string>(maxLength: 10, nullable: false),
                    Nome = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bancos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ComunidadesTradicionais",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComunidadesTradicionais", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Deficiências",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Deficiências", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EquipamentosCulturais",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquipamentosCulturais", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FunçõesCulturais",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FunçõesCulturais", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GastosAnuais",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GastosAnuais", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Gêneros",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gêneros", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ModosFuncionamento",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModosFuncionamento", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrigemRecursos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrigemRecursos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Raças",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Raças", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ReceitasAnuais",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReceitasAnuais", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SegmentosCulturais",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SegmentosCulturais", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SituaçãoBeneficiário",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SituaçãoBeneficiário", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SituaçõesAtividade",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SituaçõesAtividade", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SituaçõesTrabalho",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SituaçõesTrabalho", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TiposAtividadesEspaçoCultural",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiposAtividadesEspaçoCultural", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TiposContaBancária",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiposContaBancária", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PessoasFísicas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CPF = table.Column<string>(maxLength: 11, nullable: false),
                    NomeCompleto = table.Column<string>(maxLength: 100, nullable: false),
                    NomeSocial = table.Column<string>(maxLength: 100, nullable: true),
                    NomeMãe = table.Column<string>(maxLength: 100, nullable: false),
                    Nascimento = table.Column<DateTime>(nullable: false),
                    Telefone01 = table.Column<string>(maxLength: 11, nullable: false),
                    Telefone02 = table.Column<string>(maxLength: 11, nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: false),
                    PaísNascimento = table.Column<string>(maxLength: 15, nullable: true),
                    Endereço = table.Column<string>(maxLength: 100, nullable: false),
                    ComplementoEndereço = table.Column<string>(maxLength: 100, nullable: true),
                    Bairro = table.Column<string>(maxLength: 50, nullable: false),
                    GêneroDeclaradoId = table.Column<int>(nullable: false),
                    DeficiênciaId = table.Column<int>(nullable: false),
                    RaçaId = table.Column<int>(nullable: false),
                    ComunidadeTradicionalId = table.Column<int>(nullable: false),
                    MulherProvedora = table.Column<bool>(nullable: false),
                    FunçãoCulturalId = table.Column<int>(nullable: false),
                    Atuação = table.Column<string>(maxLength: 1000, nullable: false),
                    SituaçãoTrabalhoId = table.Column<int>(nullable: false),
                    TipoContaBancáriaId = table.Column<int>(nullable: false),
                    BancoId = table.Column<int>(nullable: false),
                    ContaBancária = table.Column<string>(maxLength: 20, nullable: false),
                    CódigoOperação = table.Column<string>(maxLength: 10, nullable: true),
                    Senha = table.Column<string>(maxLength: 50, nullable: false),
                    RG = table.Column<string>(maxLength: 15, nullable: false),
                    ÓrgãoEstado = table.Column<string>(maxLength: 10, nullable: false),
                    CadastrarEspaçoCultural = table.Column<bool>(nullable: false),
                    DeclaraçãoAtuaçãoCulturaAtualmente = table.Column<bool>(nullable: false),
                    DeclaraçãoAtuaçãoCulturaDoisAnos = table.Column<bool>(nullable: false),
                    DeclaraçãoNãoRecebeBenefício = table.Column<bool>(nullable: false),
                    DeclaraçãoÑãoRecebeSeguroDesemprego = table.Column<bool>(nullable: false),
                    DeclaraçãoBaixaRenda = table.Column<bool>(nullable: false),
                    DeclaraçãoRendimentoMédio = table.Column<bool>(nullable: false),
                    DeclaraçãoBaixoRendimento2018 = table.Column<bool>(nullable: false),
                    DeclaraçãoNãoRecebeAuxílioEmergencial = table.Column<bool>(nullable: false),
                    DeclaraçãoVeracidadeDados = table.Column<bool>(nullable: false),
                    AutorizaçãoCruzamentoDeDados = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PessoasFísicas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PessoasFísicas_Bancos_BancoId",
                        column: x => x.BancoId,
                        principalTable: "Bancos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PessoasFísicas_ComunidadesTradicionais_ComunidadeTradicionalId",
                        column: x => x.ComunidadeTradicionalId,
                        principalTable: "ComunidadesTradicionais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PessoasFísicas_Deficiências_DeficiênciaId",
                        column: x => x.DeficiênciaId,
                        principalTable: "Deficiências",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PessoasFísicas_FunçõesCulturais_FunçãoCulturalId",
                        column: x => x.FunçãoCulturalId,
                        principalTable: "FunçõesCulturais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PessoasFísicas_Gêneros_GêneroDeclaradoId",
                        column: x => x.GêneroDeclaradoId,
                        principalTable: "Gêneros",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PessoasFísicas_Raças_RaçaId",
                        column: x => x.RaçaId,
                        principalTable: "Raças",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PessoasFísicas_SituaçõesTrabalho_SituaçãoTrabalhoId",
                        column: x => x.SituaçãoTrabalhoId,
                        principalTable: "SituaçõesTrabalho",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PessoasFísicas_TiposContaBancária_TipoContaBancáriaId",
                        column: x => x.TipoContaBancáriaId,
                        principalTable: "TiposContaBancária",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AnexosAtuação",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Url = table.Column<string>(maxLength: 100, nullable: false),
                    PessoaFísicaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnexosAtuação", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AnexosAtuação_PessoasFísicas_PessoaFísicaId",
                        column: x => x.PessoaFísicaId,
                        principalTable: "PessoasFísicas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DocumentosPessoaFísica",
                columns: table => new
                {
                    PessoaFísicaId = table.Column<int>(nullable: false),
                    PessoaFísicaId1 = table.Column<int>(nullable: true),
                    FrenteRGCNH = table.Column<string>(maxLength: 200, nullable: false),
                    VersoRGCNG = table.Column<string>(maxLength: 200, nullable: false),
                    Selfie = table.Column<string>(maxLength: 200, nullable: false),
                    ComprovanteCPF = table.Column<string>(maxLength: 200, nullable: false),
                    ComprovanteEndereço = table.Column<string>(maxLength: 200, nullable: false),
                    ComprovanteBancário = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentosPessoaFísica", x => x.PessoaFísicaId);
                    table.ForeignKey(
                        name: "FK_DocumentosPessoaFísica_PessoasFísicas_PessoaFísicaId1",
                        column: x => x.PessoaFísicaId1,
                        principalTable: "PessoasFísicas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EspaçosCulturais",
                columns: table => new
                {
                    PessoaFísicaId = table.Column<int>(nullable: false),
                    PessoaFísicaId1 = table.Column<int>(nullable: true),
                    Nome = table.Column<string>(maxLength: 50, nullable: false),
                    Telefone = table.Column<string>(maxLength: 11, nullable: true),
                    Cnpj = table.Column<string>(maxLength: 14, nullable: true),
                    RazãoSocial = table.Column<string>(maxLength: 50, nullable: true),
                    Endereço = table.Column<string>(maxLength: 100, nullable: false),
                    Bairro = table.Column<string>(maxLength: 50, nullable: false),
                    NomePopular = table.Column<string>(maxLength: 50, nullable: true),
                    DataFuncionamento = table.Column<DateTime>(nullable: false),
                    AtendimentoPago = table.Column<bool>(nullable: false),
                    TipoAtendimento = table.Column<string>(maxLength: 1000, nullable: false),
                    Funcionários = table.Column<int>(nullable: false),
                    DocumentoVínculoEmpregatícioFuncionários = table.Column<bool>(nullable: false),
                    LotaçãoMáxima = table.Column<int>(nullable: false),
                    InformaçõesAdicionaisAtuação = table.Column<string>(maxLength: 200, nullable: true),
                    ReceitaAnualId = table.Column<int>(nullable: false),
                    GastoAnualId = table.Column<int>(nullable: false),
                    SituaçãoAtividadeId = table.Column<int>(nullable: false),
                    SituaçãoEspaçoId = table.Column<int>(nullable: false),
                    PossuiAlvaráDeFuncionamento = table.Column<bool>(nullable: false),
                    InterrompidoPelaPandemia = table.Column<bool>(nullable: false),
                    DeclaraçãoInformaçõesVerídicas = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EspaçosCulturais", x => x.PessoaFísicaId);
                    table.ForeignKey(
                        name: "FK_EspaçosCulturais_GastosAnuais_GastoAnualId",
                        column: x => x.GastoAnualId,
                        principalTable: "GastosAnuais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EspaçosCulturais_PessoasFísicas_PessoaFísicaId1",
                        column: x => x.PessoaFísicaId1,
                        principalTable: "PessoasFísicas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EspaçosCulturais_ReceitasAnuais_ReceitaAnualId",
                        column: x => x.ReceitaAnualId,
                        principalTable: "ReceitasAnuais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EspaçosCulturais_SituaçõesAtividade_SituaçãoAtividadeId",
                        column: x => x.SituaçãoAtividadeId,
                        principalTable: "SituaçõesAtividade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EspaçosCulturais_SituaçãoBeneficiário_SituaçãoEspaçoId",
                        column: x => x.SituaçãoEspaçoId,
                        principalTable: "SituaçãoBeneficiário",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Residentes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CPFOuCertidão = table.Column<string>(maxLength: 50, nullable: true),
                    Parentesco = table.Column<string>(maxLength: 30, nullable: false),
                    Nome = table.Column<string>(maxLength: 100, nullable: false),
                    CadastroPessoaFísicaModeloId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Residentes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Residentes_PessoasFísicas_CadastroPessoaFísicaModeloId",
                        column: x => x.CadastroPessoaFísicaModeloId,
                        principalTable: "PessoasFísicas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SegmentoCulturalPessoaFísica",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SegmentoCulturalId = table.Column<int>(nullable: false),
                    PessoaFísicaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SegmentoCulturalPessoaFísica", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SegmentoCulturalPessoaFísica_PessoasFísicas_PessoaFísicaId",
                        column: x => x.PessoaFísicaId,
                        principalTable: "PessoasFísicas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SegmentoCulturalPessoaFísica_SegmentosCulturais_SegmentoCulturalId",
                        column: x => x.SegmentoCulturalId,
                        principalTable: "SegmentosCulturais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EspaçoCulturalEquipamentoCultural",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EspaçoCulturalId = table.Column<int>(nullable: false),
                    EquipamentoCulturalId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EspaçoCulturalEquipamentoCultural", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EspaçoCulturalEquipamentoCultural_EquipamentosCulturais_EquipamentoCulturalId",
                        column: x => x.EquipamentoCulturalId,
                        principalTable: "EquipamentosCulturais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EspaçoCulturalEquipamentoCultural_EspaçosCulturais_EspaçoCulturalId",
                        column: x => x.EspaçoCulturalId,
                        principalTable: "EspaçosCulturais",
                        principalColumn: "PessoaFísicaId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EspaçoCulturalModoFuncionamento",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EspaçoCulturalId = table.Column<int>(nullable: false),
                    ModoFuncionamentoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EspaçoCulturalModoFuncionamento", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EspaçoCulturalModoFuncionamento_EspaçosCulturais_EspaçoCulturalId",
                        column: x => x.EspaçoCulturalId,
                        principalTable: "EspaçosCulturais",
                        principalColumn: "PessoaFísicaId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EspaçoCulturalModoFuncionamento_ModosFuncionamento_ModoFuncionamentoId",
                        column: x => x.ModoFuncionamentoId,
                        principalTable: "ModosFuncionamento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EspaçoCulturalSegmentoCultural",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EspaçoCulturalId = table.Column<int>(nullable: false),
                    SegmentoCultural01 = table.Column<int>(nullable: false),
                    SegmentoCulturalId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EspaçoCulturalSegmentoCultural", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EspaçoCulturalSegmentoCultural_EspaçosCulturais_EspaçoCulturalId",
                        column: x => x.EspaçoCulturalId,
                        principalTable: "EspaçosCulturais",
                        principalColumn: "PessoaFísicaId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EspaçoCulturalSegmentoCultural_SegmentosCulturais_SegmentoCulturalId",
                        column: x => x.SegmentoCulturalId,
                        principalTable: "SegmentosCulturais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EspaçoCulturalTipoAtividade",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EspaçoCulturalId = table.Column<int>(nullable: false),
                    TipoDeAtividadeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EspaçoCulturalTipoAtividade", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EspaçoCulturalTipoAtividade_EspaçosCulturais_EspaçoCulturalId",
                        column: x => x.EspaçoCulturalId,
                        principalTable: "EspaçosCulturais",
                        principalColumn: "PessoaFísicaId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EspaçoCulturalTipoAtividade_TiposAtividadesEspaçoCultural_TipoDeAtividadeId",
                        column: x => x.TipoDeAtividadeId,
                        principalTable: "TiposAtividadesEspaçoCultural",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrigemRecursosEspaçoCultural",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EspaçoCulturalId = table.Column<int>(nullable: false),
                    OrigemRecursosId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrigemRecursosEspaçoCultural", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrigemRecursosEspaçoCultural_EspaçosCulturais_EspaçoCulturalId",
                        column: x => x.EspaçoCulturalId,
                        principalTable: "EspaçosCulturais",
                        principalColumn: "PessoaFísicaId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrigemRecursosEspaçoCultural_OrigemRecursos_OrigemRecursosId",
                        column: x => x.OrigemRecursosId,
                        principalTable: "OrigemRecursos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Bancos",
                columns: new[] { "Id", "Código", "Nome" },
                values: new object[,]
                {
                    { 1, "001", "Banco do Brasil S.A." },
                    { 14, "336", "Banco C6 S.A – C6 Bank" },
                    { 13, "077", "Banco Inter" },
                    { 12, "237", "Banco Bradesco S.A / Next" },
                    { 11, "323", "Mercado Pago" },
                    { 10, "290", "Pagseguro Internet S.A" },
                    { 9, "260", "Nu Pagamentos S.A (Nubank)" },
                    { 2, "237", "Banco Bradesco S.A." },
                    { 7, "422", "Banco Safra S.A." },
                    { 6, "652", "Itaú Unibanco Holding S.A." },
                    { 5, "341", "Banco Itaú S.A." },
                    { 4, "745", "Banco Citibank S.A." },
                    { 3, "104", "Caixa Econômica Federal" },
                    { 8, "033", "Banco Santander S.A." }
                });

            migrationBuilder.InsertData(
                table: "ComunidadesTradicionais",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 9, "Nenhuma" },
                    { 8, "Outra" },
                    { 7, "Povos de Terreiro" },
                    { 6, "Pescadores Artesanais" },
                    { 4, "Comunidades Ribeirinhas" },
                    { 3, "Ciganos(as)" },
                    { 1, "Indígenas" },
                    { 5, "Comunidades Rurais" },
                    { 2, "Quilombolas" }
                });

            migrationBuilder.InsertData(
                table: "Deficiências",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 6, "Nenhuma" },
                    { 1, "Física" },
                    { 2, "Auditiva" },
                    { 3, "Visual" },
                    { 4, "Intelectual" },
                    { 5, "Múltipla" }
                });

            migrationBuilder.InsertData(
                table: "EquipamentosCulturais",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 1, "Antiquários" },
                    { 23, "Parque de Vaquejada" },
                    { 15, "Estúdio" },
                    { 16, "Feira ou Mercado Público" },
                    { 17, "Galeria de Arte" },
                    { 18, "Livraria ou Sebo" },
                    { 19, "Locadora de Vídeo" },
                    { 20, "Loja de Discos" },
                    { 21, "Museu ou Centro de Memória" },
                    { 22, "Parque de Diversões" },
                    { 30, "Outro" },
                    { 24, "Pátio de Eventos" },
                    { 25, "Produtora" },
                    { 26, "Quadra de Escolas de Samba" },
                    { 27, "Teatro" },
                    { 28, "Terreiro" },
                    { 14, "Escola de Artes" },
                    { 13, "Editora" },
                    { 8, "Centro de Educação Musical" },
                    { 2, "Arquivo" },
                    { 3, "Ateliê" },
                    { 12, "Danceteria, Gafieira ou Casas de Dança" },
                    { 11, "Circo" },
                    { 10, "Cinema ou Cineclube" },
                    { 9, "Centro de Tradições" },
                    { 29, "Sede de grupo, entidade ou empresa" },
                    { 4, "Biblioteca" },
                    { 7, "Centro Cultural" },
                    { 6, "Casa de Eventos" },
                    { 5, "Casa de Cultura" }
                });

            migrationBuilder.InsertData(
                table: "FunçõesCulturais",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 1, "Artista, Artesão(ã), Bricante ou Cantor(a)" },
                    { 5, "Outro" },
                    { 4, "Técnico(a)" },
                    { 3, "Produtor(a) ou Gestor(a)" },
                    { 2, "Consultor(a) ou Curador(a)" }
                });

            migrationBuilder.InsertData(
                table: "GastosAnuais",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 1, "R$0,00" },
                    { 8, "De R$310.0000,01 a R$360.000,00" },
                    { 7, "De R$260.0000,01 a R$310.000,00" },
                    { 6, "De R$210.0000,01 a R$260.000,00" },
                    { 5, "De R$160.0000,01 a R$210.000,00" },
                    { 4, "De R$110.0000,01 a R$160.000,00" },
                    { 3, "De R$60.0000,01 a R$110.000,00" },
                    { 2, "De R$0,01 a R$60.000,00" },
                    { 9, "Acima de 360.000,01" }
                });

            migrationBuilder.InsertData(
                table: "Gêneros",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 2, "Mulher Cis" },
                    { 3, "Mulher Trans/Travesti" },
                    { 4, "Homem Trans" },
                    { 1, "Homem Cis" },
                    { 6, "Não Declarar" },
                    { 5, "Não-Binárie/Outro" }
                });

            migrationBuilder.InsertData(
                table: "ModosFuncionamento",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 8, "Outro" },
                    { 1, "Aberto Gratuitamente ao Público Geral" },
                    { 2, "Aberto Gratuitamente a um Público Específico" },
                    { 3, "Mediante Mensalidade" },
                    { 4, "Mediante Diária" },
                    { 5, "Espaço Privado de Uso Particular" },
                    { 7, "VendaDeProdutos" },
                    { 6, "Prestação de Serviços Mediante Pagamento" }
                });

            migrationBuilder.InsertData(
                table: "OrigemRecursos",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 6, "Outras Ações de Órgãos Públicos Nacionais" },
                    { 7, "Arrecadação de Recursos Através da Internet" },
                    { 10, "Mensalidades" },
                    { 9, "Patrocínio de Empresas Privadas" },
                    { 5, "Ancine" },
                    { 11, "Bilheteria" },
                    { 12, "Doações e/ou Contribuições Espontâneas" },
                    { 8, "Venda Rifas, Almoço, Festas, etc." },
                    { 4, "Leis Rouanet" },
                    { 13, "Venda de Produtos/Serviços no Setor Cultural" },
                    { 2, "Leis Municipais de Incentivo" },
                    { 1, "Programa Municipal de Incentivo à Cultura" },
                    { 14, "Outro" },
                    { 3, "Leis Estaduais de Incentivo" }
                });

            migrationBuilder.InsertData(
                table: "Raças",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 1, "Parda" },
                    { 2, "Branca" },
                    { 3, "Amarela" },
                    { 4, "Indígena" },
                    { 5, "Não Declarar" },
                    { 6, "Preta" }
                });

            migrationBuilder.InsertData(
                table: "ReceitasAnuais",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 3, "De R$60.0000,01 a R$110.000,00" },
                    { 4, "De R$110.0000,01 a R$160.000,00" },
                    { 2, "De R$0,01 a R$60.000,00" },
                    { 5, "De R$160.0000,01 a R$210.000,00" },
                    { 6, "De R$210.0000,01 a R$260.000,00" },
                    { 7, "De R$260.0000,01 a R$310.000,00" },
                    { 8, "De R$310.0000,01 a R$360.000,00" },
                    { 9, "Acima de 360.000,01" },
                    { 1, "R$0,00" }
                });

            migrationBuilder.InsertData(
                table: "SegmentosCulturais",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 5, "Artes Visuais" },
                    { 4, "Artes de Teatro" },
                    { 3, "Artes de Dança" },
                    { 2, "Artes Circenses" },
                    { 1, "Patrimônio Cultural" },
                    { 8, "Cultura Popular" },
                    { 11, "Gastronomia" },
                    { 10, "Fotografia" },
                    { 12, "Literatura" },
                    { 13, "Moda" },
                    { 14, "Música" },
                    { 7, "Áudiovisual" },
                    { 9, "Design" },
                    { 6, "Artesanato" },
                    { 16, "Outro" },
                    { 15, "Ópera" }
                });

            migrationBuilder.InsertData(
                table: "SituaçãoBeneficiário",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 8, "Espaço Virtual" },
                    { 1, "Espaço Alugado" },
                    { 2, "Espaço Emprestado ou de Uso Compartilhado" },
                    { 3, "Espaço Itinerante" },
                    { 4, "Espaço Próprio" },
                    { 6, "Espaço Público" },
                    { 7, "Espaço Público Cedido em Comodato" },
                    { 5, "Espaço Próprio Financiar" }
                });

            migrationBuilder.InsertData(
                table: "SituaçõesAtividade",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 1, "Adiada" },
                    { 2, "Cancelada" },
                    { 3, "Paralisada" },
                    { 4, "Indefinida" },
                    { 5, "Reduzida" }
                });

            migrationBuilder.InsertData(
                table: "SituaçõesTrabalho",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 5, "Nunca trabalhei" },
                    { 3, "Sou autônomo" },
                    { 2, "Trabalho sem carteira assinada" },
                    { 1, "Trabalho com carteira assinada" },
                    { 4, "Já trabalhei, mas atualmente estou desempregado" }
                });

            migrationBuilder.InsertData(
                table: "TiposAtividadesEspaçoCultural",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 5, "Palestras" },
                    { 3, "Exibição de Filmes" },
                    { 2, "Apresentações Artísticas" },
                    { 7, "Outro" },
                    { 1, "Aulas e Cursos" },
                    { 4, "Ensaios" },
                    { 6, "Exposição de Arte" }
                });

            migrationBuilder.InsertData(
                table: "TiposContaBancária",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 2, "Conta Poupança" },
                    { 1, "Conta Corrente" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AnexosAtuação_PessoaFísicaId",
                table: "AnexosAtuação",
                column: "PessoaFísicaId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentosPessoaFísica_PessoaFísicaId1",
                table: "DocumentosPessoaFísica",
                column: "PessoaFísicaId1");

            migrationBuilder.CreateIndex(
                name: "IX_EspaçoCulturalEquipamentoCultural_EquipamentoCulturalId",
                table: "EspaçoCulturalEquipamentoCultural",
                column: "EquipamentoCulturalId");

            migrationBuilder.CreateIndex(
                name: "IX_EspaçoCulturalEquipamentoCultural_EspaçoCulturalId",
                table: "EspaçoCulturalEquipamentoCultural",
                column: "EspaçoCulturalId");

            migrationBuilder.CreateIndex(
                name: "IX_EspaçoCulturalModoFuncionamento_EspaçoCulturalId",
                table: "EspaçoCulturalModoFuncionamento",
                column: "EspaçoCulturalId");

            migrationBuilder.CreateIndex(
                name: "IX_EspaçoCulturalModoFuncionamento_ModoFuncionamentoId",
                table: "EspaçoCulturalModoFuncionamento",
                column: "ModoFuncionamentoId");

            migrationBuilder.CreateIndex(
                name: "IX_EspaçoCulturalSegmentoCultural_EspaçoCulturalId",
                table: "EspaçoCulturalSegmentoCultural",
                column: "EspaçoCulturalId");

            migrationBuilder.CreateIndex(
                name: "IX_EspaçoCulturalSegmentoCultural_SegmentoCulturalId",
                table: "EspaçoCulturalSegmentoCultural",
                column: "SegmentoCulturalId");

            migrationBuilder.CreateIndex(
                name: "IX_EspaçoCulturalTipoAtividade_EspaçoCulturalId",
                table: "EspaçoCulturalTipoAtividade",
                column: "EspaçoCulturalId");

            migrationBuilder.CreateIndex(
                name: "IX_EspaçoCulturalTipoAtividade_TipoDeAtividadeId",
                table: "EspaçoCulturalTipoAtividade",
                column: "TipoDeAtividadeId");

            migrationBuilder.CreateIndex(
                name: "IX_EspaçosCulturais_GastoAnualId",
                table: "EspaçosCulturais",
                column: "GastoAnualId");

            migrationBuilder.CreateIndex(
                name: "IX_EspaçosCulturais_PessoaFísicaId1",
                table: "EspaçosCulturais",
                column: "PessoaFísicaId1");

            migrationBuilder.CreateIndex(
                name: "IX_EspaçosCulturais_ReceitaAnualId",
                table: "EspaçosCulturais",
                column: "ReceitaAnualId");

            migrationBuilder.CreateIndex(
                name: "IX_EspaçosCulturais_SituaçãoAtividadeId",
                table: "EspaçosCulturais",
                column: "SituaçãoAtividadeId");

            migrationBuilder.CreateIndex(
                name: "IX_EspaçosCulturais_SituaçãoEspaçoId",
                table: "EspaçosCulturais",
                column: "SituaçãoEspaçoId");

            migrationBuilder.CreateIndex(
                name: "IX_OrigemRecursosEspaçoCultural_EspaçoCulturalId",
                table: "OrigemRecursosEspaçoCultural",
                column: "EspaçoCulturalId");

            migrationBuilder.CreateIndex(
                name: "IX_OrigemRecursosEspaçoCultural_OrigemRecursosId",
                table: "OrigemRecursosEspaçoCultural",
                column: "OrigemRecursosId");

            migrationBuilder.CreateIndex(
                name: "IX_PessoasFísicas_BancoId",
                table: "PessoasFísicas",
                column: "BancoId");

            migrationBuilder.CreateIndex(
                name: "IX_PessoasFísicas_ComunidadeTradicionalId",
                table: "PessoasFísicas",
                column: "ComunidadeTradicionalId");

            migrationBuilder.CreateIndex(
                name: "IX_PessoasFísicas_DeficiênciaId",
                table: "PessoasFísicas",
                column: "DeficiênciaId");

            migrationBuilder.CreateIndex(
                name: "IX_PessoasFísicas_FunçãoCulturalId",
                table: "PessoasFísicas",
                column: "FunçãoCulturalId");

            migrationBuilder.CreateIndex(
                name: "IX_PessoasFísicas_GêneroDeclaradoId",
                table: "PessoasFísicas",
                column: "GêneroDeclaradoId");

            migrationBuilder.CreateIndex(
                name: "IX_PessoasFísicas_RaçaId",
                table: "PessoasFísicas",
                column: "RaçaId");

            migrationBuilder.CreateIndex(
                name: "IX_PessoasFísicas_SituaçãoTrabalhoId",
                table: "PessoasFísicas",
                column: "SituaçãoTrabalhoId");

            migrationBuilder.CreateIndex(
                name: "IX_PessoasFísicas_TipoContaBancáriaId",
                table: "PessoasFísicas",
                column: "TipoContaBancáriaId");

            migrationBuilder.CreateIndex(
                name: "IX_Residentes_CadastroPessoaFísicaModeloId",
                table: "Residentes",
                column: "CadastroPessoaFísicaModeloId");

            migrationBuilder.CreateIndex(
                name: "IX_SegmentoCulturalPessoaFísica_PessoaFísicaId",
                table: "SegmentoCulturalPessoaFísica",
                column: "PessoaFísicaId");

            migrationBuilder.CreateIndex(
                name: "IX_SegmentoCulturalPessoaFísica_SegmentoCulturalId",
                table: "SegmentoCulturalPessoaFísica",
                column: "SegmentoCulturalId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AnexosAtuação");

            migrationBuilder.DropTable(
                name: "DocumentosPessoaFísica");

            migrationBuilder.DropTable(
                name: "EspaçoCulturalEquipamentoCultural");

            migrationBuilder.DropTable(
                name: "EspaçoCulturalModoFuncionamento");

            migrationBuilder.DropTable(
                name: "EspaçoCulturalSegmentoCultural");

            migrationBuilder.DropTable(
                name: "EspaçoCulturalTipoAtividade");

            migrationBuilder.DropTable(
                name: "OrigemRecursosEspaçoCultural");

            migrationBuilder.DropTable(
                name: "Residentes");

            migrationBuilder.DropTable(
                name: "SegmentoCulturalPessoaFísica");

            migrationBuilder.DropTable(
                name: "EquipamentosCulturais");

            migrationBuilder.DropTable(
                name: "ModosFuncionamento");

            migrationBuilder.DropTable(
                name: "TiposAtividadesEspaçoCultural");

            migrationBuilder.DropTable(
                name: "EspaçosCulturais");

            migrationBuilder.DropTable(
                name: "OrigemRecursos");

            migrationBuilder.DropTable(
                name: "SegmentosCulturais");

            migrationBuilder.DropTable(
                name: "GastosAnuais");

            migrationBuilder.DropTable(
                name: "PessoasFísicas");

            migrationBuilder.DropTable(
                name: "ReceitasAnuais");

            migrationBuilder.DropTable(
                name: "SituaçõesAtividade");

            migrationBuilder.DropTable(
                name: "SituaçãoBeneficiário");

            migrationBuilder.DropTable(
                name: "Bancos");

            migrationBuilder.DropTable(
                name: "ComunidadesTradicionais");

            migrationBuilder.DropTable(
                name: "Deficiências");

            migrationBuilder.DropTable(
                name: "FunçõesCulturais");

            migrationBuilder.DropTable(
                name: "Gêneros");

            migrationBuilder.DropTable(
                name: "Raças");

            migrationBuilder.DropTable(
                name: "SituaçõesTrabalho");

            migrationBuilder.DropTable(
                name: "TiposContaBancária");
        }
    }
}
