﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PontosCulturais.Migrations
{
    public partial class CorrecoesTextoCampos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SituaçãoBeneficiário",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.UpdateData(
                table: "ModosFuncionamento",
                keyColumn: "Id",
                keyValue: 7,
                column: "Nome",
                value: "Venda De Produtos");

            migrationBuilder.UpdateData(
                table: "OrigemRecursos",
                keyColumn: "Id",
                keyValue: 4,
                column: "Nome",
                value: "Lei Rouanet");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "ModosFuncionamento",
                keyColumn: "Id",
                keyValue: 7,
                column: "Nome",
                value: "VendaDeProdutos");

            migrationBuilder.UpdateData(
                table: "OrigemRecursos",
                keyColumn: "Id",
                keyValue: 4,
                column: "Nome",
                value: "Leis Rouanet");

            migrationBuilder.InsertData(
                table: "SituaçãoBeneficiário",
                columns: new[] { "Id", "Nome" },
                values: new object[] { 5, "Espaço Próprio Financiar" });
        }
    }
}
