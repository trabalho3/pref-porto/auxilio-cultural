﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PontosCulturais.Migrations
{
    public partial class CorrecaoChaveEspacoCulturalSegmentoCultural : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EspaçoCulturalSegmentoCultural_SegmentosCulturais_SegmentoCulturalId",
                table: "EspaçoCulturalSegmentoCultural");

            migrationBuilder.DropColumn(
                name: "SegmentoCultural01",
                table: "EspaçoCulturalSegmentoCultural");

            migrationBuilder.AlterColumn<int>(
                name: "SegmentoCulturalId",
                table: "EspaçoCulturalSegmentoCultural",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_EspaçoCulturalSegmentoCultural_SegmentosCulturais_SegmentoCulturalId",
                table: "EspaçoCulturalSegmentoCultural",
                column: "SegmentoCulturalId",
                principalTable: "SegmentosCulturais",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EspaçoCulturalSegmentoCultural_SegmentosCulturais_SegmentoCulturalId",
                table: "EspaçoCulturalSegmentoCultural");

            migrationBuilder.AlterColumn<int>(
                name: "SegmentoCulturalId",
                table: "EspaçoCulturalSegmentoCultural",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "SegmentoCultural01",
                table: "EspaçoCulturalSegmentoCultural",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_EspaçoCulturalSegmentoCultural_SegmentosCulturais_SegmentoCulturalId",
                table: "EspaçoCulturalSegmentoCultural",
                column: "SegmentoCulturalId",
                principalTable: "SegmentosCulturais",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
