﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PontosCulturais.Migrations
{
    public partial class ChavesUnicasPessoaFisica : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_PessoasFísicas_CPF",
                table: "PessoasFísicas",
                column: "CPF",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PessoasFísicas_Email",
                table: "PessoasFísicas",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PessoasFísicas_RG",
                table: "PessoasFísicas",
                column: "RG",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PessoasFísicas_CPF",
                table: "PessoasFísicas");

            migrationBuilder.DropIndex(
                name: "IX_PessoasFísicas_Email",
                table: "PessoasFísicas");

            migrationBuilder.DropIndex(
                name: "IX_PessoasFísicas_RG",
                table: "PessoasFísicas");
        }
    }
}
