﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PontosCulturais.Migrations
{
    public partial class EspacoCulturalAnexos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EspaçoCulturalAnexos",
                columns: table => new
                {
                    PessoaFísicaId = table.Column<int>(nullable: false),
                    PessoaFísicaId1 = table.Column<int>(nullable: true),
                    ComprovanteEndereço = table.Column<string>(nullable: false),
                    ComprovanteAtuação = table.Column<string>(nullable: false),
                    ComprovanteCNPJ = table.Column<string>(nullable: true),
                    ComprovanteFuncionários = table.Column<string>(nullable: true),
                    ComprovanteAlvará = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EspaçoCulturalAnexos", x => x.PessoaFísicaId);
                    table.ForeignKey(
                        name: "FK_EspaçoCulturalAnexos_PessoasFísicas_PessoaFísicaId1",
                        column: x => x.PessoaFísicaId1,
                        principalTable: "PessoasFísicas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EspaçoCulturalAnexos_PessoaFísicaId1",
                table: "EspaçoCulturalAnexos",
                column: "PessoaFísicaId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EspaçoCulturalAnexos");
        }
    }
}
