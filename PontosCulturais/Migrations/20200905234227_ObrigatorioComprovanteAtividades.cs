﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PontosCulturais.Migrations
{
    public partial class ObrigatorioComprovanteAtividades : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AnexosAtuação");

            migrationBuilder.AlterColumn<string>(
                name: "ComprovanteAtividades",
                table: "DocumentosPessoaFísica",
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(200)",
                oldMaxLength: 200,
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ComprovanteAtividades",
                table: "DocumentosPessoaFísica",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200);

            migrationBuilder.CreateTable(
                name: "AnexosAtuação",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PessoaFísicaId = table.Column<int>(type: "int", nullable: false),
                    Url = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnexosAtuação", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AnexosAtuação_PessoasFísicas_PessoaFísicaId",
                        column: x => x.PessoaFísicaId,
                        principalTable: "PessoasFísicas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AnexosAtuação_PessoaFísicaId",
                table: "AnexosAtuação",
                column: "PessoaFísicaId");
        }
    }
}
