﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PontosCulturais.Migrations
{
    public partial class Agencia : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ContaBancária",
                table: "PessoasFísicas",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(20)",
                oldMaxLength: 20);

            migrationBuilder.AddColumn<string>(
                name: "Agência",
                table: "PessoasFísicas",
                maxLength: 10,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Agência",
                table: "PessoasFísicas");

            migrationBuilder.AlterColumn<string>(
                name: "ContaBancária",
                table: "PessoasFísicas",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 20,
                oldNullable: true);
        }
    }
}
