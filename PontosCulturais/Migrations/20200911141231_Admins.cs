﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PontosCulturais.Migrations
{
    public partial class Admins : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Administradores",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(nullable: false),
                    NomeCompleto = table.Column<string>(maxLength: 100, nullable: false),
                    Senha = table.Column<string>(maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Administradores", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Administradores",
                columns: new[] { "Id", "Email", "NomeCompleto", "Senha" },
                values: new object[] { 1, "cultura@portonacional.to.gov.br", "Secretaria de Cultura", "3A9ACC77EDE11E68434B5C61EB2F7A95" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Administradores");
        }
    }
}
