﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Models.CadastroEspaçoCultural
{
    public class AnexosEspaçoCulturalModelo
    {
        [Key,Required,DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PessoaFísicaId { get; set; }
        public CadastroPessoaFísicaModelo PessoaFísica { get; set; }

        [Required]
        public string ComprovanteEndereço { get; set; }
        [Required]
        public string ComprovanteAtuação { get; set; }

        public string ComprovanteCNPJ { get; set; }
        public string ComprovanteFuncionários { get; set; }
        public string ComprovanteAlvará { get; set; }

    }
}
