﻿using PontosCulturais.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Models.CadastroEspaçoCultural
{
    public class EquipamentoCulturalModelo : IOpçõesCadastroModeloParaRepresentarEmTexto
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Nome { get; set; }
    }
}
