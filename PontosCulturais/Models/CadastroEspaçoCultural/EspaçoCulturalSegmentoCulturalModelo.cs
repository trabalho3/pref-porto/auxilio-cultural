﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Models.CadastroEspaçoCultural
{
    public class EspaçoCulturalSegmentoCulturalModelo
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int EspaçoCulturalId { get; set; }
        public EspaçoCulturalSimplesModelo EspaçoCultural { get; set; }

        [Required]
        public int SegmentoCulturalId { get; set; }
        public SegmentoCulturalModelo SegmentoCultural { get; set; }

    }
}
