﻿using PontosCulturais.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Models.CadastroEspaçoCultural
{
    public class TipoDeAtividadeModelo : IOpçõesCadastroModeloParaRepresentarEmTexto
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(30)]
        public string Nome { get; set; }
    }
}
