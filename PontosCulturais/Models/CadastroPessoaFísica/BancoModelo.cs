﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Models.CadastroPessoaFísica
{
    public class BancoModelo
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(10)]
        [Required]
        public string Código { get; set; }

        [MaxLength(50)]
        [Required]
        public string Nome { get; set; }
    }
}
