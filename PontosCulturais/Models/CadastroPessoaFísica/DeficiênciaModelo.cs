﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Models.CadastroPessoaFísica
{
    public class DeficiênciaModelo
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(20)]
        [Required]
        public string Nome { get; set; }
    }
}
