﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Models.CadastroPessoaFísica
{
    public class DocumentosPessoaFísicaModelo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None), Required]
        public int PessoaFísicaId { get; set; }
        public CadastroPessoaFísicaModelo PessoaFísica { get; set; }

        [Required]
        [MaxLength(200)]
        public string FrenteRGCNH { get; set; }

        [Required]
        [MaxLength(200)]
        public string VersoRGCNG { get; set; }

        [Required]
        [MaxLength(200)]
        public string Selfie { get; set; }

        [Required]
        [MaxLength(200)]
        public string ComprovanteCPF { get; set; }

        [Required]
        [MaxLength(200)]
        public string ComprovanteEndereço { get; set; }

        [Required]
        [MaxLength(200)]
        public string ComprovanteBancário { get; set; }

        [Required]
        [MaxLength(200)]
        public string ComprovanteAtividades { get; set; }
    }
}
