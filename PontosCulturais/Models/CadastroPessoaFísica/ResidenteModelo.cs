﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Models.CadastroPessoaFísica
{
    public class ResidenteModelo
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(50)]
        public string CPFOuCertidão { get; set; }

        [MaxLength(30)]
        [Required]
        public string Parentesco { get; set; }

        [MaxLength(100)]
        [Required(ErrorMessage = "Campo Nome é obrigatório")]
        public string Nome { get; set; }

        [Required]
        public int CadastroPessoaFísicaModeloId { get; set; }
        public CadastroPessoaFísicaModelo CadastroPessoaFísicaModelo { get; set; }
    }
}
