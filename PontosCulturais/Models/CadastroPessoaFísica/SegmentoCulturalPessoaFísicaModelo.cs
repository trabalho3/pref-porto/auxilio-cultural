﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Models.CadastroPessoaFísica
{
    public class SegmentoCulturalPessoaFísicaModelo
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public SegmentoCulturalModelo SegmentoCultural { get; set; }

        [Required]
        public CadastroPessoaFísicaModelo PessoaFísica { get; set; }
    }
}
