﻿using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PontosCulturais.Models
{
    public class CadastroPessoaFísicaModelo : ILogávelModelo
    {
        [Key] 
        public int Id { get; set; }

        //Etapa 01 - Dados Pessoais

        [StringLength(11)]
        [Required]
        public string CPF { get; set; }

        [MaxLength(100)]
        [Required]
        public string NomeCompleto { get; set; }

        [MaxLength(100)]
        public string NomeSocial { get; set; }

        [MaxLength(100)]
        [Required]
        public string NomeMãe { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime Nascimento { get; set; }

        [StringLength(11)]
        [Required]
        public string Telefone01 { get; set; }

        [StringLength(11)]
        public string Telefone02 { get; set; }

        [EmailAddress]
        [MaxLength(50)]
        [Required]
        public string Email { get; set; }

        [MaxLength(15)]
        public string PaísNascimento { get; set; }

        [MaxLength(100)]
        [Required]
        public string Endereço { get; set; }

        [MaxLength(100)]
        public string ComplementoEndereço { get; set; }

        [MaxLength(50)]
        [Required]
        public string Bairro { get; set; }

        [Required]
        public int GêneroDeclaradoId { get; set; }
        public GêneroModelo GêneroDeclarado { get; set; }

        [Required]
        public int DeficiênciaId { get; set; }
        public DeficiênciaModelo Deficiência { get; set; }

        [Required]
        public int RaçaId { get; set; }
        public RaçaModelo Raça { get; set; }

        //Etapa 02 - Dados Culturais e Socioeconômicos
        [Required]
        public int ComunidadeTradicionalId { get; set; }
        public ComunidadeTradicionalModelo ComunidadeTradicional { get; set; }

        public bool MulherProvedora { get; set; }

        [Required]
        public int FunçãoCulturalId { get; set; }
        public FunçãoCulturalModelo FunçãoCultural { get; set; }

        [Required]
        [MaxLength(1000)]
        public string Atuação { get; set; }

        [Required]
        public int SituaçãoTrabalhoId { get; set; }
        public SituaçãoTrabalhoModelo SituaçãoTrabalho { get; set; }

        [Required]
        public int TipoContaBancáriaId { get; set; }
        public TipoContaBancáriaModelo TipoContaBancária { get; set; }
        
        [Required]
        public int BancoId { get; set; }
        public BancoModelo Banco { get; set; }

        [MaxLength(10)]
        public string Agência { get; set; }

        [MaxLength(20)]
        public string ContaBancária { get; set; }

        [MaxLength(10)]
        public string CódigoOperação { get; set; }

        [MinLength(6)]
        [MaxLength(50)]
        [Required]
        public string Senha { get; set; }

        [Required]
        [MaxLength(15)]
        public string RG { get; set; }

        [Required]
        [MaxLength(10)]
        public string ÓrgãoEstado { get; set; }

        public bool CadastrarEspaçoCultural { get; set; }
        public bool DeclaraçãoAtuaçãoCulturaAtualmente { get; set; }
        public bool DeclaraçãoAtuaçãoCulturaDoisAnos { get; set; }
        public bool DeclaraçãoNãoRecebeBenefício { get; set; }
        public bool DeclaraçãoÑãoRecebeSeguroDesemprego { get; set; }
        public bool DeclaraçãoBaixaRenda { get; set; }
        public bool DeclaraçãoRendimentoMédio { get; set; }
        public bool DeclaraçãoBaixoRendimento2018 { get; set; }
        public bool DeclaraçãoNãoRecebeAuxílioEmergencial { get; set; }
        public bool DeclaraçãoVeracidadeDados { get; set; }
        public bool AutorizaçãoCruzamentoDeDados { get; set; }

        public bool EmailConfirmado { get; set; }


        [NotMapped]
        public ICollection<int> SegmentosCulturaisId { get; set; }

        [NotMapped]
        public string ConfirmarSenha { get; set; }

        public CadastroPessoaFísicaModelo()
        {
            Nascimento = DateTime.Now.Date;
            SegmentosCulturaisId = new List<int>();
        }

    }
}
