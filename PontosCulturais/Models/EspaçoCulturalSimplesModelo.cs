﻿using Newtonsoft.Json;
using PontosCulturais.Models.CadastroEspaçoCultural;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Models
{
    public class EspaçoCulturalSimplesModelo
    {
        [Key,Required, DatabaseGenerated(DatabaseGeneratedOption.None)] 
        public int PessoaFísicaId { get; set; }
        public CadastroPessoaFísicaModelo PessoaFísica { get; set; }

        [Required]
        [MaxLength(50)]
        public string Nome { get; set; }

        [MaxLength(11)]
        public string Telefone { get; set; }

        [StringLength(14)]
        public string Cnpj { get; set; }

        [MaxLength(50)]
        public string RazãoSocial { get; set; }

        [Required]
        [MaxLength(100)]
        public string Endereço { get; set; }

        [Required]
        [MaxLength(50)]
        public string Bairro { get; set; }

        [MaxLength(50)]
        public string NomePopular { get; set; }

        [Required]
        public DateTime DataFuncionamento { get; set; }

        public bool AtendimentoPago { get; set; }

        [Required]
        [MaxLength(1000)]
        public string TipoAtendimento { get; set; }

        public int Funcionários { get; set; }

        public bool DocumentoVínculoEmpregatícioFuncionários { get; set; }

        public int LotaçãoMáxima { get; set; }

        [MaxLength(200)]
        public string InformaçõesAdicionaisAtuação { get; set; }

        [Required]
        public int ReceitaAnualId { get; set; }
        public ReceitaAnualModelo ReceitaAnual { get; set; }

        [Required]
        public int GastoAnualId { get; set; }
        public GastoAnualModelo GastoAnual { get; set; }

        [Required]
        public int SituaçãoAtividadeId { get; set; }
        public SituaçãoAtividadeModelo SituaçãoAtividade { get; set; }

        [Required]
        public int SituaçãoEspaçoId { get; set; }
        public SituaçãoBeneficiárioModelo SituaçãoEspaço { get; set; }

        public bool PossuiAlvaráDeFuncionamento { get; set; }
        public bool InterrompidoPelaPandemia { get; set; }
        public bool DeclaraçãoInformaçõesVerídicas { get; set; }
    }
}
