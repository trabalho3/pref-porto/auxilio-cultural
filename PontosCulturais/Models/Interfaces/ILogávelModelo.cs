﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Models.Interfaces
{
    public interface ILogávelModelo
    {
        public int Id { get;  }
        public string Email { get; }
        public string NomeCompleto { get; }
    }
}
