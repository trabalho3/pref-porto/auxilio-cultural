﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Models.Interfaces
{
    public interface IOpçõesCadastroModeloParaRepresentarEmTexto
    {
        int Id { get; }
        string Nome { get; }
    }
}
