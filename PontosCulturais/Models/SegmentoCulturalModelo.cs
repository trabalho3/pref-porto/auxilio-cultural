﻿using PontosCulturais.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime;
using System.Threading.Tasks;

namespace PontosCulturais.Models
{
    public class SegmentoCulturalModelo : IOpçõesCadastroModeloParaRepresentarEmTexto
    {
        [Key]
        public int Id { get; set; }
        
        [MaxLength(30)]
        [Required]
        public string Nome { get; set; }

    }
}
