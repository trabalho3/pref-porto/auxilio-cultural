﻿using PontosCulturais.Contexto;
using PontosCulturais.Models;
using PontosCulturais.Repositórios.Interface;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios
{
    public class AdminRepositório : IAdminRepositório
    {
        private readonly AppContexto _contexto;
        private readonly ISegurançaServiço _segurançaServiço;

        public AdminRepositório(AppContexto contexto, ISegurançaServiço segurançaServiço)
        {
            _contexto = contexto;
            _segurançaServiço = segurançaServiço;
        }

        public AdminModelo ChecarLogin(AdminViewModel viewModel)
        {
            var senhaCriptografada = _segurançaServiço.Criptografar(viewModel.Senha);
            return _contexto.Administradores.Where(adm => adm.Email == viewModel.Email && adm.Senha == senhaCriptografada).FirstOrDefault();
        }
    }
}
