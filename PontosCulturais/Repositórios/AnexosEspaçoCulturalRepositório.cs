﻿using PontosCulturais.Contexto;
using PontosCulturais.Models.CadastroEspaçoCultural;
using PontosCulturais.Repositórios.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios
{
    public class AnexosEspaçoCulturalRepositório : IAnexosEspaçoCulturalRepositório
    {
        private readonly AppContexto _contexto;

        public AnexosEspaçoCulturalRepositório(AppContexto contexto)
        {
            _contexto = contexto;
        }

        public void Inserir(AnexosEspaçoCulturalModelo modelo) => _contexto.EspaçoCulturalAnexos.Add(modelo);

        public bool VerificarSeJáCadastrou(int id) => _contexto.EspaçoCulturalAnexos.Where(ec => ec.PessoaFísicaId == id).Count() == 1;
    }
}
