﻿using Microsoft.EntityFrameworkCore;
using PontosCulturais.Contexto;
using PontosCulturais.Models;
using PontosCulturais.Repositórios.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios
{
    public class CadastroPessoaFísicaRepositório : ICadastroPessoaFísicaRepositório
    {
        private readonly AppContexto _contexto;

        public CadastroPessoaFísicaRepositório(AppContexto contexto)
        {
            _contexto = contexto;
        }

        public int AcharIdPorEmail(string email)
        {
            var cadastro = _contexto.PessoasFísicas.Where(pf => pf.Email == email).FirstOrDefault();

            if (cadastro == null) return -1;
            return cadastro.Id;
        }

        public void AtualizarSenha(int id, string senha)
        {
            var cadastro = RetornarPorId(id);
            cadastro.Senha = senha;
            _contexto.PessoasFísicas.Update(cadastro);
        }

        public void ConfirmarEmail(int id)
        {
            var cadastro = _contexto.PessoasFísicas.Where(pf => pf.Id == id).First();

            if (!cadastro.EmailConfirmado) cadastro.EmailConfirmado = true;
        }

        public void Inserir(CadastroPessoaFísicaModelo modelo) => _contexto.PessoasFísicas.Add(modelo);

        public CadastroPessoaFísicaModelo Logar(string email, string senhaCriptografada) => _contexto.PessoasFísicas.Where(pf => pf.Email == email && pf.Senha == senhaCriptografada).FirstOrDefault();

        public CadastroPessoaFísicaModelo RetornarPorId(int id)
        {
            var retorno = _contexto.PessoasFísicas.Where(pf => pf.Id == id).First();
            retorno.Senha = "";

            return retorno;
        }

        public CadastroPessoaFísicaModelo RetornarPorIdComJoins(int id)
        {
            var retorno = _contexto.PessoasFísicas
                .Where(pf => pf.Id == id)
                .Include(pf => pf.GêneroDeclarado)
                .Include(pf => pf.Raça)
                .Include(pf => pf.FunçãoCultural)
                .Include(pf => pf.ComunidadeTradicional)
                .Include(pf => pf.TipoContaBancária)
                .Include(pf => pf.Banco)
                .Include(pf => pf.Deficiência)
                .Include(pf => pf.SituaçãoTrabalho)
                .First();

            return retorno;
        }

        public bool VerificarCPFÚnico(string cpf) => _contexto.PessoasFísicas.Where(pf => pf.CPF == cpf).Count() == 0;

        public bool VerificarEmailÚnico(string email) => _contexto.PessoasFísicas.Where(pf => pf.Email == email).Count() == 0;

        public bool VerificarRGÚnico(string rG) => _contexto.PessoasFísicas.Where(pf => pf.RG == rG).Count() == 0;
    }
}
