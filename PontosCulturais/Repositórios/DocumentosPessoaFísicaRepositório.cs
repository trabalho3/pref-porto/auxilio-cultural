﻿using PontosCulturais.Contexto;
using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.Repositórios.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios
{
    public class DocumentosPessoaFísicaRepositório : IDocumentosPessoaFísicaRepositório
    {
        private readonly AppContexto _contexto;

        public DocumentosPessoaFísicaRepositório(AppContexto contexto)
        {
            _contexto = contexto;
        }

        public bool ChecarUsuárioJáMandou(int idUsuário) => _contexto.DocumentosPessoaFísica.Where(d => d.PessoaFísicaId == idUsuário).Count() == 1;

        public void Inserir(DocumentosPessoaFísicaModelo modelo) => _contexto.DocumentosPessoaFísica.Add(modelo);
    }
}
