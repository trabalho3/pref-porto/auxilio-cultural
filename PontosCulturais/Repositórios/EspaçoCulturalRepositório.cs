﻿using Microsoft.EntityFrameworkCore;
using PontosCulturais.Contexto;
using PontosCulturais.Models;
using PontosCulturais.Repositórios.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios
{
    public class EspaçoCulturalRepositório : IEspaçoCulturalRepositório
    {
        private readonly AppContexto _contexto;

        public EspaçoCulturalRepositório(AppContexto contexto)
        {
            _contexto = contexto;
        }

        public bool ChecarPeloIdUsuário(int id) => _contexto.EspaçosCulturais.Where(ec => ec.PessoaFísicaId == id).Count() == 1;

        public void Inserir(EspaçoCulturalSimplesModelo modelo) => _contexto.EspaçosCulturais.Add(modelo);

        public EspaçoCulturalSimplesModelo RetornarPorId(int id) => _contexto.EspaçosCulturais
            .Where(ec => ec.PessoaFísicaId == id)
            .Include(ec => ec.ReceitaAnual)
            .Include(ec => ec.GastoAnual)
            .Include(ec => ec.SituaçãoAtividade)
            .Include(ec => ec.SituaçãoEspaço)
            .First();

        public EspaçoCulturalSimplesModelo RetornarPorIdSemIncludesPodendoSerNulo(int id) => _contexto.EspaçosCulturais.Where(ec => ec.PessoaFísicaId == id).FirstOrDefault();

        public bool VerificarCNPJÚnico(string cnpj) => _contexto.EspaçosCulturais.Where(ec => ec.Cnpj == cnpj).Count() == 0;
    }
}
