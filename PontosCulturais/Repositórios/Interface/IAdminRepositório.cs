﻿using PontosCulturais.Models;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios.Interface
{
    public interface IAdminRepositório
    {
        AdminModelo ChecarLogin(AdminViewModel viewModel);
    }
}
