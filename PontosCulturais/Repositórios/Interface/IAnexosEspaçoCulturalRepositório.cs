﻿using PontosCulturais.Models.CadastroEspaçoCultural;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios.Interface
{
    public interface IAnexosEspaçoCulturalRepositório
    {
        void Inserir(AnexosEspaçoCulturalModelo modelo);
        bool VerificarSeJáCadastrou(int v);
    }
}
