﻿using PontosCulturais.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios.Interface
{
    public interface ICadastroPessoaFísicaRepositório
    {
        void Inserir(CadastroPessoaFísicaModelo modelo);
        bool VerificarCPFÚnico(string cpf);
        CadastroPessoaFísicaModelo Logar(string email, string senhaCriptografada);
        CadastroPessoaFísicaModelo RetornarPorId(int id);
        CadastroPessoaFísicaModelo RetornarPorIdComJoins(int id);
        bool VerificarRGÚnico(string rG);
        bool VerificarEmailÚnico(string email);
        int AcharIdPorEmail(string email);
        void ConfirmarEmail(int id);
        void AtualizarSenha(int id, string senha);
    }
}
