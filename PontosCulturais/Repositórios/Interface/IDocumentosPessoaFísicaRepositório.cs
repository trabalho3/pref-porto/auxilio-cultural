﻿using PontosCulturais.Models.CadastroPessoaFísica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios.Interface
{
    public interface IDocumentosPessoaFísicaRepositório
    {
        public void Inserir(DocumentosPessoaFísicaModelo modelo);
        public bool ChecarUsuárioJáMandou(int idUsuário);
    }
}
