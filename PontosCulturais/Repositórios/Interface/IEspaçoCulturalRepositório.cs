﻿using PontosCulturais.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios.Interface
{
    public interface IEspaçoCulturalRepositório
    {
        void Inserir(EspaçoCulturalSimplesModelo modelo);
        bool VerificarCNPJÚnico(string cnpj);
        bool ChecarPeloIdUsuário(int id);
        EspaçoCulturalSimplesModelo RetornarPorId(int id);
        EspaçoCulturalSimplesModelo RetornarPorIdSemIncludesPodendoSerNulo(int id);
    }
}
