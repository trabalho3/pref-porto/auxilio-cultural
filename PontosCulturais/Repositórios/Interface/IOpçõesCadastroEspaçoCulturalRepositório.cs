﻿using PontosCulturais.Models;
using PontosCulturais.Models.CadastroEspaçoCultural;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios.Interface
{
    public interface IOpçõesCadastroEspaçoCulturalRepositório
    {
        IEnumerable<TipoDeAtividadeModelo> RetornarTiposAtividades();
        IEnumerable<EquipamentoCulturalModelo> RetornarEquipamentosCulturais();
        IEnumerable<SituaçãoBeneficiárioModelo> RetornarSituaçõesImóvel();
        IEnumerable<ReceitaAnualModelo> RetornarReceitasAnuais();
        IEnumerable<GastoAnualModelo> RetornarDespesasAnuais();
        IEnumerable<OrigemRecursosModelo> RetornarOrigemRecursos();
        IEnumerable<SituaçãoAtividadeModelo> RetornarSituaçõesAtividades();
        IEnumerable<ModoFuncionamentoModelo> RetornarModosFuncionamento();

        void Inserir(CadastroEspaçoCulturalViewModel viewModel);

        IEnumerable<TipoDeAtividadeModelo> RetornarAtividadesPorIdPessoaFísica(int id);
        IEnumerable<OrigemRecursosModelo> RetornarOrigensPorIdPessoaFísica(int id);
        IEnumerable<ModoFuncionamentoModelo> RetornarFuncionamentosPorIdPessoaFísica(int id);
        IEnumerable<SegmentoCulturalModelo> RetornarSegmentosPorIdPessoaFísica(int id);
        IEnumerable<EquipamentoCulturalModelo> RetornarEquipamentosPorIdPessoaFísica(int id);
    }
}
