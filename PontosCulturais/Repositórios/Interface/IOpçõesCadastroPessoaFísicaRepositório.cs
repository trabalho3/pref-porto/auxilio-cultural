﻿using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios.Interface
{
    public interface IOpçõesCadastroPessoaFísicaRepositório
    {
        IEnumerable<GêneroModelo> RetornarGêneros();
        IEnumerable<RaçaModelo> RetornarRaças();
        IEnumerable<DeficiênciaModelo> RetornarDeficiências();
        IEnumerable<FunçãoCulturalModelo> RetornarFunçõesCulturais();
        IEnumerable<ComunidadeTradicionalModelo> RetornarComunidadesTradicionais();
        IEnumerable<SituaçãoTrabalhoModelo> RetornarSituaçõesTrabalho();
        IEnumerable<TipoContaBancáriaModelo> RetornarTiposContas();
        IEnumerable<BancoModelo> RetornarBancos();
        
    }
}
