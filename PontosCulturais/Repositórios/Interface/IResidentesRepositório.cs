﻿using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios.Interface
{
    public interface IResidentesRepositório
    {
        IEnumerable<ResidenteModelo> RetornarResidentes(int idUsuário);
        void Inserir(ResidenteModelo modelo);
        bool VerificarCPFCertidãoNãoCadastrado(string cpfOuCertidão);
    }
}
