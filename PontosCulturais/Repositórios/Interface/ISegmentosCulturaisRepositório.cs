﻿using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios.Interface
{
    public interface ISegmentosCulturaisRepositório
    {
        IEnumerable<SegmentoCulturalModelo> RetornarSegmentos(IEnumerable<int> ids);
        IEnumerable<SegmentoCulturalModelo> RetornarSegmentosPessoaFísica(int pessoaFísicaId);
        IEnumerable<SegmentoCulturalModelo> RetornarTodos();
        void InserirRelacionamentos(IEnumerable<SegmentoCulturalPessoaFísicaModelo> relacionamentos);
    }
}
