﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Scaffolding;
using PontosCulturais.Contexto;
using PontosCulturais.Models;
using PontosCulturais.Models.CadastroEspaçoCultural;
using PontosCulturais.Repositórios.Interface;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios
{
    public class OpçõesCadastroEspaçoCulturalRepositório : IOpçõesCadastroEspaçoCulturalRepositório
    {
        private readonly AppContexto _contexto;

        public OpçõesCadastroEspaçoCulturalRepositório(AppContexto contexto)
        {
            _contexto = contexto;
        }

        public void Inserir(CadastroEspaçoCulturalViewModel viewModel)
        {
            var modelo = viewModel.EspaçoCultural;

            var equipamentos = viewModel.IdsEquipamentos.Select(id => new EspaçoCulturalEquipamentoCulturalModelo { EquipamentoCulturalId = id, EspaçoCultural = modelo });
            var modos = viewModel.IdsModosFuncionamento.Select(id => new EspaçoCulturalModoFuncionamentoModelo { EspaçoCultural = modelo, ModoFuncionamentoId = id });
            var origens = viewModel.IdsOrigensRecursos.Select(id => new OrigemRecursosEspaçoCulturalModelo { EspaçoCultural = modelo, OrigemRecursosId = id });
            var atividades = viewModel.IdsTiposAtividades.Select(id => new EspaçoCulturalTiposAtividadeModelo { EspaçoCultural = modelo, TipoDeAtividadeId = id });
            var segmentos = viewModel.IdsSegmentosCulturais.Select(id => new EspaçoCulturalSegmentoCulturalModelo { EspaçoCultural = modelo, SegmentoCulturalId = id });

            _contexto.EspaçoCulturalEquipamentoCultural.AddRange(equipamentos);
            _contexto.EspaçoCulturalModoFuncionamento.AddRange(modos);
            _contexto.OrigemRecursosEspaçoCultural.AddRange(origens);
            _contexto.EspaçoCulturalTipoAtividade.AddRange(atividades);
            _contexto.EspaçoCulturalSegmentoCultural.AddRange(segmentos);
        }


        public IEnumerable<GastoAnualModelo> RetornarDespesasAnuais() => _contexto.GastosAnuais.ToArray();
        public IEnumerable<EquipamentoCulturalModelo> RetornarEquipamentosCulturais() => _contexto.EquipamentosCulturais.ToArray();
        public IEnumerable<ModoFuncionamentoModelo> RetornarModosFuncionamento() => _contexto.ModosFuncionamento.ToArray();
        public IEnumerable<OrigemRecursosModelo> RetornarOrigemRecursos() => _contexto.OrigemRecursos.ToArray();

        public IEnumerable<ReceitaAnualModelo> RetornarReceitasAnuais() => _contexto.ReceitasAnuais.ToArray();
        public IEnumerable<SituaçãoAtividadeModelo> RetornarSituaçõesAtividades() => _contexto.SituaçõesAtividade.ToArray();
        public IEnumerable<SituaçãoBeneficiárioModelo> RetornarSituaçõesImóvel() => _contexto.SituaçãoBeneficiário.ToArray();
        public IEnumerable<TipoDeAtividadeModelo> RetornarTiposAtividades() => _contexto.TiposAtividadesEspaçoCultural.ToArray();


        public IEnumerable<OrigemRecursosModelo> RetornarOrigensPorIdPessoaFísica(int id) => _contexto.OrigemRecursosEspaçoCultural
            .Where(ori => ori.EspaçoCulturalId == id)
            .Include(ori => ori.OrigemRecursos)
            .Select(ori => ori.OrigemRecursos);

        public IEnumerable<TipoDeAtividadeModelo> RetornarAtividadesPorIdPessoaFísica(int id) => _contexto.EspaçoCulturalTipoAtividade
            .Where(atv => atv.EspaçoCulturalId == id)
            .Include(atv => atv.TipoDeAtividade)
            .Select(atv => atv.TipoDeAtividade);

        public IEnumerable<ModoFuncionamentoModelo> RetornarFuncionamentosPorIdPessoaFísica(int id) => _contexto.EspaçoCulturalModoFuncionamento
            .Where(mod => mod.EspaçoCulturalId == id)
            .Include(mod => mod.ModoFuncionamento)
            .Select(mod => mod.ModoFuncionamento);

        public IEnumerable<SegmentoCulturalModelo> RetornarSegmentosPorIdPessoaFísica(int id) => _contexto.EspaçoCulturalSegmentoCultural
            .Where(seg => seg.EspaçoCulturalId == id)
            .Include(seg => seg.SegmentoCultural)
            .Select(seg => seg.SegmentoCultural);

        public IEnumerable<EquipamentoCulturalModelo> RetornarEquipamentosPorIdPessoaFísica(int id) => _contexto.EspaçoCulturalEquipamentoCultural
            .Where(equ => equ.EspaçoCulturalId == id)
            .Include(equ => equ.EquipamentoCultural)
            .Select(equ => equ.EquipamentoCultural);


    }
}
