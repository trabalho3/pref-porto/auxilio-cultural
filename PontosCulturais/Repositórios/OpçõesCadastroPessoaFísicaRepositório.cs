﻿using PontosCulturais.Contexto;
using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.Repositórios.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios
{
    public class OpçõesCadastroPessoaFísicaRepositório : IOpçõesCadastroPessoaFísicaRepositório
    {
        private readonly AppContexto _contexto;

        public OpçõesCadastroPessoaFísicaRepositório(AppContexto contexto)
        {
            _contexto = contexto;
        }

        public IEnumerable<BancoModelo> RetornarBancos() => _contexto.Bancos.ToArray();
        public IEnumerable<ComunidadeTradicionalModelo> RetornarComunidadesTradicionais() => _contexto.ComunidadesTradicionais.ToArray();
        public IEnumerable<DeficiênciaModelo> RetornarDeficiências() => _contexto.Deficiências.ToArray();
        public IEnumerable<FunçãoCulturalModelo> RetornarFunçõesCulturais() => _contexto.FunçõesCulturais.ToArray();
        public IEnumerable<GêneroModelo> RetornarGêneros() =>_contexto.Gêneros.ToArray();
        public IEnumerable<RaçaModelo> RetornarRaças() => _contexto.Raças.ToArray();
        public IEnumerable<SituaçãoTrabalhoModelo> RetornarSituaçõesTrabalho() => _contexto.SituaçõesTrabalho.ToArray();
        public IEnumerable<TipoContaBancáriaModelo> RetornarTiposContas() => _contexto.TiposContaBancária.ToArray();
    }
}
