﻿using PontosCulturais.Contexto;
using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.Repositórios.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios
{
    public class ResidentesRepositório : IResidentesRepositório
    {
        private readonly AppContexto _contexto;

        public ResidentesRepositório(AppContexto contexto)
        {
            _contexto = contexto;
        }

        public void Inserir(ResidenteModelo modelo) => _contexto.Residentes.Add(modelo);

        public IEnumerable<ResidenteModelo> RetornarResidentes(int idUsuário) => _contexto.Residentes.Where(r => r.CadastroPessoaFísicaModeloId == idUsuário).ToArray();

        public bool VerificarCPFCertidãoNãoCadastrado(string cpfOuCertidão) => _contexto.Residentes.Where(r => r.CPFOuCertidão == cpfOuCertidão).Count() == 0;
    }
}
