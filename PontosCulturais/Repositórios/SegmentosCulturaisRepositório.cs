﻿using Microsoft.EntityFrameworkCore;
using PontosCulturais.Contexto;
using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.Repositórios.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Repositórios
{
    public class SegmentosCulturaisRepositório : ISegmentosCulturaisRepositório
    {
        private readonly AppContexto _contexto;

        public SegmentosCulturaisRepositório(AppContexto contexto)
        {
            _contexto = contexto;
        }

        public void InserirRelacionamentos(IEnumerable<SegmentoCulturalPessoaFísicaModelo> relacionamentos) => _contexto.SegmentoCulturalPessoaFísica.AddRange(relacionamentos);

        public IEnumerable<SegmentoCulturalModelo> RetornarSegmentos(IEnumerable<int> ids)
        {
            var retorno = new List<SegmentoCulturalModelo>();
            foreach (var item in _contexto.SegmentosCulturais)
            {
                if (ids.Contains(item.Id)) retorno.Add(item);
            }

            return retorno;
        }

        public IEnumerable<SegmentoCulturalModelo> RetornarSegmentosPessoaFísica(int pessoaFísicaId)
        {
            var retorno = _contexto.SegmentoCulturalPessoaFísica
                .Where(scpf => scpf.PessoaFísica.Id == pessoaFísicaId)
                .Include(scpf => scpf.SegmentoCultural)
                .Select(scpf => scpf.SegmentoCultural)
                .ToArray();

            return retorno;
        }

        public IEnumerable<SegmentoCulturalModelo> RetornarTodos() => _contexto.SegmentosCulturais.ToArray();
    }
}
