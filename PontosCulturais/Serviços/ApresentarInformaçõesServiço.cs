﻿using PontosCulturais.Serviços.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços
{
    public class ApresentarInformaçõesServiço : IApresentarInformaçõesServiço
    {
        public string Apresentar(bool valor) => valor ? "Sim" : "Não";
    }
}
