﻿using MailKit.Net.Smtp;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MimeKit;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços
{
    public class EmailServiço : IEmailServiço
    {
        private readonly IVariáveisAmbienteSingleton _ambiente;

        public EmailServiço(IVariáveisAmbienteSingleton ambiente)
        {
            _ambiente = ambiente;
        }

        public void EnviarEmailConfirmação(string token, string email)
        {
            var mensagem = PreprararMensagem(email, "Confirmar E-mail", TextoConfirmarEmail(token));
            EnviarEmail(mensagem);
        }



        public void EnviarEmailRedefinirSenha(string token, string email)
        {
            var mensagem = PreprararMensagem(email, "Redefinir Senha", TextoRedefinirSenha(token));
            EnviarEmail(mensagem);
        }

        private MimeMessage IniciarEmail(string emailAlvo)
        {
            var mimeMessage = new MimeMessage();
            mimeMessage.From.Add(MailboxAddress.Parse(_ambiente.UsuárioServidorEmail));
            mimeMessage.To.Add(MailboxAddress.Parse(emailAlvo));

            return mimeMessage;

        }
        private MimeMessage PreprararMensagem(string email, string assunto, string texto)
        {
            var mimeMessage = IniciarEmail(email);
            mimeMessage.Subject = assunto;
            mimeMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = texto
            };

            return mimeMessage;
        }

        private void EnviarEmail(MimeMessage mensagem)
        {
            SmtpClient smtpClient = new SmtpClient();

            smtpClient.Connect(_ambiente.ServidorEmail, int.Parse(_ambiente.ServidorEmailPorta), true);
            smtpClient.Authenticate(_ambiente.UsuárioServidorEmail, _ambiente.SenhaServidorEmail);
            smtpClient.Send(mensagem);
            smtpClient.Disconnect(true);
        }

        private string TextoConfirmarEmail(string token) =>
            $"Olá! Seja bem-vindo ao sistema de cadastro para recebimento do auxílio conforme regulamentado pela Lei Aldir Blanc. Para confirmar seu e-mail, acesse o seguinte link: " +
            $"<a href='https://auxilio.portonacional.to.gov.br/Email/ConfirmarEmail/{token}'>https://auxilio.portonacional.to.gov.br/Email/ConfirmarEmail/{token}</a>. Se preferir, " +
            $"você pode copiar esse link na barra de endereços do seu navegador. Não responda esse e-mail, ele é automático.";

        private string TextoRedefinirSenha(string token) =>
            $"Olá! Clique no link a seguir para redefinir sua senha: " +
            $"<a href='https://auxilio.portonacional.to.gov.br/Email/RedefinirSenha/{token}'>https://auxilio.portoncional.to.gov.br/Email/RedefinirSenha/{token}</a>. " +
            $"Não responda esse e-mail, ele é automático.";

    }
}
