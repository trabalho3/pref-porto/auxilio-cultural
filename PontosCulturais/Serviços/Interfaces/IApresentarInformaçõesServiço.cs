﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços.Interfaces
{
    public interface IApresentarInformaçõesServiço
    {
        string Apresentar(bool valor);
    }
}
