﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços.Interfaces
{
    public interface IEmailServiço
    {
        void EnviarEmailConfirmação(string token, string email);
        void EnviarEmailRedefinirSenha(string token, string email);
    }
}
