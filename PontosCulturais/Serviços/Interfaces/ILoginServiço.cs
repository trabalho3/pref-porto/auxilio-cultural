﻿using Microsoft.AspNetCore.Http;
using PontosCulturais.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços.Interfaces
{
    public interface ILoginServiço
    {
        Task LogarCandidatoAoAuxílioAsync(HttpContext contexto, CadastroPessoaFísicaModelo pessoaFísica);
        Task LogarAdministradorAsync(HttpContext contexto, AdminModelo administrador);
        Task LogoutAsync(HttpContext contexto);

    }
}
