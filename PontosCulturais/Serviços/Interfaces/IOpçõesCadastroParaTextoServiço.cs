﻿using PontosCulturais.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços.Interfaces
{
    public interface IOpçõesCadastroParaTextoServiço
    {
        string ConverterParaTexto(IEnumerable<IOpçõesCadastroModeloParaRepresentarEmTexto> opções);
    }
}
