﻿using PontosCulturais.Singletons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços.Interfaces
{
    public interface ISegurançaServiço
    {
        string Criptografar(string valor);
        string GerarToken(int idUsuário, PropósitoToken propósito, int horasValidade = 24);
        (StatusToken, int) ValidarToken(string token, PropósitoToken propósito);
    }
}
