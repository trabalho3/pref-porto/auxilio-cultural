﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços.Interfaces
{
    public interface IUploadServiço
    {
        public Task<string> FazerUpload(IFormFile arquivo, string idUsuário, IEnumerable<string> extensões);
        public Task<IEnumerable<string>> FazerUpload(IEnumerable<IFormFile> arquivos, string idUsuário, IEnumerable<string> extensões);
    }
}
