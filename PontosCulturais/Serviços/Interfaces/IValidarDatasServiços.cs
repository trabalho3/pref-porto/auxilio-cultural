﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços.Interfaces
{
    public interface IValidarDatasServiços
    {
        bool ValidarIdade(int idadeMínima, int idadeMáxima, DateTime nascimento);
    }
}
