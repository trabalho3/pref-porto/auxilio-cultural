﻿using PontosCulturais.Models;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços.Interfaces
{
    public interface IValidarModeloServiço<T>
    {
        AvisoViewModel Validar(T modelo);
    }
}
