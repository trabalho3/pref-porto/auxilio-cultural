﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços.Interfaces
{
    public interface IValidarStringServiço
    {
        bool ValidarCPF(string valor);
        bool ValidarRegex(string valor, string padrão);
        bool ValidarSomenteNúmeros(string valor);
        bool ValidarQuantidadeCaracteres(string valor, int mínimo = 0, int máximo = 0);
        bool ValidarQuantidadeCaracteres(string valor, int quantidade);
        bool ValidarTelefone(string valor);
        bool ValidarTelefoneQuePodeSerNulo(string valor);
        bool ValidarEmail(string valor);
        bool ValidarRG(string rg);
    }
}
