﻿using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços.Interfaces
{
    public interface IValidarTokenServiço
    {
        (AvisoViewModel, int) Validar(string token, PropósitoToken propósito);
    }
}
