﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using PontosCulturais.Models;
using PontosCulturais.Models.Interfaces;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços
{
    public class LoginServiço : ILoginServiço
    {

        private async Task Logar(HttpContext contexto, ILogávelModelo campo, string role)
        {
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, campo.NomeCompleto),
                new Claim(ClaimTypes.Email, campo.Email),
                new Claim(ClaimTypes.Role, role),
                new Claim(ClaimTypes.NameIdentifier, campo.Id.ToString())
            };

            var identidade = new ClaimsIdentity(claims, "Login");
            var usuário = new ClaimsPrincipal(identidade);
            var propriedades = new AuthenticationProperties
            {
                AllowRefresh = true,
                ExpiresUtc = DateTime.Now.AddHours(3),
                IsPersistent = true
            };

            await contexto.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, usuário, propriedades);
        }

        public async Task LogarAdministradorAsync(HttpContext contexto, AdminModelo administrador) => await Logar(contexto, administrador, ConstantesAutenticaçãoSingleton.Administrador);
        public async Task LogarCandidatoAoAuxílioAsync(HttpContext contexto, CadastroPessoaFísicaModelo pessoaFísica) => await Logar(contexto, pessoaFísica, ConstantesAutenticaçãoSingleton.CandidatoAoAuxílio);
        public async Task LogoutAsync(HttpContext contexto) => await contexto.SignOutAsync();
    }
}
