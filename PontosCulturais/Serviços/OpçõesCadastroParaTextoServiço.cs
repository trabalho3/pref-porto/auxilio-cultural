﻿using PontosCulturais.Models.Interfaces;
using PontosCulturais.Serviços.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços
{
    public class OpçõesCadastroParaTextoServiço : IOpçõesCadastroParaTextoServiço
    {
        public string ConverterParaTexto(IEnumerable<IOpçõesCadastroModeloParaRepresentarEmTexto> opções)
        {
            var retorno = $"{opções.ElementAt(0).Nome}";
            if (opções.Count() == 1) return retorno;

            retorno = $"{retorno},";

            for (int i = 1; i < opções.Count() - 1; i++)
            {
                retorno = $"{retorno} {opções.ElementAt(i).Nome},";
            }

            retorno = $"{retorno} {opções.Last().Nome}.";

            return retorno;
        }
    }
}
