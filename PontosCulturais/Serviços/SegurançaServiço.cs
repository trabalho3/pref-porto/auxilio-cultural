﻿using Microsoft.IdentityModel.Tokens;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons;
using PontosCulturais.Singletons.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços
{
    public class SegurançaServiço : ISegurançaServiço
    {
        private readonly IVariáveisAmbienteSingleton _ambiente;

        public SegurançaServiço(IVariáveisAmbienteSingleton ambiente)
        {
            _ambiente = ambiente;
        }

        public string Criptografar(string valor)
        {
            var md5 = MD5.Create();
            var bytes = Encoding.UTF8.GetBytes(valor);
            var hash = md5.ComputeHash(bytes);
            var construtorString = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                construtorString.Append(hash[i].ToString("X2"));
            }

            return construtorString.ToString();
        }

        public string GerarToken(int idUsuário, PropósitoToken propósito, int horasValidade = 24)
        {
            var bytesId = BitConverter.GetBytes(idUsuário);
            var bytesPropósito = BitConverter.GetBytes((int)propósito);
            var bytesChave = Encoding.ASCII.GetBytes(Criptografar(_ambiente.ChaveToken));
            var bytesHoje = BitConverter.GetBytes(DateTime.UtcNow.AddHours(horasValidade).ToBinary());

            var dados = new byte[bytesId.Length + bytesPropósito.Length + bytesChave.Length + bytesHoje.Length];

            Buffer.BlockCopy(bytesId, 0, dados, 0, bytesId.Length);
            Buffer.BlockCopy(bytesHoje, 0, dados, bytesId.Length, bytesHoje.Length);
            Buffer.BlockCopy(bytesPropósito, 0, dados, bytesId.Length + bytesHoje.Length, bytesPropósito.Length);
            Buffer.BlockCopy(bytesChave, 0, dados, bytesId.Length + bytesHoje.Length + bytesPropósito.Length, bytesChave.Length);

            return Base64UrlEncoder.Encode(dados);
        }

        public (StatusToken, int) ValidarToken(string token, PropósitoToken propósito)
        {
            var dados = Base64UrlEncoder.DecodeBytes(token);
            var id = BitConverter.ToInt32(dados.Take(4).ToArray(), 0);
            var data = DateTime.FromBinary(BitConverter.ToInt64(dados.Skip(4).Take(8).ToArray(), 0));
            var propósitoLido = (PropósitoToken)BitConverter.ToInt32(dados.Skip(12).Take(4).ToArray(), 0);
            var chave = Encoding.ASCII.GetString(dados.Skip(16).ToArray());

            if (DateTime.UtcNow > data) return (StatusToken.Expirado, -1);
            if (propósito != propósitoLido) return (StatusToken.PropósitoIncompatível, -1);
            if (Criptografar(_ambiente.ChaveToken) != chave) return (StatusToken.AssinaturaInválida, -1);

            return (StatusToken.Válido, id);
        }
    }
}
