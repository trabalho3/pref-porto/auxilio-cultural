﻿using Microsoft.AspNetCore.Http;
using PontosCulturais.Exceptions;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços
{
    public class UploadServiço : IUploadServiço
    {
        private readonly IVariáveisAmbienteSingleton _ambiente;

        public UploadServiço(IVariáveisAmbienteSingleton ambiente)
        {
            _ambiente = ambiente;
        }

        public async Task<string> FazerUpload(IFormFile arquivo, string idUsuário, IEnumerable<string> extensões)
        {
            if (arquivo == null || arquivo.Length == 0) throw new NullReferenceException(nameof(arquivo));

            var extensão = arquivo.FileName.Split('.')[^1];
            if (!extensões.Contains(extensão.ToLower())) throw new ExtensãoInválidaException();

            var diretórioCompleto = Path.Combine(_ambiente.CaminhoUpload, idUsuário);

            if(!Directory.Exists(diretórioCompleto))
            {
                Directory.CreateDirectory(diretórioCompleto);
            }

            var caminhoCompleto = Path.Combine(diretórioCompleto, arquivo.FileName);

            if (File.Exists(caminhoCompleto)) throw new ArquivoExistenteException();

            using var stream = new FileStream(caminhoCompleto, FileMode.Create);

            await arquivo.CopyToAsync(stream);

            return caminhoCompleto;
        }

        public async Task<IEnumerable<string>> FazerUpload(IEnumerable<IFormFile> arquivos, string idUsuário, IEnumerable<string> extensões)
        {
            var retorno = new List<string>();
            foreach (var item in arquivos)
            {
                retorno.Add(await FazerUpload(item, idUsuário, extensões));
            }

            return retorno;
        }
    }
}
