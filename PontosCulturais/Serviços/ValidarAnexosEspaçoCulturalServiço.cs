﻿using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços
{
    public class ValidarAnexosEspaçoCulturalServiço : IValidarModeloServiço<AnexosEspaçoCulturalViewModel>
    {
        private readonly IAvisoViewModelGerador _geradorAvisos;

        public ValidarAnexosEspaçoCulturalServiço(IAvisoViewModelGerador geradorAvisos)
        {
            _geradorAvisos = geradorAvisos;
        }

        public AvisoViewModel Validar(AnexosEspaçoCulturalViewModel modelo)
        {
            if (modelo.ComprovanteAtividades == null || modelo.ComprovanteAtividades.Length == 0) return _geradorAvisos.GerarMensagemErro("Comprovante de Atividades vazio", Erros.ComprovanteAtividadesVazio);
            if (modelo.ComprovanteEndereço == null || modelo.ComprovanteEndereço.Length == 0) return _geradorAvisos.GerarMensagemErro("Comprovante de Endereço vazio", Erros.ComprovanteEndereçoNulo);

            return _geradorAvisos.GerarFlagSucesso();
        }
    }
}
