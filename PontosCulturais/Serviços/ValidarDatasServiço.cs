﻿using PontosCulturais.Serviços.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços
{
    public class ValidarDatasServiço : IValidarDatasServiços
    {
        public bool ValidarIdade(int idadeMínima, int idadeMáxima, DateTime nascimento) => nascimento.AddYears(idadeMínima) <= DateTime.Now.Date && nascimento.AddYears(idadeMáxima) >= DateTime.Now.Date;
    }
}
