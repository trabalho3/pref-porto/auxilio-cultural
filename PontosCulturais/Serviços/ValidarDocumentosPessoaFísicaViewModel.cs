﻿using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços
{
    public class ValidarDocumentosPessoaFísicaViewModel : IValidarModeloServiço<DocumentosPessoaFísicaViewModel>
    {
        private readonly IAvisoViewModelGerador _geradorAvisoViewModel;

        public ValidarDocumentosPessoaFísicaViewModel(IAvisoViewModelGerador geradorAvisoViewModel)
        {
            _geradorAvisoViewModel = geradorAvisoViewModel;
        }

        public AvisoViewModel Validar(DocumentosPessoaFísicaViewModel modelo)
        {
            if (modelo.FrenteRGCNH == null) return _geradorAvisoViewModel.GerarMensagemComErroNulo("Frente do RG/CNH", Erros.FrenteRGCNHNulo);
            if (modelo.VersoRGCNH == null) return _geradorAvisoViewModel.GerarMensagemComErroNulo("Verdo do RG/CNH", Erros.VersoRGCNHNulo);
            if (modelo.Selfie == null) return _geradorAvisoViewModel.GerarMensagemComErroNulo("Selfie", Erros.SelfieNula);
            if (modelo.CPF == null) return _geradorAvisoViewModel.GerarMensagemComErroNulo("Comprovante do CPF", Erros.ComprovanteCPFNulo);
            if (modelo.ComprovanteEndereço == null) return _geradorAvisoViewModel.GerarMensagemComErroNulo("Comprovante de Endereço", Erros.ComprovanteEndereçoNulo);
            //if (modelo.ComprovanteBancário == null) return _geradorAvisoViewModel.GerarMensagemComErroNulo("Comprovante Bancário", Erros.ComprovanteBancárioNulo);
            if (modelo.ComprovanteAtividades == null) return _geradorAvisoViewModel.GerarMensagemComErroNulo("Comprovante de Atividades", Erros.ComprovanteAtividadesVazio);

            if (modelo.ComprovanteAtividades.Length > ConstantesUploadSingleton.TamanhoMáximoUpload) return _geradorAvisoViewModel.GerarMensagemErro("Comprovante de Atividades maior que 10 MB",
                 Erros.ComprovanteAtividadesGrande);

            return _geradorAvisoViewModel.GerarFlagSucesso();
        }
    }
}
