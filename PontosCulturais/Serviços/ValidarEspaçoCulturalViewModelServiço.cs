﻿using Brazil.Data.Validators;
using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços
{
    public class ValidarEspaçoCulturalViewModelServiço : IValidarModeloServiço<CadastroEspaçoCulturalViewModel>
    {
        private readonly IValidarStringServiço _validarStringServiço;
        private readonly IAvisoViewModelGerador _geradorAvisoViewModel;

        public ValidarEspaçoCulturalViewModelServiço(IValidarStringServiço validarStringServiço, IAvisoViewModelGerador avisoViewModelGerador)
        {
            _validarStringServiço = validarStringServiço;
            _geradorAvisoViewModel = avisoViewModelGerador;
        }

        public AvisoViewModel Validar(CadastroEspaçoCulturalViewModel modelo)
        {
            if (!_validarStringServiço.ValidarQuantidadeCaracteres(modelo.EspaçoCultural.Nome, 1, 50)) return _geradorAvisoViewModel.GerarMensagemComErroDeCaracteres("Nome", Erros.NomeInválido, 1, 50);
            if (!_validarStringServiço.ValidarQuantidadeCaracteres(modelo.EspaçoCultural.Endereço, 1, 100)) return _geradorAvisoViewModel.GerarMensagemComErroDeCaracteres("Endereço", Erros.EndereçoInválido, 1, 100);
            if (!_validarStringServiço.ValidarTelefone(modelo.EspaçoCultural.Telefone)) return _geradorAvisoViewModel.GerarMensagemErro("Telefone inválido", Erros.Telefone01Inválido);
            if (!ValidarCNPJ(modelo.EspaçoCultural.Cnpj)) return _geradorAvisoViewModel.GerarMensagemErro("CNPJ inválido", Erros.CNPJInválido);
            if (!ValidarRazãoSocial(modelo.EspaçoCultural.Cnpj, modelo.EspaçoCultural.RazãoSocial)) return _geradorAvisoViewModel.GerarMensagemErro("Razão Social inválida", Erros.RazãoSocialInválida);
            if (!_validarStringServiço.ValidarQuantidadeCaracteres(modelo.EspaçoCultural.Bairro, 1, 50)) return _geradorAvisoViewModel.GerarMensagemComErroDeCaracteres("Bairro", Erros.BairroInválido, 1, 50);
            if (!_validarStringServiço.ValidarQuantidadeCaracteres(modelo.EspaçoCultural.NomePopular, 0, 50))
                return _geradorAvisoViewModel.GerarMensagemComErroDeCaracteres("Nome Popúlar", Erros.NomePopularInválido, 0, 50);
            if (!_validarStringServiço.ValidarQuantidadeCaracteres(modelo.EspaçoCultural.TipoAtendimento, 1, 1000))
                return _geradorAvisoViewModel.GerarMensagemComErroDeCaracteres("Tipo de Atividade", Erros.AtuaçãoInválida, 1, 1000);

            if (modelo.EspaçoCultural.Funcionários < 0) return _geradorAvisoViewModel.GerarMensagemErro("Número de Funcionários inválido", Erros.FuncionáriosInválido);
            if (modelo.IdsTiposAtividades.Count() == 0) return _geradorAvisoViewModel.GerarMensagemErro("Selecione pelo menos um Tipo de Atividade", Erros.TiposAtividadeInválido);
            if (modelo.EspaçoCultural.LotaçãoMáxima < 0) return _geradorAvisoViewModel.GerarMensagemErro("Lotação Máxima inválida", Erros.LotaçãoMáximaInválida);
            if (modelo.IdsSegmentosCulturais.Count() == 0) return _geradorAvisoViewModel.GerarMensagemErro("Segmento Cultural inválido", Erros.SegmentoCulturalInválido);
            if (modelo.IdsEquipamentos.Count() == 0) return _geradorAvisoViewModel.GerarMensagemErro("Atividade Desenvolvida inválida", Erros.AtividadeDesenvolvidaInválida);
            if (modelo.IdsOrigensRecursos.Count() == 0) return _geradorAvisoViewModel.GerarMensagemErro("Origem dos Recursos inválida", Erros.OrigemRecursoInválida);
            if (modelo.IdsModosFuncionamento.Count() == 0) return _geradorAvisoViewModel.GerarMensagemErro("Modo de Funcionamento inválido", Erros.ModoFuncionamentoInválida);

            if (!_validarStringServiço.ValidarQuantidadeCaracteres(modelo.EspaçoCultural.InformaçõesAdicionaisAtuação, 0, 200))
                return _geradorAvisoViewModel.GerarMensagemComErroDeCaracteres("Informações Adicionais de Atuação", Erros.InformaçãoAdicionaInválida, 0, 200);

            if (!modelo.EspaçoCultural.DeclaraçãoInformaçõesVerídicas) return _geradorAvisoViewModel.GerarMensagemComErroBooleanoFalso("Declaração de Informações Verídicas", Erros.NãoDeclarouVeracidade);

            return _geradorAvisoViewModel.GerarFlagSucesso();
        }

        private bool ValidarCNPJ(string cnpj)
        {
            if (string.IsNullOrEmpty(cnpj)) return true;
            if (!_validarStringServiço.ValidarSomenteNúmeros(cnpj)) return false;
 
            return CnpjValidator.Validate(cnpj);

            //"52407852000100"; //Esse cnpj deu erro
        }

        private bool ValidarRazãoSocial(string cnpj, string razão)
        {
            if (string.IsNullOrEmpty(cnpj) ^ string.IsNullOrEmpty(razão)) return false;
            if (string.IsNullOrEmpty(cnpj) && string.IsNullOrEmpty(razão)) return true;

            return _validarStringServiço.ValidarQuantidadeCaracteres(razão, 0, 50);
        }
    }
}
