﻿using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Models;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;

namespace PontosCulturais.Serviços
{
    public class ValidarPessoaFísicaServiço : IValidarModeloServiço<CadastroPessoaFísicaModelo>
    {
        private readonly IValidarStringServiço _validarString;
        private readonly IValidarDatasServiços _validarData;
        private readonly IAvisoViewModelGerador _geradorAvisos;

        public ValidarPessoaFísicaServiço(IValidarStringServiço validarString, IValidarDatasServiços validarData, IAvisoViewModelGerador geradorAvisos)
        {
            _validarString = validarString;
            _validarData = validarData;
            _geradorAvisos = geradorAvisos;
        }

        public AvisoViewModel Validar(CadastroPessoaFísicaModelo modelo)
        {
            if (!_validarString.ValidarCPF(modelo.CPF)) return _geradorAvisos.GerarMensagemErro("CPF inválido. Verifique seu documento e digite somente os números", Erros.CPFInválido);
            if (!_validarString.ValidarQuantidadeCaracteres(modelo.NomeCompleto, 1, 100))  return _geradorAvisos.GerarMensagemComErroDeCaracteres("Nome Completo", Erros.NomeInválido, 1, 100);
            if (!_validarString.ValidarRG(modelo.RG)) return _geradorAvisos.GerarMensagemErro("RG deve ter entre 1 e 15 caracteres. Somente números", Erros.RGInválido);
            if (!_validarString.ValidarQuantidadeCaracteres(modelo.ÓrgãoEstado, 1, 10)) return _geradorAvisos.GerarMensagemComErroDeCaracteres("Órgão Emissor com Estado", Erros.ÓrgãoEstadoInválido, 1, 10);
            if (!_validarString.ValidarQuantidadeCaracteres(modelo.NomeSocial, máximo: 100))  return _geradorAvisos.GerarMensagemComErroDeCaracteres("Nome Social", Erros.NomeSocialInválido, 0, 100);;
            if (!_validarString.ValidarQuantidadeCaracteres(modelo.NomeMãe, 1, 100))  return _geradorAvisos.GerarMensagemComErroDeCaracteres("Nome da Mãe", Erros.NomeMãeInválido, 1, 100);
            if (!_validarData.ValidarIdade(18, 120, modelo.Nascimento)) return _geradorAvisos.GerarMensagemErro("Idade inválida", Erros.IdadeInválida);
            if (!_validarString.ValidarTelefone(modelo.Telefone01))  return _geradorAvisos.GerarMensagemComErroDeCaracteres("Telefone 01", Erros.Telefone01Inválido, 11);
            if (!_validarString.ValidarTelefoneQuePodeSerNulo(modelo.Telefone02))  return _geradorAvisos.GerarMensagemComErroDeCaracteres("Telefone 02", Erros.Telefone02Inválido, 11);
            if (!_validarString.ValidarEmail(modelo.Email)) return _geradorAvisos.GerarMensagemErro("E-mail inválido", Erros.EmailInválido);
            if (!_validarString.ValidarQuantidadeCaracteres(modelo.PaísNascimento, máximo: 15))  return _geradorAvisos.GerarMensagemComErroDeCaracteres("País de Nascimento", Erros.PaísNascimentoInválido, 0, 15);
            if (!_validarString.ValidarQuantidadeCaracteres(modelo.Endereço, 1, 100))  return _geradorAvisos.GerarMensagemComErroDeCaracteres("Endereço", Erros.EndereçoInválido, 1, 100);
            if (!_validarString.ValidarQuantidadeCaracteres(modelo.ComplementoEndereço, máximo: 100))  return _geradorAvisos.GerarMensagemComErroDeCaracteres("Complemento", Erros.ComplementoInválido, 0, 100);
            if (!_validarString.ValidarQuantidadeCaracteres(modelo.Bairro, 1, 50))  return _geradorAvisos.GerarMensagemComErroDeCaracteres("Bairro", Erros.BairroInválido, 1, 50);

            if (modelo.SegmentosCulturaisId.Count == 0) return _geradorAvisos.GerarMensagemErro("Selecione pelo menos um segmento cultural", Erros.SegmentoCulturalInválido);

            if (!_validarString.ValidarQuantidadeCaracteres(modelo.Atuação, 1, 1000)) return _geradorAvisos.GerarMensagemComErroDeCaracteres("Atuação", Erros.AtuaçãoInválida, 1, 1000);
            if (!_validarString.ValidarQuantidadeCaracteres(modelo.ContaBancária, 0, 20))
                return _geradorAvisos.GerarMensagemErro("Conta Bancária inválida. Use saté 20 caracteres", Erros.ContaBancáriaInválida);

            if (!_validarString.ValidarQuantidadeCaracteres(modelo.Agência, 0, 10)) return _geradorAvisos.GerarMensagemComErroDeCaracteres("Agência", Erros.AgênciaInválida, 0, 10);

            if (!_validarString.ValidarQuantidadeCaracteres(modelo.CódigoOperação, máximo: 10)) return _geradorAvisos.GerarMensagemComErroDeCaracteres("Código da Operação", Erros.CódigoOperaçãoInválido, máximo: 10);

            if ((modelo.Senha != modelo.ConfirmarSenha)) return _geradorAvisos.GerarMensagemErro("As senhas devem coincidir", Erros.SenhasDiferentes);
            if (!_validarString.ValidarQuantidadeCaracteres(modelo.Senha, 6, 20)) return _geradorAvisos.GerarMensagemComErroDeCaracteres("Senha", Erros.SenhaInválida, 6, 20);

            if (!modelo.DeclaraçãoAtuaçãoCulturaAtualmente) return _geradorAvisos.GerarMensagemComErroBooleanoFalso("Declaração de que atua em serviços culturais atualmente", Erros.NãoAtuaComCulturalAtualmente);
            if (!modelo.DeclaraçãoAtuaçãoCulturaDoisAnos) return _geradorAvisos.GerarMensagemComErroBooleanoFalso("Declaração de que atuou em serviços culturais nos últimos dois anos", 
                Erros.NãoTrabalhouComCulturaÚltimosDoisAnos);
            //if (!modelo.DeclaraçãoNãoRecebeBenefício) return _geradorAvisos.GerarMensagemComErroBooleanoFalso("Declaração de que não recebe benefícios sociais", Erros.RecebeBenefício);
            //if (!modelo.DeclaraçãoÑãoRecebeSeguroDesemprego) return _geradorAvisos.GerarMensagemComErroBooleanoFalso("Declaração de que não recebe seguro desemprego", Erros.RecebeSeguroDesemprego);
            //if (!modelo.DeclaraçãoBaixaRenda) return _geradorAvisos.GerarMensagemComErroBooleanoFalso("Declaração que é de baixa renda", Erros.DeclaraçãoBaixaRenda);
            //if (!modelo.DeclaraçãoRendimentoMédio) return _geradorAvisos.GerarMensagemComErroBooleanoFalso("Declaração de rendimento médio", Erros.RendimentoMédioAlto);
            //if (!modelo.DeclaraçãoBaixoRendimento2018) return _geradorAvisos.GerarMensagemComErroBooleanoFalso("Declaração de rendimento em 2018", Erros.RendimentoAlto2018);
            //if (!modelo.DeclaraçãoNãoRecebeAuxílioEmergencial) return _geradorAvisos.GerarMensagemComErroBooleanoFalso("Declaração de que não recebe auxílio emergencial", Erros.RecebeAuxílioEmergencial);
            if (!modelo.DeclaraçãoVeracidadeDados) return _geradorAvisos.GerarMensagemComErroBooleanoFalso("Declaração de que atesta a veracidade dos dados", Erros.NãoDeclarouVeracidade);
            if (!modelo.AutorizaçãoCruzamentoDeDados) return _geradorAvisos.GerarMensagemComErroBooleanoFalso("Autorização para cruzamento de dados", Erros.NãoAutorizouCruzamento);

            return _geradorAvisos.GerarFlagSucesso();
        }
    }
}
