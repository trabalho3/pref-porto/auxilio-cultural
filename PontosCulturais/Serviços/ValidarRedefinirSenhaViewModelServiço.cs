﻿using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços
{
    public class ValidarRedefinirSenhaViewModelServiço : IValidarModeloServiço<RedefinirSenhaViewModel>
    {
        private readonly IValidarStringServiço _validarStringServiço;
        private readonly IAvisoViewModelGerador _avisoViewModelGerador;

        public ValidarRedefinirSenhaViewModelServiço(IValidarStringServiço validarStringServiço, IAvisoViewModelGerador avisoViewModelGerador)
        {
            _validarStringServiço = validarStringServiço;
            _avisoViewModelGerador = avisoViewModelGerador;
        }

        public AvisoViewModel Validar(RedefinirSenhaViewModel modelo)
        {
            if (!_validarStringServiço.ValidarQuantidadeCaracteres(modelo.Senha, 6, 20)) return _avisoViewModelGerador.GerarMensagemComErroDeCaracteres("Senha", Erros.SenhaInválida, 1, 6);
            if (modelo.Senha != modelo.ConfirmarSenha) return _avisoViewModelGerador.GerarMensagemErro("Senhas Diferentes", Erros.SenhasDiferentes);

            return _avisoViewModelGerador.GerarFlagSucesso();
        }
    }
}
