﻿using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços
{
    public class ValidarResidenteServiço : IValidarModeloServiço<ResidenteModelo>
    {
        private readonly IValidarStringServiço _validarString;
        private readonly IAvisoViewModelGerador _geradorAviso;

        public ValidarResidenteServiço(IValidarStringServiço validarString, IAvisoViewModelGerador geradorAviso)
        {
            _validarString = validarString;
            _geradorAviso = geradorAviso;
        }

        public AvisoViewModel Validar(ResidenteModelo modelo)
        {
            if (!_validarString.ValidarQuantidadeCaracteres(modelo.CPFOuCertidão, 11, 50)) return _geradorAviso.GerarMensagemComErroDeCaracteres("CPF ou Registro de Nascimento", Erros.CPFInválido, 11, 50);
            if (modelo.CPFOuCertidão.Length == 11)
            {
                if (!_validarString.ValidarCPF(modelo.CPFOuCertidão)) return _geradorAviso.GerarMensagemErro("CPF Inválido", Erros.CPFInválido);
            }
            if (!_validarString.ValidarQuantidadeCaracteres(modelo.Nome, 1, 100)) return _geradorAviso.GerarMensagemComErroDeCaracteres("Nome Completo", Erros.NomeInválido, 1, 100);
            if (!_validarString.ValidarQuantidadeCaracteres(modelo.Parentesco, 1, 30)) return _geradorAviso.GerarMensagemComErroDeCaracteres("Parentesco", Erros.ParentescoInválido, 1, 30);

            return _geradorAviso.GerarFlagSucesso();
        }
    }
}
