﻿using Brazil.Data.Validators;
using PontosCulturais.Serviços.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços
{
    public class ValidarStringServiço : IValidarStringServiço
    {
        public bool ValidarSomenteNúmeros(string valor) => ValidarRegex(valor, @"^[0-9]+$");

        public bool ValidarRegex(string valor, string padrão)
        {
            if (string.IsNullOrEmpty(valor) || string.IsNullOrEmpty(padrão)) return false;

            var regex = new Regex(padrão);
            return regex.Match(valor).Success;

        }

        public bool ValidarQuantidadeCaracteres(string valor, int mínimo = 0, int máximo = 0)
        {
            if (string.IsNullOrEmpty(valor) && mínimo == 0) return true;
            if (string.IsNullOrEmpty(valor) && mínimo != 0) return false;
            if (mínimo != 0 && valor.Length < mínimo) return false;
            if (máximo != 0 && valor.Length > máximo) return false;

            return true;
        }

        public bool ValidarQuantidadeCaracteres(string valor, int quantidade)
        {
            if (quantidade <= 0 || string.IsNullOrEmpty(valor)) return false;

            return valor.Length == quantidade;
        }

        public bool ValidarTelefone(string valor) => !string.IsNullOrEmpty(valor) && (valor.Length == 11 || valor.Length == 10) && ValidarSomenteNúmeros(valor);

        public bool ValidarEmail(string valor) => ValidarRegex(valor, @"^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$");

        public bool ValidarTelefoneQuePodeSerNulo(string valor) => string.IsNullOrEmpty(valor) || ValidarTelefone(valor);

        public bool ValidarCPF(string valor) => CpfValidator.Validate(valor) && ValidarSomenteNúmeros(valor);

        public bool ValidarRG(string rg) => ValidarQuantidadeCaracteres(rg, 1, 15) && ValidarSomenteNúmeros(rg);
    }
}
