﻿using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Serviços
{
    public class ValidarTokenServiço : IValidarTokenServiço
    {

        private readonly IAvisoViewModelGerador _avisoViewModelGerador;
        private readonly ISegurançaServiço _segurançaServiço;

        public ValidarTokenServiço(IAvisoViewModelGerador avisoViewModelGerador, ISegurançaServiço segurançaServiço)
        {
            _avisoViewModelGerador = avisoViewModelGerador;
            _segurançaServiço = segurançaServiço;
        }

        public (AvisoViewModel, int) Validar(string token, PropósitoToken propósito)
        {
            if (string.IsNullOrEmpty(token)) return (_avisoViewModelGerador.GerarMensagemErro("Token vazio", Erros.TokenVazio), -1);

            var análise = _segurançaServiço.ValidarToken(token, propósito);

            if (análise.Item1 == StatusToken.AssinaturaInválida) return (_avisoViewModelGerador.GerarMensagemErro("Token inválido", Erros.TokenInválido), análise.Item2);
            if (análise.Item1 == StatusToken.Expirado) return (_avisoViewModelGerador.GerarMensagemErro("Token expirou", Erros.TokenExpirou), análise.Item2);
            if (análise.Item1 == StatusToken.PropósitoIncompatível) return (_avisoViewModelGerador.GerarMensagemErro("Token gerado em outra situação", Erros.TokenIncompatível), análise.Item2);

            return (_avisoViewModelGerador.GerarFlagSucesso(), análise.Item2);
        }
    }
}
