﻿using Microsoft.AspNetCore.Server.IIS.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Singletons
{
    public static class ConstantesAutenticaçãoSingleton
    {
        public const string Administrador = "Admin";
        public const string CandidatoAoAuxílio = "Candidato";
    }
}
