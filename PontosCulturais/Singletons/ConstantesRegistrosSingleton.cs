﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Singletons
{
    public static class ConstantesRegistrosSingleton
    {
        public const int EspaçoArtísticoECulturalId = 1;
        public const int NãoPreencherResponsávelEspaçoCulturalId = 8;
        public const int ColetivoCulturalId = 1;
        public const int EspaçoPúblicId = 6;
    }
}
