﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Singletons
{
    public static class ConstantesUploadSingleton
    {
        public const int TamanhoMáximoUpload = 10 * 1024 * 1024; //MB

        public static readonly IEnumerable<string> ExtensõesDocumentos = new string[] { "jpg", "png", "jpeg", "pdf", "docx", "doc", "ppt", "pptx" };
        public static readonly IEnumerable<string> ExtensõesFoto = new string[] { "jpg", "png", "jpeg", "pdf" };
        public static readonly IEnumerable<string> ExtensõesAtividades = new string[] { "jpg", "png", "jpeg", "pdf", "docx", "doc", "ppt", "pptx", "avi", "mp4", "zip", "rar", "7z", "wmv" };

        public static string ExtensõesDocumentosForm => Concatenar(ExtensõesDocumentos, ", ");
        public static string ExtensõesFotoForm => Concatenar(ExtensõesFoto, ", ");
        public static string ExtensõesAtividadesForm => Concatenar(ExtensõesAtividades, ", ");

        private static string Concatenar(IEnumerable<string> lista, string junção)
        {
            var listaComPonto = lista.Select(e => $".{e}");

            var retorno = listaComPonto.First();
            for (int i = 1; i < listaComPonto.Count(); i++)
            {
                retorno = $"{retorno}{junção}{listaComPonto.ElementAt(i)}";
            }

            return retorno;
        }

    }
}
