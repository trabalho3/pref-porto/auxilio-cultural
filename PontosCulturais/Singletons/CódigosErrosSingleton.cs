﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Singletons
{
    public enum Erros
    {
        Nenhum,
        ExceçãoLançada,
        CamposLoginVazios,
        UsuárioNãoEncontrado,
        CPFRepetidoCadastroCadastro, //CPF já usado em um cadastro e tentativa de uso em outro cadastro
        CPFRepetidoCadastroResidente, //CPF já usado em um cadastro e tentativa de uso em um residente
        CPFRepetidoResidenteCadastro, //CPF já usado em um residente e tentativa de uso em um cadastro
        CPFRepetidoResidenteResidente, //CPF já usado em um residente e tentativa de uso em um residente
        RGRepetido,
        CPFInválido,
        NomeInválido,
        RGInválido,
        ÓrgãoEstadoInválido,
        NomeSocialInválido,
        NomeMãeInválido,
        IdadeInválida,
        Telefone01Inválido,
        Telefone02Inválido,
        EmailInválido,
        EmailRepetido,
        PaísNascimentoInválido,
        EndereçoInválido,
        ComplementoInválido,
        BairroInválido,
        SegmentoCulturalInválido,
        AtuaçãoInválida,
        ContaBancáriaInválida,
        CódigoOperaçãoInválido,
        SenhasDiferentes,
        SenhaInválida,
        NãoAtuaComCulturalAtualmente,
        NãoTrabalhouComCulturaÚltimosDoisAnos,
        RecebeBenefício,
        RecebeSeguroDesemprego,
        DeclaraçãoBaixaRenda,
        RendimentoMédioAlto,
        RendimentoAlto2018,
        RecebeAuxílioEmergencial,
        NãoDeclarouVeracidade,
        NãoAutorizouCruzamento,
        ParentescoInválido,
        FrenteRGCNHNulo,
        VersoRGCNHNulo,
        SelfieNula,
        ComprovanteCPFNulo,
        ComprovanteEndereçoNulo,
        ComprovanteBancárioNulo,
        CNPJInválido,
        RazãoSocialInválida,
        NomePopularInválido,
        FuncionáriosInválido,
        TiposAtividadeInválido,
        LotaçãoMáximaInválida,
        AtividadeDesenvolvidaInválida,
        InformaçãoAdicionaInválida,
        OrigemRecursoInválida,
        ModoFuncionamentoInválida,
        CNPJRepetido,
        ComprovanteAtividadesVazio,
        AnexoAntesEspaço,
        ComprovanteCNPJVazio,
        AlvaráVazio,
        ComprovanteFuncionáriosVazio,
        ComprovanteAtividadesGrande,
        EmailNãoConfirmado,
        TokenInválido,
        TokenExpirou,
        TokenIncompatível,
        TokenVazio,
        AgênciaInválida
    }
}
