﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Singletons
{
    public enum StatusToken
    {
        Válido,
        Expirado,
        PropósitoIncompatível,
        AssinaturaInválida
    }

    public enum PropósitoToken
    {
        ConfirmarEmail,
        RedefinirSenha
    }
}
