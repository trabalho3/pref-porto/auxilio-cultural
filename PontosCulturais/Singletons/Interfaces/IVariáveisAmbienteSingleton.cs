﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PontosCulturais.Singletons.Interfaces
{
    public interface IVariáveisAmbienteSingleton
    {
        string CaminhoUpload { get; }
        string ChaveToken { get; }
        string UsuárioServidorEmail { get; }
        string ServidorEmail { get; }
        string ServidorEmailPorta { get; }
        string SenhaServidorEmail { get; }
    }
}
