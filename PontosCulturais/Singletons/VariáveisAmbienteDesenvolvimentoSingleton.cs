﻿using PontosCulturais.Singletons.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Singletons
{
    public class VariáveisAmbienteDesenvolvimentoSingleton : IVariáveisAmbienteSingleton
    {
        public string CaminhoUpload => Environment.GetEnvironmentVariable("CaminhoUpload", EnvironmentVariableTarget.Process);

        public string ChaveToken => Environment.GetEnvironmentVariable("ChaveToken", EnvironmentVariableTarget.User);

        public string UsuárioServidorEmail => Environment.GetEnvironmentVariable("UsuarioEmailSistemasPorto", EnvironmentVariableTarget.User);

        public string ServidorEmail => Environment.GetEnvironmentVariable("ServidorEmailSistemasPorto", EnvironmentVariableTarget.User);

        public string ServidorEmailPorta => Environment.GetEnvironmentVariable("PortaEmailSistemasPorto", EnvironmentVariableTarget.User);

        public string SenhaServidorEmail => Environment.GetEnvironmentVariable("SenhaEmailSistemasPorto", EnvironmentVariableTarget.User);
    }
}
