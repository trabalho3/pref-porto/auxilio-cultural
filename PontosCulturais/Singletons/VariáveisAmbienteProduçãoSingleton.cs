﻿using PontosCulturais.Singletons.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.Singletons
{
    public class VariáveisAmbienteProduçãoSingleton : IVariáveisAmbienteSingleton
    {
        public string CaminhoUpload => Environment.GetEnvironmentVariable("CaminhoUpload", EnvironmentVariableTarget.Process);

        public string ChaveToken => Environment.GetEnvironmentVariable("ChaveToken", EnvironmentVariableTarget.Process);

        public string UsuárioServidorEmail => Environment.GetEnvironmentVariable("UsuarioEmailSistemasPorto", EnvironmentVariableTarget.Process);

        public string ServidorEmail => Environment.GetEnvironmentVariable("ServidorEmailSistemasPorto", EnvironmentVariableTarget.Process);

        public string ServidorEmailPorta => Environment.GetEnvironmentVariable("PortaEmailSistemasPorto", EnvironmentVariableTarget.Process);

        public string SenhaServidorEmail => Environment.GetEnvironmentVariable("SenhaEmailSistemasPorto", EnvironmentVariableTarget.Process);
    }
}
