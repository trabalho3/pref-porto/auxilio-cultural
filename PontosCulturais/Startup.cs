﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PontosCulturais.Contexto;
using PontosCulturais.Geradores;
using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.Repositórios;
using PontosCulturais.Repositórios.Interface;
using PontosCulturais.Serviços;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.Singletons;
using PontosCulturais.Singletons.Interfaces;
using PontosCulturais.ViewModels;

namespace PontosCulturais
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddDbContext<AppContexto>();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = "/Painel/Entrar";
                    options.AccessDeniedPath = "/Painel/Negado";
                    options.LogoutPath = "/Painel/Entrar";
                });

            //Serviços
            services.AddScoped<IValidarDatasServiços, ValidarDatasServiço>();
            services.AddScoped<IValidarStringServiço, ValidarStringServiço>();
            services.AddScoped<IValidarModeloServiço<CadastroPessoaFísicaModelo>, ValidarPessoaFísicaServiço>();
            services.AddScoped<IValidarModeloServiço<ResidenteModelo>, ValidarResidenteServiço>();
            services.AddScoped<ISegurançaServiço, SegurançaServiço>();
            services.AddScoped<IUploadServiço, UploadServiço>();
            services.AddScoped<IValidarModeloServiço<DocumentosPessoaFísicaViewModel>, ValidarDocumentosPessoaFísicaViewModel>();
            services.AddScoped<IValidarModeloServiço<CadastroEspaçoCulturalViewModel>, ValidarEspaçoCulturalViewModelServiço>();
            services.AddScoped<IOpçõesCadastroParaTextoServiço, OpçõesCadastroParaTextoServiço>();
            services.AddScoped<IApresentarInformaçõesServiço, ApresentarInformaçõesServiço>();
            services.AddScoped<IValidarModeloServiço<AnexosEspaçoCulturalViewModel>, ValidarAnexosEspaçoCulturalServiço>();
            services.AddScoped<IEmailServiço, EmailServiço>();
            services.AddScoped<IValidarTokenServiço, ValidarTokenServiço>();
            services.AddScoped<IValidarModeloServiço<RedefinirSenhaViewModel>, ValidarRedefinirSenhaViewModelServiço>();
            services.AddScoped<ILoginServiço, LoginServiço>();

            //Geradores
            services.AddScoped<IViewModelControllersGerador<CadastroPessoaFísicaViewModel>, CadastroPessoaFísicaViewModelGerador>();
            services.AddScoped<IViewModelControllersGerador<LoginViewModel>, LoginViewModelGerador>();
            services.AddScoped<IAvisoViewModelGerador, AvisoViewModelGerador>();
            services.AddScoped<IResidentesViewModelGerador, ResidentesViewModelGerador>();
            services.AddScoped<IMostrarDadosViewModelGerador, MostrarDadosViewModelGerador>();
            services.AddScoped<IViewModelControllersGerador<DocumentosPessoaFísicaViewModel>, DocumentosPessoaFísicaViewModelGerador>();
            services.AddScoped<IViewModelControllersGerador<CadastroEspaçoCulturalViewModel>, CadastroEspaçoCulturalViewModelGerador>();
            services.AddScoped<IMostrarEspaçoCulturalViewModelGerador, MostrarEspaçoCulturalViewModelGerador>();
            services.AddScoped<IViewModelControllersGerador<AnexosEspaçoCulturalViewModel>, AnexosEspaçoCulturalViewModelGerador>();
            services.AddScoped<IViewModelControllersGerador<EmailViewModel>, EmailViewModelGerador>();
            services.AddScoped<IViewModelControllersGerador<RedefinirSenhaViewModel>, RedefinirSenhaViewModelGerador>();

            //Repositórios
            services.AddScoped<IOpçõesCadastroPessoaFísicaRepositório, OpçõesCadastroPessoaFísicaRepositório>();
            services.AddScoped<ICadastroPessoaFísicaRepositório, CadastroPessoaFísicaRepositório>();
            services.AddScoped<ISegmentosCulturaisRepositório, SegmentosCulturaisRepositório>();
            services.AddScoped<IResidentesRepositório, ResidentesRepositório>();
            services.AddScoped<IDocumentosPessoaFísicaRepositório, DocumentosPessoaFísicaRepositório>();
            services.AddScoped<IOpçõesCadastroEspaçoCulturalRepositório, OpçõesCadastroEspaçoCulturalRepositório>();
            services.AddScoped<IEspaçoCulturalRepositório, EspaçoCulturalRepositório>();
            services.AddScoped<IAnexosEspaçoCulturalRepositório, AnexosEspaçoCulturalRepositório>();
            services.AddScoped<IAdminRepositório, AdminRepositório>();

            //Singletons
#if DEBUG
            services.AddSingleton<IVariáveisAmbienteSingleton, VariáveisAmbienteDesenvolvimentoSingleton>();
#else
            services.AddSingleton<IVariáveisAmbienteSingleton, VariáveisAmbienteProduçãoSingleton>();
#endif
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
