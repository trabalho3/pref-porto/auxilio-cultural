﻿using Microsoft.AspNetCore.Http;
using PontosCulturais.Models.CadastroEspaçoCultural;
using PontosCulturais.ViewModels.Base;

namespace PontosCulturais.ViewModels
{
    public class AnexosEspaçoCulturalViewModel : ComAvisoViewModelBase
    {
        public IFormFile ComprovanteEndereço { get; set; }
        public IFormFile ComprovanteCNPJ { get; set; }
        public IFormFile ComprovanteFuncionários { get; set; }
        public IFormFile ComprovanteAlvará { get; set; }
        public IFormFile ComprovanteAtividades { get; set; }
    }
}
