﻿using PontosCulturais.Singletons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.ViewModels
{
    public class AvisoViewModel
    {
        public bool Erro { get; set; }
        public string Classe { get; set; }
        public string Mensagem { get; set; }
        public Erros CódigoErro { get; set; }
    }
}
