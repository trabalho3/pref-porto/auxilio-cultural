﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.ViewModels.Base
{
    public abstract class ComAvisoViewModelBase
    {
        public AvisoViewModel Aviso { get; set; }
    }
}
