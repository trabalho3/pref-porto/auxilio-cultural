﻿using PontosCulturais.Models;
using PontosCulturais.Models.CadastroEspaçoCultural;
using PontosCulturais.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.ViewModels
{
    public class CadastroEspaçoCulturalViewModel : ComAvisoViewModelBase
    {
        public EspaçoCulturalSimplesModelo EspaçoCultural { get; set; } = new EspaçoCulturalSimplesModelo();
        public IEnumerable<TipoDeAtividadeModelo> TiposAtividades { get; set; }
        public IEnumerable<EquipamentoCulturalModelo> EquipamentosCulturais { get; set; }
        public IEnumerable<SituaçãoBeneficiárioModelo> SituaçõesBeneficiário { get; set; }
        public IEnumerable<ReceitaAnualModelo> ReceitasAnuais { get; set; }
        public IEnumerable<GastoAnualModelo> GastosAnuais { get; set; }
        public IEnumerable<OrigemRecursosModelo> OrigensRecursos { get; set; }
        public IEnumerable<SituaçãoAtividadeModelo> SituaçõesAtividade { get; set; }
        public IEnumerable<ModoFuncionamentoModelo> ModosFuncionamento { get; set; }
        public IEnumerable<SegmentoCulturalModelo> SegmentosCulturais { get; set; }

        public List<int> IdsTiposAtividades { get; set; }
        public List<int> IdsEquipamentos { get; set; }
        public List<int> IdsOrigensRecursos { get; set; }
        public List<int> IdsModosFuncionamento { get; set; }
        public List<int> IdsSegmentosCulturais { get; set; }
    }
}

