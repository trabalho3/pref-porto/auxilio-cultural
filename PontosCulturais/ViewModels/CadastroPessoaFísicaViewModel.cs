﻿using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.ViewModels
{
    public class CadastroPessoaFísicaViewModel : ComAvisoViewModelBase
    {
        public CadastroPessoaFísicaModelo Cadastro { get; set; }
        public IEnumerable<GêneroModelo> Gêneros { get; set; }
        public IEnumerable<RaçaModelo> Raças { get; set; }
        public IEnumerable<DeficiênciaModelo> Deficiências { get; set; }
        public IEnumerable<ComunidadeTradicionalModelo> Comunidades { get; set; }
        public IEnumerable<FunçãoCulturalModelo> FunçõesCulturais { get; set; }
        public IEnumerable<SituaçãoTrabalhoModelo> SituaçõesTrabalho { get; set; }
        public IEnumerable<TipoContaBancáriaModelo> TiposContasBancárias { get; set; }
        public IEnumerable<BancoModelo> Bancos { get; set; }
        public IEnumerable<SegmentoCulturalModelo> SegmentosCulturais { get; set; }

    }
}
