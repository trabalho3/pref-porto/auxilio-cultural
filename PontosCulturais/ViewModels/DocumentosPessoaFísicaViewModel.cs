﻿using Microsoft.AspNetCore.Http;
using PontosCulturais.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.ViewModels
{
    public class DocumentosPessoaFísicaViewModel : ComAvisoViewModelBase
    {
        public IFormFile FrenteRGCNH { get; set; }
        public IFormFile VersoRGCNH { get; set; }
        public IFormFile Selfie { get; set; }
        public IFormFile CPF { get; set; }
        public IFormFile ComprovanteEndereço { get; set; }
        public IFormFile ComprovanteBancário { get; set; }
        public IFormFile ComprovanteAtividades { get; set; }
    }
}
