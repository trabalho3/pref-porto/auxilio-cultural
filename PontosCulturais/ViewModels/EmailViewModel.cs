﻿using PontosCulturais.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.ViewModels
{
    public class EmailViewModel : ComAvisoViewModelBase
    {
        public string Email { get; set; }
    }
}
