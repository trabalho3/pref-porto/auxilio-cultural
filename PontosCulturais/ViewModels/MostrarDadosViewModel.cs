﻿using PontosCulturais.Models;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.ViewModels
{
    public class MostrarDadosViewModel : ComAvisoViewModelBase
    {

        private readonly IOpçõesCadastroParaTextoServiço _opçõesParaTextoServiço;

        public MostrarDadosViewModel(IOpçõesCadastroParaTextoServiço opçõesParaTextoServiço)
        {
            _opçõesParaTextoServiço = opçõesParaTextoServiço;
        }

        public CadastroPessoaFísicaModelo PessoaFísica { get; set; }
        public IEnumerable<SegmentoCulturalModelo> SegmentosCulturais { get; set; }
        public string SegmentosCulturaisTexto => _opçõesParaTextoServiço.ConverterParaTexto(SegmentosCulturais);
    }
}
