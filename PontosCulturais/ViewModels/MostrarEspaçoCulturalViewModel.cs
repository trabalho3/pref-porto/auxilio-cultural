﻿using PontosCulturais.Models;
using PontosCulturais.Models.CadastroEspaçoCultural;
using PontosCulturais.Serviços.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.ViewModels
{
    public class MostrarEspaçoCulturalViewModel
    {
        public MostrarEspaçoCulturalViewModel(IOpçõesCadastroParaTextoServiço opçõesParaTexto, EspaçoCulturalSimplesModelo espaço, IEnumerable<TipoDeAtividadeModelo> atividades, IEnumerable<OrigemRecursosModelo> origens, 
            IEnumerable<ModoFuncionamentoModelo> funcionamentos, IEnumerable<SegmentoCulturalModelo> segmentos, IEnumerable<EquipamentoCulturalModelo> equipamentos)
        {
            Atividades = opçõesParaTexto.ConverterParaTexto(atividades);
            Origem = opçõesParaTexto.ConverterParaTexto(origens);
            Funcionamento = opçõesParaTexto.ConverterParaTexto(funcionamentos);
            Segmentos = opçõesParaTexto.ConverterParaTexto(segmentos);
            Equipamentos = opçõesParaTexto.ConverterParaTexto(equipamentos);
            EspaçoCultural = espaço;
        }

        public EspaçoCulturalSimplesModelo EspaçoCultural { get; }
        public string Atividades { get; }
        public string Origem { get; }
        public string Funcionamento { get; }
        public string Segmentos { get; }
        public string Equipamentos { get; }
    }
}
