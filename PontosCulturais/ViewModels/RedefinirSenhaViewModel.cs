﻿using PontosCulturais.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.ViewModels
{
    public class RedefinirSenhaViewModel : ComAvisoViewModelBase
    {
        public string Senha { get; set; }
        public string ConfirmarSenha { get; set; }
        public string Token { get; set; }
    }
}
