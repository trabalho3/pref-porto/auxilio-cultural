﻿using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PontosCulturais.ViewModels
{
    public class ResidentesViewModel : ComAvisoViewModelBase
    {
        public IEnumerable<ResidenteModelo> Residentes { get; set; }
        public ResidenteModelo Residente { get; set; }
    }
}
