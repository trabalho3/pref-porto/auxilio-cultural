﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using PontosCulturais.Contexto;
using PontosCulturais.Controllers;
using PontosCulturais.Models;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using TestesCadastroPessoaFísica.Dados;
using TestesControllers.Dados;
using TestesControllers.Serviços;
using TestesControllers.Serviços.Interfaces;
using Xunit;

namespace TestesControllers
{
    public class CadastrarEspaçoCulturalTestes
    {
        private static int testesAnexosCorretos = 0;
        private static int testesEspaçosCorretos = 0;
        private readonly IVerificarErroServiço _verificarErroServiço;

        public CadastrarEspaçoCulturalTestes()
        {
            _verificarErroServiço = new VerificarErroServiço();
        }

        [Theory]
        [ClassData(typeof(EspaçoCulturalDadosCorretos))]
        public void VerificarCadastrosCorretos(CadastroEspaçoCulturalViewModel espaço)
        {
            var cadastro = PessoaFísicaDados.RetornarCadastroCorreto();
            var controllerEspaçoCultural = RetornarControllerLogado(cadastro, $"testesEspaçosCorretos{testesEspaçosCorretos}");
            testesEspaçosCorretos++;

            var resultado = controllerEspaçoCultural.InserirEspaco(espaço) as RedirectToActionResult;
            Assert.Equal(nameof(EspacoCulturalController.MostrarEspaco), resultado.ActionName);
        }

        [Theory]
        [ClassData(typeof(EspaçoCulturalDadosErrados))]
        public void VerificarErrosDadoCadastroEErroEsperado(CadastroEspaçoCulturalViewModel espaçoCultural, Erros erroEsperado)
        {
            var cadastro = PessoaFísicaDados.RetornarCadastroCorreto();
            var controllerEspaçoCultural = RetornarControllerLogado(cadastro, "Memória07");

            var resultado = controllerEspaçoCultural.InserirEspaco(espaçoCultural) as ViewResult;
            Assert.True(_verificarErroServiço.VerificarErro(erroEsperado, resultado));
        }

        [Fact]
        public void VerificarCadastroCNPJRepetido()
        {
            var cadastro = PessoaFísicaDados.RetornarCadastroCorreto();
            var controllerEspaçoCultural = RetornarControllerLogado(cadastro, "Memória08");

            var espaçoViewModel = EspaçoCulturalDados.RetornarEspaçoCorreto();
            var outro = EspaçoCulturalDados.RetornarEspaçoCorreto();
            controllerEspaçoCultural.InserirEspaco(espaçoViewModel);

            var resultado = controllerEspaçoCultural.InserirEspaco(outro) as ViewResult;
            Assert.True(_verificarErroServiço.VerificarErro(Erros.CNPJRepetido, resultado));
        }

        [Fact]
        public async Task VerificarErroCadastrarAnexosAntesCadastrarEspaço()
        {
            var cadastro = PessoaFísicaDados.RetornarCadastroCorreto();
            var controllerEspaçoCultural = RetornarControllerLogado(cadastro, "Memória09");

            var resultado = await controllerEspaçoCultural.InserirAnexos(new AnexosEspaçoCulturalViewModel()) as ViewResult;
            Assert.True(_verificarErroServiço.VerificarErro(Erros.AnexoAntesEspaço, resultado));
        }

        [Fact]
        public async Task VerificarErrosValidaçãoUploadAnexosDadosAnexosEErrosEsperados()
        {
            var cadastro = PessoaFísicaDados.RetornarCadastroCorreto();
            var espaço = EspaçoCulturalDados.RetornarEspaçoCorreto();
            var controllerEspaçoCultural = RetornarControllerComEspaçoCadastrado(cadastro, espaço, "Memória10");

            foreach (var item in new AnexosEspaçoCulturalErrosValidação())
            {
                var anexo = item[0] as AnexosEspaçoCulturalViewModel;
                var erroEsperado = (Erros)item[1];

                var resultado = await controllerEspaçoCultural.InserirAnexos(anexo) as ViewResult;

                Assert.True(_verificarErroServiço.VerificarErro(erroEsperado, resultado));
            }

        }

        [Theory]
        [ClassData(typeof(AnexosEspaçoCulturalCorretos))]
        public async Task VerificarAnexosCorretosDadosAnexos(CadastroEspaçoCulturalViewModel espaço, AnexosEspaçoCulturalViewModel anexos)
        {
            var cadastro = PessoaFísicaDados.RetornarCadastroCorreto();
            var controller = RetornarControllerComEspaçoCadastrado(cadastro, espaço, $"TesteAnexosCorretos{testesAnexosCorretos}");
            testesAnexosCorretos++;

            var caminhoDiretório = Path.Combine(Directory.GetCurrentDirectory(), cadastro.Id.ToString());

            var caminhoEndereço = Path.Combine(caminhoDiretório, anexos.ComprovanteEndereço.FileName);
            var caminhoAtividades = Path.Combine(caminhoDiretório, anexos.ComprovanteAtividades.FileName);

            var dadosCNPJ = RetornarCaminhoSeExistir(caminhoDiretório, anexos.ComprovanteCNPJ);
            var dadosAlvará = RetornarCaminhoSeExistir(caminhoDiretório, anexos.ComprovanteAlvará);
            var dadosFuncionários = RetornarCaminhoSeExistir(caminhoDiretório, anexos.ComprovanteFuncionários);
            

            var resultado = await controller.InserirAnexos(anexos) as ViewResult;

            Assert.Equal("Sucesso", resultado.ViewName);
            Assert.True(File.Exists(caminhoEndereço));
            Assert.True(File.Exists(caminhoAtividades));

            Assert.Equal(dadosCNPJ.Item1, File.Exists(dadosCNPJ.Item2));
            Assert.Equal(dadosAlvará.Item1, File.Exists(dadosAlvará.Item2));
            Assert.Equal(dadosFuncionários.Item1, File.Exists(dadosFuncionários.Item2));

            //Limpeza

            File.Delete(caminhoEndereço);
            File.Delete(caminhoAtividades);

            DeletarSeExistir(dadosCNPJ.Item2);
            DeletarSeExistir(dadosAlvará.Item2);
            DeletarSeExistir(dadosFuncionários.Item2);

        }

        private void DeletarSeExistir(string alvo)
        {
            if (string.IsNullOrEmpty(alvo)) return;

            File.Delete(alvo);
        }

        private (bool, string) RetornarCaminhoSeExistir(string caminhoBase, IFormFile arquivo)
        {
            if (arquivo == null) return (false, "");

            return (true, Path.Combine(caminhoBase, arquivo.FileName));
        }

        private EspacoCulturalController RetornarControllerLogado(CadastroPessoaFísicaModelo cadastro, string nomeBancoDeDados)
        {
            var opções = new DbContextOptionsBuilder().UseInMemoryDatabase(nomeBancoDeDados);
            var contexto = new AppContexto(opções.Options);

            var controllerPessoaFísica = ControllersDados.RetornarControllerPessoaFísica(contexto);
            var controllerEspaçoCultural = ControllersDados.RetornarControllerEspaçoCultural(contexto);

            var viewModel = new CadastroPessoaFísicaViewModel { Cadastro = cadastro };
            controllerPessoaFísica.Finalizar(viewModel); //Inserir um cadastro

            var usuário = LoginDados.RetornarUsuário(cadastro);

            controllerEspaçoCultural.ControllerContext = new ControllerContext();
            controllerEspaçoCultural.ControllerContext.HttpContext = new DefaultHttpContext { User = usuário }; //Simular Login

            return controllerEspaçoCultural;
        }

        private EspacoCulturalController RetornarControllerComEspaçoCadastrado(CadastroPessoaFísicaModelo cadastro, CadastroEspaçoCulturalViewModel espaço, string nomeBancoDeDados)
        {
            var controller = RetornarControllerLogado(cadastro, nomeBancoDeDados);
            controller.InserirEspaco(espaço);

            return controller;
        }


    }
}
