﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using PontosCulturais.Contexto;
using PontosCulturais.Controllers;
using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;
using PontosCulturais.ViewModels.Base;
using System.IO;
using System.Threading.Tasks;
using TestesCadastroPessoaFísica.Dados;
using TestesControllers.Serviços;
using TestesControllers.Serviços.Interfaces;
using TestesServiços.Dados;
using Xunit;

namespace TestesControllers
{
    public class CadastrarPessoaFísicaTestes
    {

        private readonly IVerificarErroServiço _verificarErroServiço;

        public CadastrarPessoaFísicaTestes()
        {
            _verificarErroServiço = new VerificarErroServiço();
        }

        [Theory]
        [ClassData(typeof(CadastroDadosCorretos))]
        //[ClassData(typeof(CadastroDadosIncorretos))]
        public void VerificarCadastroDadoCadastroEResultadoEsperado(CadastroPessoaFísicaModelo cadastro, bool sucessoEsperado, Erros erroEsperado)
        {
            var opções = new DbContextOptionsBuilder().UseInMemoryDatabase("Memória01");
            var contexto = new AppContexto(opções.Options);

            var controller = ControllersDados.RetornarControllerPessoaFísica(contexto);
            var viewModel = new CadastroPessoaFísicaViewModel { Cadastro = cadastro };

            var resultado = controller.Finalizar(viewModel) as ViewResult;
            var nomeAction = resultado.ViewName;

            if (sucessoEsperado) Assert.Equal("Sucesso", nomeAction);
            else Assert.True(_verificarErroServiço.VerificarErro(erroEsperado, resultado));
        }

        [Fact]
        public void ProibirCPFRepetidoResidenteDadoResidentes()
        {
            var cadastro = PessoaFísicaDados.RetornarCadastroCorreto();
            var controller = RetornarControllerLogado(cadastro, "Memória02");

            var residenteVálido = ResidentesDados.RetornarResidenteVálido(cadastro);
            controller.CadastrarResidente(new ResidentesViewModel { Residente = residenteVálido }); //Inserir um residente válido

            var residenteMesmoCPFDoCadastro = new ResidenteModelo { CadastroPessoaFísicaModelo = cadastro, CPFOuCertidão = cadastro.CPF, Nome = "Residente Inválido", Parentesco = "Teste" };
            var resultado = controller.CadastrarResidente(new ResidentesViewModel { Residente = residenteMesmoCPFDoCadastro }) as ViewResult;
            Assert.True(_verificarErroServiço.VerificarErro(Erros.CPFRepetidoResidenteCadastro, resultado));

            var residenteMesmoCPFDoOutroResidente = new ResidenteModelo { CadastroPessoaFísicaModelo = cadastro, CPFOuCertidão = residenteVálido.CPFOuCertidão, Nome = "Errado", Parentesco = "Teste" };
            resultado = controller.CadastrarResidente(new ResidentesViewModel { Residente = residenteMesmoCPFDoOutroResidente }) as ViewResult;
            Assert.True(_verificarErroServiço.VerificarErro(Erros.CPFRepetidoResidenteResidente, resultado));
        }

        [Theory]
        [ClassData(typeof(ResidentesDadosCorretos))]
        [ClassData(typeof(ResidentesDadosIncorretos))]
        public void VerificarResidenteVálidoDadoResidentesEResultadoEsperado(ResidenteModelo residente, bool sucessoEsperado, Erros erroEsperado = Erros.Nenhum)
        {
            var cadastro = PessoaFísicaDados.RetornarCadastroCorreto();
            var controller = RetornarControllerLogado(cadastro, "Memória03");

            residente.CadastroPessoaFísicaModelo = cadastro;

            var resultado = controller.CadastrarResidente(new ResidentesViewModel { Residente = residente });
            if (sucessoEsperado)
            {
                Assert.IsType<RedirectToActionResult>(resultado);
                var redirecionamento = resultado as RedirectToActionResult;
                Assert.Equal(nameof(PessoaFisicaController.Residentes), redirecionamento.ActionName);

            }
            else Assert.True(_verificarErroServiço.VerificarErro(erroEsperado, resultado as ViewResult));
        }

        [Fact]
        public async Task VerificarUploadCorreto()
        {
            var cadastro = PessoaFísicaDados.RetornarCadastroCorreto();
            var controller = RetornarControllerLogado(cadastro, "Memória04");

            var documentosViewModel = UploadDados.RetornarDocumentosCorreto();

            var resultado = await controller.InserirAnexos(documentosViewModel) as ViewResult;

            var caminhoDiretório = Path.Combine(Directory.GetCurrentDirectory(), cadastro.Id.ToString());
            var caminhoCPF = Path.Combine(caminhoDiretório, documentosViewModel.CPF.FileName);
            var caminhoBanco = Path.Combine(caminhoDiretório, documentosViewModel.ComprovanteBancário.FileName);
            var caminhoFrente = Path.Combine(caminhoDiretório, documentosViewModel.FrenteRGCNH.FileName);
            var caminhoEndereço = Path.Combine(caminhoDiretório, documentosViewModel.ComprovanteEndereço.FileName);
            var caminhoVerso = Path.Combine(caminhoDiretório, documentosViewModel.VersoRGCNH.FileName);
            var caminhoSelfie = Path.Combine(caminhoDiretório, documentosViewModel.Selfie.FileName);
            var caminhoAtividades = Path.Combine(caminhoDiretório, documentosViewModel.ComprovanteAtividades.FileName);

            Assert.Equal("DocumentosEnviados", resultado.ViewName);

            Assert.True(File.Exists(caminhoCPF));
            Assert.True(File.Exists(caminhoBanco));
            Assert.True(File.Exists(caminhoFrente));
            Assert.True(File.Exists(caminhoEndereço));
            Assert.True(File.Exists(caminhoVerso));
            Assert.True(File.Exists(caminhoSelfie));
            Assert.True(File.Exists(caminhoAtividades));

            //Cuidar da limpeza agora

            File.Delete(caminhoCPF);
            File.Delete(caminhoBanco);
            File.Delete(caminhoFrente);
            File.Delete(caminhoEndereço);
            File.Delete(caminhoVerso);
            File.Delete(caminhoSelfie);
            File.Delete(caminhoAtividades);
        }

        [Theory]
        [ClassData(typeof(UploadDados))]
        public async Task VerificarValidaçãoUploadDadosUploadEErros(DocumentosPessoaFísicaViewModel documentos, Erros erroEsperado)
        {
            var cadastro = PessoaFísicaDados.RetornarCadastroCorreto();
            var controller = RetornarControllerLogado(cadastro, "Memória05");

            var resultado = await controller.InserirAnexos(documentos) as ViewResult;
            Assert.True(_verificarErroServiço.VerificarErro(erroEsperado, resultado));
        }


        private PessoaFisicaController RetornarControllerLogado(CadastroPessoaFísicaModelo cadastro, string nomeBancoDeDados)
        {
            var opções = new DbContextOptionsBuilder().UseInMemoryDatabase(nomeBancoDeDados);
            var contexto = new AppContexto(opções.Options);
            var controller = ControllersDados.RetornarControllerPessoaFísica(contexto);

            var viewModel = new CadastroPessoaFísicaViewModel { Cadastro = cadastro };
            controller.Finalizar(viewModel); //Inserir um cadastro

            var usuário = LoginDados.RetornarUsuário(cadastro);

            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext { User = usuário }; //Simular Login

            return controller;
        }


    }
}
