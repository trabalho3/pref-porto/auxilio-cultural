﻿using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TestesServiços.Dados;

namespace TestesControllers.Dados
{
    public static class AnexosEspaçoCulturalDados
    {
        public static AnexosEspaçoCulturalViewModel RetornarAnexosCheio()
        {
            var retorno = new AnexosEspaçoCulturalViewModel
            {
                ComprovanteAlvará = new ArquivoPDF("Alvará.pdf"),
                ComprovanteCNPJ = new ArquivoJPG("cnpj.jpg"),
                ComprovanteAtividades = new ArquivoWord("ativ.docx"),
                ComprovanteEndereço = new ArquivoPDF("endereço.pdf"),
                ComprovanteFuncionários = new ArquivoJPG("funças.jpg")
            };

            return retorno;
        }

        public static AnexosEspaçoCulturalViewModel RetornarAnexosSemComprovanteDeEndereço() => new AnexosEspaçoCulturalViewModel() { ComprovanteAtividades = new ArquivoPDF() };
        public static AnexosEspaçoCulturalViewModel RetornarAnexosSemComprovanteAtividades() => new AnexosEspaçoCulturalViewModel() { ComprovanteEndereço = new ArquivoPDF() };
        public static AnexosEspaçoCulturalViewModel RetornarAnexosSemCNPJ()
        {
            var retorno = RetornarAnexosCheio();
            retorno.ComprovanteCNPJ = null;

            return retorno;
        }

        public static AnexosEspaçoCulturalViewModel RetornarAnexosSemAlvará()
        {
            var retorno = RetornarAnexosCheio();
            retorno.ComprovanteAlvará = null;

            return retorno;
        }

        public static AnexosEspaçoCulturalViewModel RetornarAnexosSemDocumentoFuncionários()
        {
            var retorno = RetornarAnexosCheio();
            retorno.ComprovanteFuncionários = null;

            return retorno;
        }
    }

    public class AnexosEspaçoCulturalErrosValidação : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { AnexosEspaçoCulturalDados.RetornarAnexosSemComprovanteDeEndereço(), Erros.ComprovanteEndereçoNulo };
            yield return new object[] { AnexosEspaçoCulturalDados.RetornarAnexosSemComprovanteAtividades(), Erros.ComprovanteAtividadesVazio };
            yield return new object[] { AnexosEspaçoCulturalDados.RetornarAnexosSemCNPJ(), Erros.ComprovanteCNPJVazio };
            yield return new object[] { AnexosEspaçoCulturalDados.RetornarAnexosSemAlvará(), Erros.AlvaráVazio };
            yield return new object[] { AnexosEspaçoCulturalDados.RetornarAnexosSemDocumentoFuncionários(), Erros.ComprovanteFuncionáriosVazio };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class AnexosEspaçoCulturalCorretos : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCorreto(), AnexosEspaçoCulturalDados.RetornarAnexosCheio() };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCorretoSemCNPJ(), AnexosEspaçoCulturalDados.RetornarAnexosSemCNPJ() };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCorretoSemAlvará(), AnexosEspaçoCulturalDados.RetornarAnexosSemAlvará() };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCorretoSemComprovaçãoFuncionários(), AnexosEspaçoCulturalDados.RetornarAnexosSemDocumentoFuncionários() };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
