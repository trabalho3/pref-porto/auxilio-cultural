﻿using Moq;
using PontosCulturais.Contexto;
using PontosCulturais.Controllers;
using PontosCulturais.Geradores;
using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Repositórios;
using PontosCulturais.Serviços;
using PontosCulturais.Singletons;
using PontosCulturais.Singletons.Interfaces;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TestesCadastroPessoaFísica.Geradores;

namespace TestesCadastroPessoaFísica.Dados
{
    public static class ControllersDados
    {
        public static PessoaFisicaController RetornarControllerPessoaFísica(AppContexto contexto)
        {
            var ambiente = RetornarAmbienteSingletonMockado();

            var validarStringServiço = new ValidarStringServiço();
            var validarDataServiço = new ValidarDatasServiço();
            var avisoViewModelGerador = new AvisoViewModelGerador();

            var geradorAvisoViewModel = new AvisoViewModelGerador();
            var geradorResidentesViewModel = new ResidentesViewModelGerador();
            var geradorDocumentosViewModel = new DocumentosPessoaFísicaViewModelGerador();

            var validarResidente = new ValidarResidenteServiço(validarStringServiço, geradorAvisoViewModel);
            var validarPessoaFísica = new ValidarPessoaFísicaServiço(validarStringServiço, validarDataServiço, avisoViewModelGerador);
            var validarDocumentosViewModel = new ValidarDocumentosPessoaFísicaViewModel(geradorAvisoViewModel);

            var repositórioCadastroPessoaFísica = new CadastroPessoaFísicaRepositório(contexto);
            var repositórioResidentes = new ResidentesRepositório(contexto);
            var repositórioSegmentos = new SegmentosCulturaisRepositório(contexto);
            var repositórioOpções = new OpçõesCadastroPessoaFísicaRepositório(contexto);
            var repositórioDocumentos = new DocumentosPessoaFísicaRepositório(contexto);

            var criptografiaServiço = new SegurançaServiço(ambiente);
            var uploadServiço = new UploadServiço(ambiente);


            var controller = new PessoaFísicaControllerGerador(contexto)
                .ComValidadorCadastroPessoaFísica(validarPessoaFísica)
                .ComRepositórioCadastroPessoaFísica(repositórioCadastroPessoaFísica)
                .ComRepositórioResidentes(repositórioResidentes)
                .ComCriptografiaServiço(criptografiaServiço)
                .ComRepositórioSegmentosCulturais(repositórioSegmentos)
                .ComRepositórioOpçõesCadastroPessoaFísica(repositórioOpções)
                .ComGeradorAvisoViewModel(geradorAvisoViewModel)
                .ComServiçoValidarResidente(validarResidente)
                .ComValidarDocumentoViewModel(validarDocumentosViewModel)
                .ComGeradorResidenteViewModel(geradorResidentesViewModel)
                .ComServiçoUpload(uploadServiço)
                .ComGeradorDocumentosViewModel(geradorDocumentosViewModel)
                .ComRepositórioDocumentos(repositórioDocumentos)
                .RetornarController();

            return controller;
        }

        public static EspacoCulturalController RetornarControllerEspaçoCultural(AppContexto contexto)
        {
            var validarString = new ValidarStringServiço();
            var avisoViewModelGerador = new AvisoViewModelGerador();


            var geradorEspaçoCulturalViewModel = new CadastroEspaçoCulturalViewModelGerador();
            var repositórioOpçõesCadastroCultural = new OpçõesCadastroEspaçoCulturalRepositório(contexto);
            var repositórioSegmentosCulturais = new SegmentosCulturaisRepositório(contexto);
            var validarCadastroEspaçoCulturalViewModel = new ValidarEspaçoCulturalViewModelServiço(validarString, avisoViewModelGerador);
            var repositórioEspaçoCultural = new EspaçoCulturalRepositório(contexto);
            var geradorAviso = new AvisoViewModelGerador();
            var geradorMostrarEspaço = new MostrarEspaçoCulturalViewModelGerador();
            var opçõesParaTexto = new OpçõesCadastroParaTextoServiço();
            var geradorAnexosEspaçoCultural = new AnexosEspaçoCulturalViewModelGerador();
            var validarAnexosEspaçoCultural = new ValidarAnexosEspaçoCulturalServiço(geradorAviso);
            var upload = new UploadServiço(RetornarAmbienteSingletonMockado());
            var repositórioAnexosEspaçoCultural = new AnexosEspaçoCulturalRepositório(contexto);

            var controller = new EspacoCulturalController(contexto, geradorEspaçoCulturalViewModel, repositórioOpçõesCadastroCultural, 
                repositórioSegmentosCulturais, validarCadastroEspaçoCulturalViewModel, repositórioEspaçoCultural, geradorAviso, geradorMostrarEspaço, opçõesParaTexto, geradorAnexosEspaçoCultural,
                validarAnexosEspaçoCultural, upload, repositórioAnexosEspaçoCultural);

            return controller;
        }

        private static IVariáveisAmbienteSingleton RetornarAmbienteSingletonMockado()
        {
            var ambienteMock = new Mock<IVariáveisAmbienteSingleton>();
            ambienteMock.Setup(amb => amb.CaminhoUpload).Returns(Directory.GetCurrentDirectory());
            ambienteMock.Setup(amb => amb.ChaveToken).Returns("Teste123");

            return ambienteMock.Object;
        }
    }
}
