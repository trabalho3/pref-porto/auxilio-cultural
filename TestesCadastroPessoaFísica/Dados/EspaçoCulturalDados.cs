﻿using Microsoft.AspNetCore.Http;
using PontosCulturais.Models;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace TestesControllers.Dados
{
    public static class EspaçoCulturalDados
    {
        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCorreto() => new CadastroEspaçoCulturalViewModel
        {
            IdsEquipamentos = new List<int>() { 1, 2 },
            IdsModosFuncionamento = new List<int>() { 5, 6, 7 },
            IdsOrigensRecursos = new List<int>() { 12, 13 },
            IdsSegmentosCulturais = new List<int>() { 3, 16 },
            IdsTiposAtividades = new List<int>() { 1 },
            EspaçoCultural = new EspaçoCulturalSimplesModelo
            {
                Endereço = "Rua das almas malucas",
                Bairro = "Cemitério",
                Nome = "Teatro Maluca",
                Telefone = "00123456789",
                Cnpj = "66606721000158",
                RazãoSocial = "Razão Espiritual",
                DataFuncionamento = new DateTime(2018, 07, 02),
                TipoAtendimento = "Tarô e cartas",
                Funcionários = 2,
                LotaçãoMáxima = 40,
                ReceitaAnualId = 3,
                GastoAnualId = 2,
                SituaçãoAtividadeId = 1,
                SituaçãoEspaçoId = 2,
                DeclaraçãoInformaçõesVerídicas = true,
                PossuiAlvaráDeFuncionamento = true,
                DocumentoVínculoEmpregatícioFuncionários = true
            }
        };

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCorretoSemCNPJ()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.EspaçoCultural.Cnpj = "";
            retorno.EspaçoCultural.RazãoSocial = "";

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCorretoSemAlvará()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.EspaçoCultural.PossuiAlvaráDeFuncionamento = false;

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCorretoSemComprovaçãoFuncionários()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.EspaçoCultural.DocumentoVínculoEmpregatícioFuncionários = false;

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCulturalSemVeracidade()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.EspaçoCultural.DeclaraçãoInformaçõesVerídicas = false;

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCulturalLotaçãoMáximaNegativa()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.EspaçoCultural.LotaçãoMáxima = -40;

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCulturalFuncionáriosNegativo()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.EspaçoCultural.Funcionários = -2;

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCulturalSemTipoAtendimento()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.EspaçoCultural.TipoAtendimento = "";

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCulturalComCNPJSemRazãoSocial()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.EspaçoCultural.RazãoSocial = "";

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCulturalSemCNPJComRazãoSocial()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.EspaçoCultural.Cnpj = "";

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCulturalCNPJInválido()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.EspaçoCultural.Cnpj = "59300000000104";

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCulturalSemTelefone()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.EspaçoCultural.Telefone = "";

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCulturalSemNome()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.EspaçoCultural.Nome = "";

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCulturalSemBairro()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.EspaçoCultural.Bairro = "";

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCulturalSemEndereço()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.EspaçoCultural.Endereço = "";

            return retorno;
        }
    
        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCulturalSemTiposAtividades()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.IdsTiposAtividades = new List<int>();

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoSemSegmentosCulturais()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.IdsSegmentosCulturais = new List<int>();

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoSemEquipamentos()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.IdsEquipamentos = new List<int>();

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCulturalSemModosFuncionamento()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.IdsModosFuncionamento = new List<int>();

            return retorno;
        }

        public static CadastroEspaçoCulturalViewModel RetornarEspaçoCulturalSemOrigemRecursos()
        {
            var retorno = RetornarEspaçoCorreto();
            retorno.IdsOrigensRecursos = new List<int>();

            return retorno;
        }
    }

    public class EspaçoCulturalDadosErrados : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoSemEquipamentos(), Erros.AtividadeDesenvolvidaInválida };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCulturalSemModosFuncionamento(), Erros.ModoFuncionamentoInválida };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCulturalSemOrigemRecursos(), Erros.OrigemRecursoInválida };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoSemSegmentosCulturais(), Erros.SegmentoCulturalInválido };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCulturalSemTiposAtividades(), Erros.TiposAtividadeInválido };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCulturalSemEndereço(), Erros.EndereçoInválido };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCulturalSemBairro(), Erros.BairroInválido };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCulturalSemNome(), Erros.NomeInválido };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCulturalSemTelefone(), Erros.Telefone01Inválido };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCulturalCNPJInválido(), Erros.CNPJInválido };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCulturalSemCNPJComRazãoSocial(), Erros.RazãoSocialInválida };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCulturalComCNPJSemRazãoSocial(), Erros.RazãoSocialInválida };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCulturalSemTipoAtendimento(), Erros.AtuaçãoInválida };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCulturalFuncionáriosNegativo(), Erros.FuncionáriosInválido };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCulturalLotaçãoMáximaNegativa(), Erros.LotaçãoMáximaInválida };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCulturalSemVeracidade(), Erros.NãoDeclarouVeracidade };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class EspaçoCulturalDadosCorretos : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCorreto() };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCorretoSemCNPJ() };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCorretoSemAlvará() };
            yield return new object[] { EspaçoCulturalDados.RetornarEspaçoCorretoSemComprovaçãoFuncionários() };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
