﻿using PontosCulturais.Models;
using PontosCulturais.Singletons;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace TestesCadastroPessoaFísica.Dados
{
    public static class LoginDados
    {
        public static ClaimsPrincipal RetornarUsuário(CadastroPessoaFísicaModelo cadastro)
        {
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, cadastro.NomeCompleto),
                new Claim(ClaimTypes.Email, cadastro.Email),
                new Claim(ClaimTypes.Role, ConstantesAutenticaçãoSingleton.CandidatoAoAuxílio),
                new Claim(ClaimTypes.NameIdentifier, cadastro.Id.ToString())
            };

            var identidade = new ClaimsIdentity(claims, "Login");
            return new ClaimsPrincipal(identidade);
        }
    }
}
