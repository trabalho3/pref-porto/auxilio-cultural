﻿using PontosCulturais.Models;
using PontosCulturais.Singletons;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace TestesCadastroPessoaFísica.Dados
{
    public static class PessoaFísicaDados
    {
        public static CadastroPessoaFísicaModelo RetornarCadastroCorreto() =>
            new CadastroPessoaFísicaModelo
            {
                CPF = "61666318043",
                NomeCompleto = "Esse Cadastro Deve Dar Certo",
                NomeMãe = "Mãe correta",
                Nascimento = new DateTime(1970, 01, 01),
                Telefone01 = "00123456789",
                Email = "emailcorreto@gmail.com.teste.denovo",
                Endereço = "Rua das almas",
                Bairro = "Souls",
                GêneroDeclaradoId = 1,
                DeficiênciaId = 6,
                RaçaId = 2,
                ComunidadeTradicionalId = 9,
                FunçãoCulturalId = 5,
                Atuação = "Isso aí",
                SituaçãoTrabalhoId = 5,
                TipoContaBancáriaId = 1,
                BancoId = 9,
                ContaBancária = "14587634",
                Agência = "1264",
                Senha = "123456",
                RG = "1111111",
                ÓrgãoEstado = "SSP/TO",
                DeclaraçãoAtuaçãoCulturaAtualmente = true,
                DeclaraçãoAtuaçãoCulturaDoisAnos = true,
                DeclaraçãoNãoRecebeBenefício = true,
                DeclaraçãoÑãoRecebeSeguroDesemprego = true,
                DeclaraçãoBaixaRenda = true,
                DeclaraçãoRendimentoMédio = true,
                DeclaraçãoBaixoRendimento2018 = true,
                DeclaraçãoNãoRecebeAuxílioEmergencial = true,
                DeclaraçãoVeracidadeDados = true,
                AutorizaçãoCruzamentoDeDados = true,
                SegmentosCulturaisId = new List<int>() { 1, 2 },
                ConfirmarSenha = "123456"
            };

        public static CadastroPessoaFísicaModelo RetornarCadastroNomeCompletoVazio()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "89309815000";
            retorno.RG = "0000000000";
            retorno.NomeCompleto = "";

            return retorno;
        }

        public static CadastroPessoaFísicaModelo RetornarEmailRepetido()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "09675115009";
            return retorno;
        }

        public static CadastroPessoaFísicaModelo RetornarCadastroNomeMãeVazio()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "09675115009";
            retorno.RG = "0000000001";
            retorno.NomeMãe = "";

            return retorno;
        }

        public static CadastroPessoaFísicaModelo RetornarCadastroIdadeMaiorQue120()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "27268062010";
            retorno.RG = "0000000002";
            retorno.Nascimento = new DateTime(1, 1, 1);

            return retorno;
        }

        public static CadastroPessoaFísicaModelo RetornarCadastroIdadeMenorQue18()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "53645787046";
            retorno.RG = "0000000002";
            retorno.Nascimento = new DateTime(2008, 12, 1);

            return retorno;
        }

        public static CadastroPessoaFísicaModelo RetornarCadastroTelefone10Dígitos()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "30831009063";
            retorno.RG = "0000000003";
            retorno.Email = "testeemaildiferente@email.com";
            retorno.Telefone01 = "6333632020";

            return retorno;
        }

        public static CadastroPessoaFísicaModelo RetornarCadastroTelefoneInválido()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "35196822062";
            retorno.RG = "0000000004";
            retorno.Telefone01 = "0a123a56$%9";

            return retorno;
        }

        public static CadastroPessoaFísicaModelo RetornarCadastroEmailVazio()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "53274690007";
            retorno.RG = "0000000005";
            retorno.Email = "";

            return retorno;
        }

        public static CadastroPessoaFísicaModelo RetornarCadastroEndereçoVazio()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "42822851093";
            retorno.RG = "0000000006";
            retorno.Endereço = "";

            return retorno;
        }

        public static CadastroPessoaFísicaModelo RetornarCadastroBairroVazio()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "33658216000";
            retorno.RG = "0000000007";
            retorno.Bairro = "";

            return retorno;
        }

        internal static CadastroPessoaFísicaModelo RetonarCadastroRGRepetido()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "46160705040";
            retorno.Email = "passarparachecarrg@teste.com";

            return retorno;
        }

        public static CadastroPessoaFísicaModelo RetonarCadastroContaVazia()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "31654576034";
            retorno.RG = "0000000009";
            retorno.Email = "denovo@oi.com";
            retorno.ContaBancária = "";

            return retorno;
        }

        internal static CadastroPessoaFísicaModelo RetonarCadastroContaInválida()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "85402836035";
            retorno.RG = "0000000010";
            retorno.ContaBancária = "anb559685";

            return retorno;
        }

        internal static CadastroPessoaFísicaModelo RetonarCadastroAtuaçãoVazia()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "65835064080";
            retorno.RG = "0000000011";
            retorno.Atuação = "";

            return retorno;
        }

        internal static CadastroPessoaFísicaModelo RetonarCadastroRGInválido()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "54088005040";
            retorno.RG = "ab124c";

            return retorno;
        }

        internal static CadastroPessoaFísicaModelo RetonarCadastroRGVazio()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "99306303092";
            retorno.RG = "";

            return retorno;
        }

        public static CadastroPessoaFísicaModelo RetornarCadastroAgênciaVazia()
        {
            var retorno = RetornarCadastroCorreto();
            retorno.CPF = "34661957059";
            retorno.RG = "1789546";
            retorno.Email = "Testando@vai.com";

            retorno.Agência = "";

            return retorno;
        }
    }

    public class CadastroDadosCorretos : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { PessoaFísicaDados.RetornarCadastroCorreto(), true, Erros.Nenhum };
            yield return new object[] { PessoaFísicaDados.RetornarCadastroTelefone10Dígitos(), true, Erros.Nenhum };
            yield return new object[] { PessoaFísicaDados.RetonarCadastroContaVazia(), true, Erros.Nenhum };
            yield return new object[] { PessoaFísicaDados.RetornarCadastroAgênciaVazia(), true, Erros.Nenhum };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class CadastroDadosIncorretos : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { PessoaFísicaDados.RetornarCadastroCorreto(), false, Erros.CPFRepetidoCadastroCadastro }; //CPF Repetido
            yield return new object[] { PessoaFísicaDados.RetornarEmailRepetido(), false, Erros.EmailRepetido };
            yield return new object[] { PessoaFísicaDados.RetornarCadastroNomeCompletoVazio(), false, Erros.NomeInválido };
            yield return new object[] { PessoaFísicaDados.RetornarCadastroNomeMãeVazio(), false, Erros.NomeMãeInválido };
            yield return new object[] { PessoaFísicaDados.RetornarCadastroIdadeMaiorQue120(), false, Erros.IdadeInválida };
            yield return new object[] { PessoaFísicaDados.RetornarCadastroIdadeMenorQue18(), false, Erros.IdadeInválida };
            yield return new object[] { PessoaFísicaDados.RetornarCadastroTelefoneInválido(), false, Erros.Telefone01Inválido };
            yield return new object[] { PessoaFísicaDados.RetornarCadastroEmailVazio(), false, Erros.EmailInválido };
            yield return new object[] { PessoaFísicaDados.RetornarCadastroEndereçoVazio(), false, Erros.EndereçoInválido };
            yield return new object[] { PessoaFísicaDados.RetornarCadastroBairroVazio(), false, Erros.BairroInválido };
            yield return new object[] { PessoaFísicaDados.RetonarCadastroRGVazio(), false, Erros.RGInválido };
            yield return new object[] { PessoaFísicaDados.RetonarCadastroRGInválido(), false, Erros.RGInválido };
            yield return new object[] { PessoaFísicaDados.RetonarCadastroRGRepetido(), false, Erros.RGRepetido };
            yield return new object[] { PessoaFísicaDados.RetonarCadastroAtuaçãoVazia(), false, Erros.AtuaçãoInválida };
            //yield return new object[] { PessoaFísicaDados.RetonarCadastroContaVazia(), false, Erros.ContaBancáriaInválida };
            //yield return new object[] { PessoaFísicaDados.RetonarCadastroContaInválida(), false, Erros.ContaBancáriaInválida };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
