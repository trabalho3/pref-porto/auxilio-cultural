﻿using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.Singletons;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace TestesCadastroPessoaFísica.Dados
{
    public static class ResidentesDados
    {
        public static ResidenteModelo RetornarResidenteVálido(CadastroPessoaFísicaModelo cadastro) => 
            new ResidenteModelo { CadastroPessoaFísicaModelo = cadastro, CPFOuCertidão = "24996345020", Nome = "Residente Válido", Parentesco = "Teste" };

        public static ResidenteModelo RetornarResidenteVálido() =>
            new ResidenteModelo {CPFOuCertidão = "24996345020", Nome = "Residente Válido", Parentesco = "Teste" };

        public static ResidenteModelo RetornarResidenteCPFCertidãoInválida()
        {
            var retorno = RetornarResidenteVálido();
            retorno.CPFOuCertidão = "4577as";

            return retorno;
        }

        public static ResidenteModelo RetornarResidenteCPFInválido()
        {
            var retorno = RetornarResidenteVálido();
            retorno.CPFOuCertidão = "00000000000";

            return retorno;
        }

        internal static ResidenteModelo RetornarResidenteNomeInválido()
        {
            var retorno = RetornarResidenteVálido();
            retorno.Nome = "";

            return retorno;
        }

        internal static ResidenteModelo RetornarResidenteParentescoInválido()
        {
            var retorno = RetornarResidenteVálido();
            retorno.Parentesco = "";

            return retorno;
        }
    }

    public class ResidentesDadosCorretos : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { ResidentesDados.RetornarResidenteVálido(), true };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class ResidentesDadosIncorretos : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { ResidentesDados.RetornarResidenteCPFCertidãoInválida(), false, Erros.CPFInválido };
            yield return new object[] { ResidentesDados.RetornarResidenteCPFInválido(), false, Erros.CPFInválido };
            yield return new object[] { ResidentesDados.RetornarResidenteNomeInválido(), false, Erros.NomeInválido };
            yield return new object[] { ResidentesDados.RetornarResidenteParentescoInválido(), false, Erros.ParentescoInválido };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}