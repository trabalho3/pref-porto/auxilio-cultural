﻿using PontosCulturais.Singletons;
using PontosCulturais.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TestesServiços.Dados;

namespace TestesCadastroPessoaFísica.Dados
{
    public class UploadDados : IEnumerable<object[]>
    {

        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { RetornarDocumentosCPFVazio(), Erros.ComprovanteCPFNulo };
            //yield return new object[] { RetornarBancoVazio(), Erros.ComprovanteBancárioNulo };
            yield return new object[] {RetornarEndereçoVazio(), Erros.ComprovanteEndereçoNulo};
            yield return new object[] { RetornarFrenteVazia(), Erros.FrenteRGCNHNulo };
            yield return new object[] { RetornarVersoVazio(), Erros.VersoRGCNHNulo};
            yield return new object[] { RetornarSelfieVazia(), Erros.SelfieNula};
            yield return new object[] { RetornarComprovanteAtividadesVazio(), Erros.ComprovanteAtividadesVazio};
            yield return new object[] { RetornarComprovanteAtividadesMuitoGrande(), Erros.ComprovanteAtividadesGrande};
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public static DocumentosPessoaFísicaViewModel RetornarDocumentosCorreto() => new DocumentosPessoaFísicaViewModel
        {
            CPF = new ArquivoPDF("cpf.pdf"),
            ComprovanteBancário = new ArquivoWord("banco.docx"),
            ComprovanteEndereço = new ArquivoJPG("end.jpg"),
            FrenteRGCNH = new ArquivoJPG("frenterg.jpg"),
            VersoRGCNH = new ArquivoWord("versorg.docx"),
            Selfie = new ArquivoJPG("selfie.jpg"),
            ComprovanteAtividades = new ArquivoWord("atividades.docx")
        };

        public static DocumentosPessoaFísicaViewModel RetornarDocumentosCPFVazio()
        {
            var retorno = RetornarDocumentosCorreto();
            retorno.CPF = null;

            return retorno;
        }

        public static DocumentosPessoaFísicaViewModel RetornarBancoVazio()
        {
            var retorno = RetornarDocumentosCorreto();
            retorno.ComprovanteBancário = null;

            return retorno;
        }

        public static DocumentosPessoaFísicaViewModel RetornarEndereçoVazio()
        {
            var retorno = RetornarDocumentosCorreto();
            retorno.ComprovanteEndereço = null;

            return retorno;
        }

        public static DocumentosPessoaFísicaViewModel RetornarSelfieVazia()
        {
            var retorno = RetornarDocumentosCorreto();
            retorno.Selfie = null;

            return retorno;
        }

        public static DocumentosPessoaFísicaViewModel RetornarFrenteVazia()
        {
            var retorno = RetornarDocumentosCorreto();
            retorno.FrenteRGCNH = null;

            return retorno;
        }

        public static DocumentosPessoaFísicaViewModel RetornarVersoVazio()
        {
            var retorno = RetornarDocumentosCorreto();
            retorno.VersoRGCNH = null;

            return retorno;
        }

        public static DocumentosPessoaFísicaViewModel RetornarComprovanteAtividadesVazio()
        {
            var retorno = RetornarDocumentosCorreto();
            retorno.ComprovanteAtividades = null;

            return retorno;
        }

        public static DocumentosPessoaFísicaViewModel RetornarComprovanteAtividadesMuitoGrande()
        {
            var retorno = RetornarDocumentosCorreto();
            retorno.ComprovanteAtividades = new ArquivoGrande("word.docx", "grandão.docx");

            return retorno;
        }


    }
}
