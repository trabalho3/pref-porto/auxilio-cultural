﻿using PontosCulturais.Contexto;
using PontosCulturais.Controllers;
using PontosCulturais.Geradores;
using PontosCulturais.Geradores.Interfaces;
using PontosCulturais.Models;
using PontosCulturais.Models.CadastroPessoaFísica;
using PontosCulturais.Repositórios.Interface;
using PontosCulturais.Serviços.Interfaces;
using PontosCulturais.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using TestesControllers.Serviços;

namespace TestesCadastroPessoaFísica.Geradores
{
    public class PessoaFísicaControllerGerador
    {
        private AppContexto _contexto;

        private IViewModelControllersGerador<CadastroPessoaFísicaViewModel> _geradorViewModelCadastroPessoaFísica;
        private IAvisoViewModelGerador _geradorAvisoViewModel;
        private IResidentesViewModelGerador _geradorResidenteViewModel;
        private IMostrarDadosViewModelGerador _geradorMostrarDadosViewModel;
        private IViewModelControllersGerador<DocumentosPessoaFísicaViewModel> _geradorDocumentosViewModel;

        private ICadastroPessoaFísicaRepositório _repositórioCadastroPessoaFísica;
        private IOpçõesCadastroPessoaFísicaRepositório _repositórioOpçõesCadastroPessoaFísica;
        private ISegmentosCulturaisRepositório _repositórioSegmentosCulturais;
        private IResidentesRepositório _repositórioResidentes;
        private IDocumentosPessoaFísicaRepositório _repositórioDocumentos;

        private IValidarModeloServiço<CadastroPessoaFísicaModelo> _validarCadastroPessoaFísica;
        private IValidarModeloServiço<ResidenteModelo> _validarResidenteServiço;
        private IValidarModeloServiço<DocumentosPessoaFísicaViewModel> _validarDocumentosViewModel;

        private ISegurançaServiço _criptografiaServiço;
        private IUploadServiço _uploadServiço;

        public PessoaFísicaControllerGerador(AppContexto contexto)
        {
            _contexto = contexto;
        }

        public PessoaFísicaControllerGerador ComGeradorViewModelCadastroPessoaFísica(IViewModelControllersGerador<CadastroPessoaFísicaViewModel> gerador)
        {
            _geradorViewModelCadastroPessoaFísica = gerador;
            return this;
        }

        public PessoaFísicaControllerGerador ComRepositórioCadastroPessoaFísica(ICadastroPessoaFísicaRepositório repositório)
        {
            _repositórioCadastroPessoaFísica = repositório;
            return this;
        }

        public PessoaFísicaControllerGerador ComValidadorCadastroPessoaFísica(IValidarModeloServiço<CadastroPessoaFísicaModelo> validador)
        {
            _validarCadastroPessoaFísica = validador;
            return this;
        }

        public PessoaFísicaControllerGerador ComGeradorAvisoViewModel(IAvisoViewModelGerador gerador)
        {
            _geradorAvisoViewModel = gerador;
            return this;
        }

        public PessoaFísicaControllerGerador ComRepositórioOpçõesCadastroPessoaFísica(IOpçõesCadastroPessoaFísicaRepositório repositório)
        {
            _repositórioOpçõesCadastroPessoaFísica = repositório;
            return this;
        }

        public PessoaFísicaControllerGerador ComRepositórioSegmentosCulturais(ISegmentosCulturaisRepositório repositório)
        {
            _repositórioSegmentosCulturais = repositório;
            return this;
        }

        public PessoaFísicaControllerGerador ComCriptografiaServiço(ISegurançaServiço serviço)
        {
            _criptografiaServiço = serviço;
            return this;
        }

        public PessoaFísicaControllerGerador ComRepositórioResidentes(IResidentesRepositório repositório)
        {
            _repositórioResidentes = repositório;
            return this;
        }

        public PessoaFísicaControllerGerador ComGeradorResidenteViewModel(IResidentesViewModelGerador gerador)
        {
            _geradorResidenteViewModel = gerador;
            return this;
        }

        public PessoaFísicaControllerGerador ComServiçoValidarResidente(IValidarModeloServiço<ResidenteModelo> serviço)
        {
            _validarResidenteServiço = serviço;
            return this;
        }

        public PessoaFísicaControllerGerador ComServiçoUpload(IUploadServiço serviço)
        {
            _uploadServiço = serviço;

            return this;
        }

        public PessoaFísicaControllerGerador ComGeradorMostrarDadosViewModel(IMostrarDadosViewModelGerador gerador)
        {
            _geradorMostrarDadosViewModel = gerador;
            return this;
        }

        public PessoaFísicaControllerGerador ComGeradorDocumentosViewModel(IViewModelControllersGerador<DocumentosPessoaFísicaViewModel> gerador)
        {
            _geradorDocumentosViewModel = gerador;

            return this;
        }

        public PessoaFísicaControllerGerador ComValidarDocumentoViewModel(IValidarModeloServiço<DocumentosPessoaFísicaViewModel> validar)
        {
            _validarDocumentosViewModel = validar;
            return this;
        }

        public PessoaFísicaControllerGerador ComRepositórioDocumentos(IDocumentosPessoaFísicaRepositório repositório)
        {
            _repositórioDocumentos = repositório;
            return this;
        }

        public PessoaFisicaController RetornarController()
        {
            return new PessoaFisicaController(_contexto, _geradorViewModelCadastroPessoaFísica, _repositórioOpçõesCadastroPessoaFísica, _validarCadastroPessoaFísica, _geradorAvisoViewModel,
                _repositórioCadastroPessoaFísica, _repositórioSegmentosCulturais, _criptografiaServiço, _repositórioResidentes, _geradorResidenteViewModel, _validarResidenteServiço, _geradorMostrarDadosViewModel,
                _geradorDocumentosViewModel, _uploadServiço, _validarDocumentosViewModel, _repositórioDocumentos, new FakeEmailServiço());
        }
    }
}
