﻿using PontosCulturais.Serviços.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestesControllers.Serviços
{
    public class FakeEmailServiço : IEmailServiço
    {
        public void EnviarEmailConfirmação(string token, string email)
        {
            
        }

        public void EnviarEmailRedefinirSenha(string token, string email)
        {
           
        }
    }
}
