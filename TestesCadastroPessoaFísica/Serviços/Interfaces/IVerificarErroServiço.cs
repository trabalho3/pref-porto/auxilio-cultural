﻿using Microsoft.AspNetCore.Mvc;
using PontosCulturais.Singletons;

namespace TestesControllers.Serviços.Interfaces
{
    public interface IVerificarErroServiço
    {
        bool VerificarErro(Erros erroEsperado, ViewResult resultado);
    }
}