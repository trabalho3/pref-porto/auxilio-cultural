﻿using Microsoft.AspNetCore.Mvc;
using PontosCulturais.Singletons;
using PontosCulturais.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using TestesControllers.Serviços.Interfaces;

namespace TestesControllers.Serviços
{
    public class VerificarErroServiço : IVerificarErroServiço
    {
        public bool VerificarErro(Erros erroEsperado, ViewResult resultado)
        {
            var erro = resultado.ViewData.Model as ComAvisoViewModelBase;
            return erro.Aviso.CódigoErro == erroEsperado;
        }
    }
}
