﻿using PontosCulturais.Singletons;
using System;
using System.Collections.Generic;
using System.Text;
using TestesServiços.Dados.Base;

namespace TestesServiços.Dados
{
    public class ArquivoGrande : ArquivoBase
    {
        public ArquivoGrande(string nomeRecurso) : base(nomeRecurso)
        {
        }

        public ArquivoGrande(string nomeRecurso, string nomeArquivo) : base(nomeRecurso, nomeArquivo)
        {
        }

        public override long Length => 11 * 1024 * 1024; //Maior que o limite
    }
}
