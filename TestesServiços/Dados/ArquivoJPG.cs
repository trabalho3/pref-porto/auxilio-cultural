﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipelines;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestesServiços.Dados.Base;

namespace TestesServiços.Dados
{
    public class ArquivoJPG : ArquivoBase
    {
        public ArquivoJPG() : base("foto.jpg")
        {
        }

        public ArquivoJPG(string nomeArquivo) : base("foto.jpg", nomeArquivo)
        {
        }
    }
}
