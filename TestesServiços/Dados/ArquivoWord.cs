﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestesServiços.Dados.Base;

namespace TestesServiços.Dados
{
    public class ArquivoWord : ArquivoBase
    {
        public ArquivoWord() : base("word.docx")
        {
        }

        public ArquivoWord(string nome) : base("word.docx", nome)
        {
        }
    }
}

