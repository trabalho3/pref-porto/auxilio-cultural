﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipelines;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestesServiços.Dados.Base
{
    public abstract class ArquivoBase : IFormFile
    {
        protected readonly Stream _arquivo;
        protected readonly string _nomeArquivo;

        protected ArquivoBase(string nomeRecurso)
        {
            var assembly = Assembly.GetExecutingAssembly();
            _arquivo = assembly.GetManifestResourceStream($"TestesServiços.ArquivosTesteUpload.{nomeRecurso}");
            _nomeArquivo = nomeRecurso;
        }

        protected ArquivoBase(string nomeRecurso, string nomeArquivo)
        {
            var assembly = Assembly.GetExecutingAssembly();
            _arquivo = assembly.GetManifestResourceStream($"TestesServiços.ArquivosTesteUpload.{nomeRecurso}");
            _nomeArquivo = nomeArquivo;
        }
        public string ContentDisposition => throw new NotImplementedException();

        public string ContentType => throw new NotImplementedException();

        public string FileName => _nomeArquivo;

        public IHeaderDictionary Headers => throw new NotImplementedException();

        public virtual long Length => _arquivo.Length;

        public string Name => throw new NotImplementedException();

        public void CopyTo(Stream target)
        {
            throw new NotImplementedException();
        }

        public Task CopyToAsync(Stream target, CancellationToken cancellationToken = default) => _arquivo.CopyToAsync(target, cancellationToken);

        public Stream OpenReadStream()
        {
            throw new NotImplementedException();
        }
    }
}
