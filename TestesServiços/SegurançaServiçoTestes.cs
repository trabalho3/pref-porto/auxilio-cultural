﻿using Microsoft.IdentityModel.Tokens;
using Moq;
using PontosCulturais.Serviços;
using PontosCulturais.Singletons;
using PontosCulturais.Singletons.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TestesServiços
{
    public class SegurançaServiçoTestes
    {
        [Theory]
        [InlineData(1, "Teste123", PropósitoToken.ConfirmarEmail)]
        [InlineData(82, "123*#Plut0a&*", PropósitoToken.ConfirmarEmail)]
        [InlineData(33, "EitaTeste45578*", PropósitoToken.RedefinirSenha)]
        public void ChecarValidadeTokenDadoTokensVálidos(int idEsperado, string chave, PropósitoToken propósito)
        {
            var ambiente = new Mock<IVariáveisAmbienteSingleton>();
            ambiente.Setup(amb => amb.ChaveToken).Returns(chave);

            var serviço = new SegurançaServiço(ambiente.Object);
            var token = serviço.GerarToken(idEsperado, propósito);
            var resposta = serviço.ValidarToken(token, propósito);

            Assert.Equal(StatusToken.Válido, resposta.Item1);
            Assert.Equal(idEsperado, resposta.Item2);
            
        }
        [Theory]
        [InlineData(1, -2, PropósitoToken.ConfirmarEmail, PropósitoToken.ConfirmarEmail, StatusToken.Expirado)]
        [InlineData(3, -1, PropósitoToken.ConfirmarEmail, PropósitoToken.ConfirmarEmail, StatusToken.Expirado)]
        [InlineData(8, 24, PropósitoToken.ConfirmarEmail, PropósitoToken.RedefinirSenha, StatusToken.PropósitoIncompatível)]
        [InlineData(8, 24, PropósitoToken.RedefinirSenha, PropósitoToken.ConfirmarEmail, StatusToken.PropósitoIncompatível)]

        public void ChecarValidadeTokenDadoTokensInválidos(int id, int validade, PropósitoToken propósitoCerto, PropósitoToken propósitoErrado, StatusToken statusEsperado)
        {
            var ambiente = new Mock<IVariáveisAmbienteSingleton>();
            ambiente.Setup(amb => amb.ChaveToken).Returns("Teste");

            var serviço = new SegurançaServiço(ambiente.Object);
            var token = serviço.GerarToken(id, propósitoCerto, validade);
            var resposta = serviço.ValidarToken(token, propósitoErrado);

            Assert.Equal(statusEsperado, resposta.Item1);
            Assert.Equal(-1, resposta.Item2);
        }

        [Theory]
        [InlineData("erro", "acerto", StatusToken.AssinaturaInválida)]
        [InlineData("Mais Um", "Pluto0899%$&*", StatusToken.AssinaturaInválida)]
        [InlineData("abc342@#$*()th+", "abc342@#$*()th+", StatusToken.Válido)]
        public void ChecarValidadeTokenDadoTokenEChaveEsperada(string chaveUsada, string chaveEsperada, StatusToken statusEsperado)
        {
            var ambiente = new Mock<IVariáveisAmbienteSingleton>();
            ambiente.Setup(amb => amb.ChaveToken).Returns(chaveEsperada);

            var serviço = new SegurançaServiço(ambiente.Object);

            var bytesId = BitConverter.GetBytes(32);
            var bytesPropósito = BitConverter.GetBytes((int)PropósitoToken.ConfirmarEmail);
            var bytesChave = Encoding.ASCII.GetBytes(serviço.Criptografar(chaveUsada));
            var bytesHoje = BitConverter.GetBytes(DateTime.UtcNow.AddHours(24).ToBinary());

            var dados = new byte[bytesId.Length + bytesPropósito.Length + bytesChave.Length + bytesHoje.Length];

            Buffer.BlockCopy(bytesId, 0, dados, 0, bytesId.Length);
            Buffer.BlockCopy(bytesHoje, 0, dados, bytesId.Length, bytesHoje.Length);
            Buffer.BlockCopy(bytesPropósito, 0, dados, bytesId.Length + bytesHoje.Length, bytesPropósito.Length);
            Buffer.BlockCopy(bytesChave, 0, dados, bytesId.Length + bytesHoje.Length + bytesPropósito.Length, bytesChave.Length);

            var token = Base64UrlEncoder.Encode(dados);

            var resposta = serviço.ValidarToken(token, PropósitoToken.ConfirmarEmail);

            Assert.Equal(statusEsperado, resposta.Item1);
        }
    }
}
