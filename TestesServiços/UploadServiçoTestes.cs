﻿using Microsoft.AspNetCore.Http;
using Moq;
using PontosCulturais.Exceptions;
using PontosCulturais.Serviços;
using PontosCulturais.Singletons.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using TestesServiços.Dados;
using Xunit;

namespace TestesServiços
{
    public class UploadServiçoTestes
    {
        [Theory]
        [MemberData(nameof(RetornarDadosExceçãoExtensão))]
        public async Task ExceçãoDeExtensãoDadosArquivosEExtensões(IFormFile arquivo, IEnumerable<string> extensões)
        {
            var diretórioAtual = Directory.GetCurrentDirectory();
            var ambienteMock = new Mock<IVariáveisAmbienteSingleton>();
            ambienteMock.Setup(a => a.CaminhoUpload)
                .Returns(diretórioAtual);

            var id = "1";

            var uploadServiço = new UploadServiço(ambienteMock.Object);
            await Assert.ThrowsAsync<ExtensãoInválidaException>(async () => await uploadServiço.FazerUpload(arquivo, id, extensões));

        }

        [Fact]
        public async Task VerificarUploadDadoArquivo()
        {
            var arquivo = new ArquivoJPG();
            var diretórioAtual = Directory.GetCurrentDirectory();
            var ambienteMock = new Mock<IVariáveisAmbienteSingleton>();
            ambienteMock.Setup(a => a.CaminhoUpload)
                .Returns(diretórioAtual);

            var id = "1";

            var uploadServiço = new UploadServiço(ambienteMock.Object);
            var caminho = await uploadServiço.FazerUpload(arquivo, id, new List<string>() { "png", "gif", "jpg", "tiff" });
            var caminhoDiretório = Path.Combine(diretórioAtual, id);

            Assert.True(File.Exists(caminho));

            File.Delete(caminho);
            Directory.Delete(caminhoDiretório);
        }

        [Fact]
        public async Task VerificarExceçãoArquivoMesmoNome()
        {
            var arquivo = new ArquivoJPG();
            var diretórioAtual = Directory.GetCurrentDirectory();
            var ambienteMock = new Mock<IVariáveisAmbienteSingleton>();
            ambienteMock.Setup(a => a.CaminhoUpload)
                .Returns(diretórioAtual);

            var id = "1";

            var uploadServiço = new UploadServiço(ambienteMock.Object);
            var caminho = await uploadServiço.FazerUpload(arquivo, id, new List<string>() { "png", "gif", "jpg", "tiff" });
            var caminhoDiretório = Path.Combine(diretórioAtual, id);

            await Assert.ThrowsAsync<ArquivoExistenteException>(async () => await uploadServiço.FazerUpload(arquivo, id, new List<string>() { "png", "gif", "jpg", "tiff" }));

            File.Delete(caminho);
            Directory.Delete(caminhoDiretório);
        }

        [Fact]
        public async Task VerificarNullReferenceException()
        {
            var diretórioAtual = Directory.GetCurrentDirectory();
            var ambienteMock = new Mock<IVariáveisAmbienteSingleton>();
            ambienteMock.Setup(a => a.CaminhoUpload)
                .Returns(diretórioAtual);

            var uploadServiço = new UploadServiço(ambienteMock.Object);
            IFormFile nulo = null;
            await Assert.ThrowsAsync<NullReferenceException>(() => uploadServiço.FazerUpload(nulo, "1", new List<string>() { "png", "gif", "jpg", "tiff" }));
        }

        public static IEnumerable<object[]> RetornarDadosExceçãoExtensão()
        {
            yield return new object[] { new ArquivoPDF(), new List<string>() { "jpg", "png" } };
            yield return new object[] { new ArquivoWord(), new List<string>() { "pdf", "txt" } };
        }
    }
}
