﻿using PontosCulturais.Serviços;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TestesValidação
{
    public class ValidarDatasServiçoTestes
    {
        [Theory]
        [ClassData(typeof(ValidarDatasServiçoDados))]
        public void ValidarDatasDadasIdadesMínimaEMáxima(int idadeMínima, int idadeMáxima, DateTime nascimento, bool esperado)
        {
            var validador = new ValidarDatasServiço();
            Assert.Equal(esperado, validador.ValidarIdade(idadeMínima, idadeMáxima, nascimento));
        }
    }


    public class ValidarDatasServiçoDados : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { 18, 120, new DateTime(1996, 12, 08), true };
            yield return new object[] { 18, 120, new DateTime(0001, 01, 01), false };
            yield return new object[] { 18, 120, new DateTime(2009, 04, 15), false };
            yield return new object[] { 18, 120, new DateTime(1966, 05, 10), true };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
