﻿using PontosCulturais.Serviços;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xunit;

namespace TestesValidação
{
    public class ValidarStringServiçoTestes
    {
        [Theory]
        [InlineData("259.025.830-55", false)]
        [InlineData("67017750047", true)]
        [InlineData("00011100012", false)]
        [InlineData("584.358.480-19", false)]
        [InlineData("", false)]
        [InlineData("36036482000", true)]
        public void ValidarCPFSDadaEntrada(string cpf, bool esperado)
        {
            var validador = new ValidarStringServiço();

            Assert.Equal(esperado, validador.ValidarCPF(cpf));
        }

        [Theory]
        [InlineData("4568781486456987", true)]
        [InlineData("as474876568", false)]
        [InlineData("48787a", false)]
        [InlineData("", false)]
        [InlineData(null, false)]
        [InlineData("78a5784", false)]
        [InlineData("320", true)]
        [InlineData("3", true)]
        public void ValidarSomenteNúmerosDadaEntrada(string valor, bool esperado)
        {
            var validador = new ValidarStringServiço();
            Assert.Equal(esperado, validador.ValidarSomenteNúmeros(valor));
        }
        
        [Theory]
        [InlineData("teste", 0,5, true)]
        [InlineData("outro", 0,8, true)]
        [InlineData("", 0,5, true)]
        [InlineData("", 1,5, false)]
        [InlineData(null, 1,5, false)]
        [InlineData(null, 0,5, true)]
        [InlineData("o a", 0,5, true)]
        public void ValidarQuantidadeDeCaracteresDadosValorMínimoEMáximo(string valor, int mínimo, int máximo, bool esperado)
        {
            var validador = new ValidarStringServiço();
            Assert.Equal(esperado, validador.ValidarQuantidadeCaracteres(valor, mínimo, máximo));
        }

        [Theory]
        [InlineData("opa", 3, true)]
        [InlineData("", 0, false)]
        [InlineData(null, 0, false)]
        [InlineData("a", 1, true)]
        [InlineData("outros", 4, false)]
        [InlineData("mais um", 20, false)]
        public void ValidarQuantidadeDeCaracteresDadosValorEQuantidade(string valor, int quantidade, bool esperado)
        {
            var validador = new ValidarStringServiço();
            Assert.Equal(esperado, validador.ValidarQuantidadeCaracteres(valor, quantidade));
        }

        [Theory]
        [InlineData("00000000000", true)]
        [InlineData("(11)123456789", false)]
        [InlineData("1112345-6789", false)]
        [InlineData("(11)12345-6789", false)]
        [InlineData("1112345678", true)]
        [InlineData("(11)12345678", false)]
        [InlineData("(11)1234-5678", false)]
        [InlineData("111234-5678", false)]
        [InlineData("1234", false)]
        [InlineData("", false)]
        [InlineData(null, false)]
        public void ValidarTelefoneDadoValor(string valor, bool esperado)
        {
            var validador = new ValidarStringServiço();
            Assert.Equal(esperado, validador.ValidarTelefone(valor));
        }

        [Theory]
        [InlineData("correto@certo.ok", true)]
        [InlineData(null, false)]
        [InlineData("correto.08@gmail.com", true)]
        [InlineData("correto-08@gmail.com", true)]
        [InlineData("correto_08@gmail.com", true)]
        [InlineData("corretocerto.ok", false)]
        [InlineData("Correto@correto.com", true)]
        [InlineData("certo@portonacional.to.gov.br", true)]
        [InlineData("errado@@portonacional.to.gov.br", false)]
        [InlineData("errado@portonacional..to.gov.br", false)]
        [InlineData("errado@portonacional.to.gov..br", false)]
        [InlineData("errado@portonacional.to..gov.br", false)]
        public void ValidarEmailDadoValor(string dado, bool esperado)
        {
            var validador = new ValidarStringServiço();
            Assert.Equal(esperado, validador.ValidarEmail(dado));
        }

    }
}
